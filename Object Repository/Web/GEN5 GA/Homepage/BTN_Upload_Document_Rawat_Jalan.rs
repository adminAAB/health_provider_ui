<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_Upload_Document_Rawat_Jalan</name>
   <tag></tag>
   <elementGuidId>a0d5ae9c-e6f7-4f13-a556-4ef430f0b2eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;_mnitm_m1000079&quot;]//button[text()='Upload Document Rawat Inap'][count(. | //*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame']) = count(//*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;_mnitm_m1000079&quot;]//button[text()='Upload Document Rawat Inap']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/GEN5 GA/Frame</value>
   </webElementProperties>
</WebElementEntity>
