<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_Upload_Document</name>
   <tag></tag>
   <elementGuidId>18ff6e95-3411-4b61-8bc9-950a164f32d0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;_mnitm_m1000079&quot;]/div[3]/button[count(. | //*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame']) = count(//*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;_mnitm_m1000079&quot;]/div[3]/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/GEN5 GA/Frame</value>
   </webElementProperties>
</WebElementEntity>
