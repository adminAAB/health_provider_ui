<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_Choose_Doctor</name>
   <tag></tag>
   <elementGuidId>aa87e467-1db8-48ad-ac62-29b3f9a3287e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;DoctorInfoPopUpSection-0&quot;]/div[1]/div/div[5]/a2is-buttons/div/div/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;DoctorInfoPopUpSection-0&quot;]/div[1]/div/div[5]/a2is-buttons/div/div/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/GEN5 GA/Frame</value>
   </webElementProperties>
</WebElementEntity>
