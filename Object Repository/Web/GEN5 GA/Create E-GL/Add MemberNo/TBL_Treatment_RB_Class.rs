<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TBL_Treatment_RB_Class</name>
   <tag></tag>
   <elementGuidId>2ac39cc0-8008-43e4-8192-cad5bf2caa8d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;ProviderRoomBoardInfoPopUpSection-0&quot;]/div[1]/div/div[1]/a2is-datatable/div[2]/div/table[count(. | //*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame']) = count(//*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;ProviderRoomBoardInfoPopUpSection-0&quot;]/div[1]/div/div[1]/a2is-datatable/div[2]/div/table</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/GEN5 GA/Frame</value>
   </webElementProperties>
</WebElementEntity>
