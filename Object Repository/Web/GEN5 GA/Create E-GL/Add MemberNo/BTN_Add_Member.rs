<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_Add_Member</name>
   <tag></tag>
   <elementGuidId>75e82b18-45c5-4639-b165-78945cecaee3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;PatientInformation-0&quot;]/div[1]/div/a2is-text-autocomplete-wide/div[2]/div[1]/div/span/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;PatientInformation-0&quot;]/div[1]/div/a2is-text-autocomplete-wide/div[2]/div[1]/div/span/button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/GEN5 GA/Frame</value>
   </webElementProperties>
</WebElementEntity>
