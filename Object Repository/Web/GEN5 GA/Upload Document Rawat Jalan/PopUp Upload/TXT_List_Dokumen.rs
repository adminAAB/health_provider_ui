<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_List_Dokumen</name>
   <tag></tag>
   <elementGuidId>b012854d-39cb-4f3c-8cc9-f35413de845b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;txtAreaRemarks&quot;]/div[2]/div[1]/div/textarea[count(. | //*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame']) = count(//*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;txtAreaRemarks&quot;]/div[2]/div[1]/div/textarea</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/GEN5 GA/Frame</value>
   </webElementProperties>
</WebElementEntity>
