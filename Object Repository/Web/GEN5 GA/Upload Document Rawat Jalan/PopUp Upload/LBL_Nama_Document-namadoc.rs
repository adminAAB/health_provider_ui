<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_Nama_Document-namadoc</name>
   <tag></tag>
   <elementGuidId>3e581a69-082d-4638-bbcf-58e48bf87f91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;uploadImage&quot;]/div[3]/div[1]/div//div[text()='${namadoc}'][count(. | //*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame']) = count(//*[@ref_element = 'Object Repository/Web/GEN5 GA/Frame'])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;uploadImage&quot;]/div[3]/div[1]/div//div[text()='${namadoc}']</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/GEN5 GA/Frame</value>
   </webElementProperties>
</WebElementEntity>
