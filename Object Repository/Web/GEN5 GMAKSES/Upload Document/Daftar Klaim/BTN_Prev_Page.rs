<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_Prev_Page</name>
   <tag></tag>
   <elementGuidId>830d4ecc-6a09-4b9f-868e-992fd9a71df6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@class='fa fa-backward']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//span[@class='fa fa-backward']</value>
   </webElementProperties>
</WebElementEntity>
