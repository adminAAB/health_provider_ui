<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_Day-day</name>
   <tag></tag>
   <elementGuidId>141e657e-fc64-43ee-9d31-a3279b0b3efa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class,&quot;day&quot;) and not(contains(@class,&quot;outside-month&quot;))  and text()='${day}']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(@class,&quot;day&quot;) and not(contains(@class,&quot;outside-month&quot;))  and text()='${day}']</value>
   </webElementProperties>
</WebElementEntity>
