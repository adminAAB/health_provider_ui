<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TBL_Claim</name>
   <tag></tag>
   <elementGuidId>f01d1af7-e764-4785-9d4d-633aa7dc71b8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//table[@class='table table-striped table-condensed']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//table[@class='table table-striped table-condensed']</value>
   </webElementProperties>
</WebElementEntity>
