<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>BTN_Clear</name>
   <tag></tag>
   <elementGuidId>5086cdf9-3c29-4793-b0cc-84198b977e85</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;btnClaimClaimHistory&quot; and @value=&quot;Clear&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;btnClaimClaimHistory&quot; and @value=&quot;Clear&quot;]</value>
   </webElementProperties>
</WebElementEntity>
