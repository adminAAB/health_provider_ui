<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_Tgl_Admission_From</name>
   <tag></tag>
   <elementGuidId>684d19eb-c4c4-4030-a125-a17e7e62bc67</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()='Tanggal Perawatan']/parent::*[@class='form-group']/div[1]//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='Tanggal Perawatan']/parent::*[@class='form-group']/div[1]//input</value>
   </webElementProperties>
</WebElementEntity>
