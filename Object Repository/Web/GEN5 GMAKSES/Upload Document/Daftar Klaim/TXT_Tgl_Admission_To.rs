<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_Tgl_Admission_To</name>
   <tag></tag>
   <elementGuidId>5ead996d-9f05-429e-9b08-b63d98811f7e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()='Tanggal Perawatan']/parent::*[@class='form-group']/div[3]//input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='Tanggal Perawatan']/parent::*[@class='form-group']/div[3]//input</value>
   </webElementProperties>
</WebElementEntity>
