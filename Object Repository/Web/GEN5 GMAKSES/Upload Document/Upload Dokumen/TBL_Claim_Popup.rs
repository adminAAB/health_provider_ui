<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TBL_Claim_Popup</name>
   <tag></tag>
   <elementGuidId>e88a7693-6b0c-4c12-90d5-bb3291c67799</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(@class,'styles_modal')]//*[@class=&quot;table table-striped table-condensed&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(@class,'styles_modal')]//*[@class=&quot;table table-striped table-condensed&quot;]</value>
   </webElementProperties>
</WebElementEntity>
