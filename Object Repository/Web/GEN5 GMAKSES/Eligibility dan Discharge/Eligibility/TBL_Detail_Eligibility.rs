<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TBL_Detail_Eligibility</name>
   <tag></tag>
   <elementGuidId>38a17b87-d3c8-422a-b8cd-a3fa4c9881e6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[contains(@class,&quot;summary-eligibility-content&quot;)][2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[contains(@class,&quot;summary-eligibility-content&quot;)][2]</value>
   </webElementProperties>
</WebElementEntity>
