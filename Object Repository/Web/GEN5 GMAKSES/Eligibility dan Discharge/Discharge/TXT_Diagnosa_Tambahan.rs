<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>TXT_Diagnosa_Tambahan</name>
   <tag></tag>
   <elementGuidId>f0008192-2fad-4c53-b1eb-f6fbb1ce8059</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()=&quot;Diagnosa Tambahan&quot;]//parent::*//input[@type=&quot;text&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()=&quot;Diagnosa Tambahan&quot;]//parent::*//input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
