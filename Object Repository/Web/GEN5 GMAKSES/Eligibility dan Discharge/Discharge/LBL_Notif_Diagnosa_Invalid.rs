<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LBL_Notif_Diagnosa_Invalid</name>
   <tag></tag>
   <elementGuidId>1e90fe5d-91a0-4e35-bb2d-8b893ae89314</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()=&quot;Diagnosa tidak ditemukan&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()=&quot;Diagnosa tidak ditemukan&quot;]</value>
   </webElementProperties>
</WebElementEntity>
