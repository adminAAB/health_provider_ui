<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn-MonitoringInvoice</name>
   <tag></tag>
   <elementGuidId>9e25a16a-26db-4a21-8b12-d4d33fd3235a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;card&quot;]/div/h4[text()='MONITORING INVOICE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;card&quot;]/div/h4[text()='MONITORING INVOICE']</value>
   </webElementProperties>
</WebElementEntity>
