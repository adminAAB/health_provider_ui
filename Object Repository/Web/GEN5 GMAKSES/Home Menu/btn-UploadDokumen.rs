<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn-UploadDokumen</name>
   <tag></tag>
   <elementGuidId>91c80d02-0cd1-434d-9df0-93b79813b761</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;card&quot;]/div/h4[text()='UPLOAD DOCUMENT']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;card&quot;]/div/h4[text()='UPLOAD DOCUMENT']</value>
   </webElementProperties>
</WebElementEntity>
