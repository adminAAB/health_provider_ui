<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn-EligibilityDischarge</name>
   <tag></tag>
   <elementGuidId>3c7eef6c-9ae4-464d-b807-9be2a100afff</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id=&quot;card&quot;]/div/h4[text()='ELIGIBILITY &amp; DISCHARGE']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@id=&quot;card&quot;]/div/h4[text()='ELIGIBILITY &amp; DISCHARGE']</value>
   </webElementProperties>
</WebElementEntity>
