<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>icon-MenuNavigation</name>
   <tag></tag>
   <elementGuidId>2375d327-5a0b-4849-a120-031e7b65c1f5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@class='sidebar-toggle' and @role='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[@class='sidebar-toggle' and @role='button']</value>
   </webElementProperties>
</WebElementEntity>
