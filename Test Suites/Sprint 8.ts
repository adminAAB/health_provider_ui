<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint 8</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>120</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>b1c34646-409c-46d3-8f12-e41b7e7b74d7</testSuiteGuid>
   <testCaseLink>
      <guid>1bb17efc-c4e9-4bba-b6a6-74a8e8b40f90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00001 - Cek detail screen daftar transaksi, validasi dan verifikasi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ed47fa6-279d-494a-91c3-afb9fbe15a62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00004 - Cek validasi not eligible (polis expired)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b148c09-1bd5-4ee0-ba5e-7e28d5adceea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00003 - Cek validasi not eligible (nomor peserta tidak terdaftar di DB)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b03f04aa-a5ca-402a-92f8-49d9d18edd9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00002 - Cek validasi eligible (nomor peserta valid dan belum daftar)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>347253a1-de5e-4532-9ad1-c27624f3b60a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00006 - Cek behaviour aplikasi dan detail summary eligibility jenis perawatan invalid</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e683b79-5b9e-4a5b-b54d-6846800876c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00007 - Cek validasi not eligible (sudah terdaftar status E di hari H)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d6962b8-65b3-41b3-84a0-19267c548397</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00008 - Cek validasi eligible (sudah daftar sebelum hari H status E)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a0cf6ab-e95f-4975-8f1d-78d62280c9a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00010 - Cek behavour aplikasi dan detail summary skenario limit muncul</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0f6bee3-48e0-4333-b991-79ae144506df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/00002 - Cek kesesuaian kuitansi dan data utk skenario discharge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d89ee68-97c5-4472-a76f-4f78195e87fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/00001 - Cek detail screen discharge dan validasi nomor screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
