<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Tes Eligibility Discharge</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>120</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fac536a5-f2ac-4ee7-9620-7debd9064c85</testSuiteGuid>
   <testCaseLink>
      <guid>16ea4281-7700-453f-9c5d-18e3b26b4661</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/00002 - Cek validasi eligible (nomor peserta valid dan belum daftar)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>839c9d29-13ad-4551-a09c-292ca8d25521</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/00002 - Cek kesesuaian kuitansi dan data utk skenario discharge</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
