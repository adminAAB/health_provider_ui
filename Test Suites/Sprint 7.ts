<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint 7</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>120</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ed7b6bff-9d27-4c3e-835a-1341aa1b4015</testSuiteGuid>
   <testCaseLink>
      <guid>f9a3cf03-202a-4301-814e-60d32b5088bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Upload Document/00001 - Cek detail screen upload dokumen dan searching data</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97c0c74c-c890-4e1b-90ee-a68d6bf04501</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Upload Document/00002 - Cek validasi field data dan behaviour button clear</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7dc9903-1602-42af-9219-2700fd12e00c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Upload Document/00003 - Cek detail screen upload dan cek upload, delete</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c96fb6e-d598-406f-aa66-b2cac811f775</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Upload Document/00004 - Cek behaviour button next, prev, last, first page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8a47309-fbce-4fdf-bc58-6fe3bea63bf9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GMAKSES/Upload Document/00005 - Cek behaviour ketika klik grouping dan pembentukan data</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
