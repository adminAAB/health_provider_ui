<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint 10</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>120</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>3791ce1d-7e24-4e39-afc2-da4f0a53faac</testSuiteGuid>
   <testCaseLink>
      <guid>1b74c72b-e7f9-4570-b3af-8e1cfe86c869</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/01 - Cek layout, behaviour app</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a830030-25fb-4040-9a75-f371db706405</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/02 - Cek data saat paging</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cee897da-3f5d-4c8e-aba0-9868430b5b4d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/03 - Cek data saat searching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e54886cb-65f5-4764-a674-3bf3dbe6e78c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/04 - Cek validasi mandatory upload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed020ae7-68fe-4813-a5d6-efb0ad78076e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/05 - Cek validasi tipe size upload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d2721df-b2bb-44cb-9854-c854cd4226f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/06 - Cek flow upload document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb5cb444-6538-4c6b-bec7-0d79e4b32212</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/07 - Cek list document</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
