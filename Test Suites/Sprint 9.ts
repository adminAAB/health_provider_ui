<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Sprint 9</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>120</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ecf23b57-cf5b-4cd5-98bd-81591bfc6646</testSuiteGuid>
   <testCaseLink>
      <guid>1eea64b7-b70e-4be5-af2f-88105694bbd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/09 - Cek data saat submit</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aee7d0f9-becd-46a2-a6de-4e0e66460305</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/03 - Cek data saat searching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c59e860b-06bb-47f3-a895-3691fbc81fa2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/05 - Cek validasi tipe size upload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1509a05-2955-494f-b098-719e72af37ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/01 - Cek layout, behaviour app</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d12e5ccf-418e-4862-b910-1dec68830590</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/02 - Cek data saat paging</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d743584-9c09-4d08-a173-50b11c2b5428</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/04 - Cek validasi mandatory upload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ac5cbf7-2bc8-4155-a877-036dce44fee4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/06 - Cek flow upload document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ef4ec68-d313-4b10-becf-1e78478e2c92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/07 - Cek list document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f7c23cf-6abc-43f3-93b8-04e995a1b06a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenarios/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/08 - Cek flow delete document</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
