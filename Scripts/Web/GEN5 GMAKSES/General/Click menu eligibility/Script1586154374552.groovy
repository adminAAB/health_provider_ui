import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (skenario == "GEN5"){
	
	WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GMAKSES/Homepage/BTN_Menu_Eligibility'), 120)
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Homepage/BTN_Menu_Eligibility'))
	
} else if (skenario == "REACT"){

	WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GMAKSES/Home Menu/btn-EligibilityDischarge'), 5)
	
	WebUI.waitForElementClickable(findTestObject('Web/GEN5 GMAKSES/Home Menu/btn-UploadDokumen'), 5)
	
	if (menu == "daftar transaksi") {
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Home Menu/btn-EligibilityDischarge'))
		
	} else if (menu == "uploaddokumen") {
	
		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Home Menu/btn-UploadDokumen'))
	
	} else if (menu == "statuspembayaran") {
	
		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Home Menu/btn-MonitoringInvoice'))
	
	}

}




