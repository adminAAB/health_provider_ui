import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

//WebUI.callTestCase(findTestCase('Web/Health Provider/Login/Flow Login'), [('password') : GlobalVariable.password, ('userid') : 'a.RumahSakit@gmail.com'
//	, ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)
//
//WebUI.callTestCase(findTestCase('null'), [('menu') : 'riwayat transaksi'], FailureHandling.STOP_ON_FAILURE)
//
//CustomKeywords.'code.DaftarKlaim.GetDataClaim'()

//String query = "SELECT * FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK"
//
//println (REA.getAllDataDatabase("172.16.94.48", "LITT", query))

//CustomKeywords.'code.GetAllData.convertdateDB'("01-04-2020")

//CustomKeywords.'code.DaftarKlaim.GetDataClaim'("allpage")


//CustomKeywords.'code.DaftarKlaim.CentangDataClaim'()

//println (CustomKeywords.'code.DaftarKlaim.GetColumnNameTable'(findTestObject('Web/Health Provider/Upload Dokumen/Daftar Klaim/TBL_Claim')))

//WebUI.callTestCase(findTestCase('Web/Health Provider/Upload Dokumen/Data Preparation'), [('QueryInsertTable') : 'INSERT INTO BEYONDMOSS.LiTT.dbo.GardaMedikaAkses_DaftarKlaim\r\nSELECT  ID,CNO, ProviderId,MemberName, [From], [To], aa.Description, D2.Description , NominalInput, NominalApproved, NominalRejected FROM(\r\nSELECT  CE.ID ,CNO,\r\n        ProviderId ,\r\n        MemberName ,\r\n\t\tHCE.EntryDt AS [From],\r\n        HCE2.EntryDt AS [TO],\r\n        MG.[Description] ,\r\n        NominalInput,\r\n        NominalApproved ,\r\n        NominalRejected ,\r\n\t\tD.Diagnosis\r\nFROM    GardaMedikaAkses.Claim_Eligibility AS CE\r\n        INNER JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.ID = CE.ID AND HCE.ResultStatusClaim = \'D\'\r\n\t\tINNER JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE2 ON HCE2.ID = CE.ID AND HCE2.ResultStatusClaim = \'E\'\r\n        INNER JOIN GardaMedikaAkses.Mst_General AS MG ON Code = TreatmentType\r\n        INNER JOIN GardaMedikaAkses.Dtl_Claim_Transaction AS DCT ON DCT.ID = CE.ID\r\n        INNER JOIN dbo.Diagnosis AS D ON Diagnosis = PrimaryDiagnosisCode\r\n        LEFT JOIN GardaMedikaAkses.Grouping_Claim_Transaction AS GCT ON GCT.ID = CE.ID\r\nWHERE   CE.ResultStatusClaim = \'D\'\r\n        AND GCT.ID IS NULL\r\nGROUP BY CE.ID ,CNO,\r\n        MemberName ,\r\n        HCE.EntryDt , HCE2.EntryDt,\r\n        MG.Description ,\r\n        NominalApproved ,\r\n        NominalRejected ,\t\t \r\n        ChargeFieldName ,\r\n\t\tNominalInput,\r\n        ProviderId,\r\n\t\tD.Diagnosis\r\n) aa\r\nINNER JOIN dbo.Diagnosis AS D2 on D2.diagnosis = aa.diagnosis'
//	, ('QueryTruncateTable') : 'TRUNCATE TABLE GardaMedikaAkses_DaftarKlaim'], FailureHandling.STOP_ON_FAILURE)


//CustomKeywords.'code.General.chooseDate'("17 Mar 2020")

//import java.text.SimpleDateFormat

//Add to the body of test case
//SimpleDateFormat sdf = new SimpleDateFormat('MM/dd/yyyy');
//Calendar c = Calendar.getInstance();
//c.setTime(new Date()); //Use today’s date
//c.add(Calendar.DATE, 2); //Adds # of days
////c.add(Calendar.YEAR, 2); //Adds years
//String currentDate = sdf.format(c.getTime());
//System.out.println("Future Date (MM/DD/YYYY): " + currentDate);


//import java.util.Date


//ArrayList tes = ['a','b','d']
//
//tes.add(0, 'c')
//
//println (tes)


//
//ArrayList a = ['a', 'b', 'c', 'd']

//
// 
//
//ArrayList d = ["0", "Sys"]
//String x = "445273"
//ArrayList c = new ArrayList()
//ArrayList a = [1, 4, 5, 6, 7, 8, 9, 10, 28, 113]
//
//int i
//
//for (i = 0 ; i < a.size() ; i++) {
//	ArrayList b = new ArrayList()
//	b.add(x)
//	b.add(a[i])	
//	
//	for (aa = 0; aa < d.size(); aa++){
//		
//		b.add(d[aa])
//	}
//	c.add(b)
//}
//
// 
//
//println (c)


//ArrayList dataimage = ["-1", "aaa", "0", "Sys"]
//
//ArrayList xx = new ArrayList()
//int x = 4
//
//for (a = 0; a < x; a++){
//	
//	
//	
//	xx.add(dataimage)
//	
//} 
//
//println (xx)

//ArrayList a = [1, 4, 5, 6, 7, 8, 9, 10, 28, 113]
//
//String b
//
//for (i = 0; i < a.size(); i++){
//	
//	b += a[i] + ","
//	
//}
//
//println (b)


//String penampungID = ""
//
//GlobalVariable.dataDinamis = [1, 4, 5, 6, 7, 8, 9, 10, 28, 113]
//
//println (GlobalVariable.dataDinamis.size()-1)
//int j
//
//for (j = 0 ; j < GlobalVariable.dataDinamis.size() ; j++) {
//
//	if (j == GlobalVariable.dataDinamis.size()-1) {
//		
//
//		penampungID += "'" + GlobalVariable.dataDinamis[j] + "'"
//		
//		println (penampungID)
//	} else {
//
//		println (GlobalVariable.dataDinamis[j])
//		penampungID += "'" + GlobalVariable.dataDinamis[j] + "',"
//		
//		println ("aa : " +penampungID)
//	}
//}
//
//println (penampungID)
//
//penampungID.replace("null'", "'")
//
//println (penampungID)


//ArrayList SourcePCB = [0,0,1,'GMAKSES','GMAKSES',0,0,'',0,'','O',0,'GMAKSES - DataUpload' ]//RegID,RegID,PCB[8],GlobalVariable.providerID,'',PCB[1],PCB[5],PCB[2],PCB[3],PCB[4],PCB[6],PCB[7]]
//
//println (SourcePCB)


//ArrayList a = [['MOHAMAD SUKRON', '18 Mar 2020 - 18 Mar 2020', 'Rawat Jalan', 'PARATYPHOID FEVER A', 210, 210, 0]]
//
//ArrayList b = a[0]
//
//
//println (b)

String a = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fermentum dolor ligula, id consequat lectus rhoncus sagittis. Integer pellentesque, ex non pellentesque blandit, nibh felis porttitor elit, nec malesuada dolor mauris ut odio. Proin at est quam. Donec eleifend elit vulputate, pharetra odio ut, eleifend elit. Nunc a sapien at nibh semper pharetra. Duis aliquet varius mi sed commodo. Aliquam aliquam, sapien nec placerat congue, sem tortor sodales nibh, vel interdum ipsum dui ut metus. Donec tortor orci, auctor et enim eget, consectetur tristique ligula.'

println (a.length())
String b = a.substring(0, 500)
String c = a.substring(0, 499)
println (b)
println (c)

