import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


//if (login == "NonQR"){
//	
//	WebUI.openBrowser(GlobalVariable.URLGardaAkses)	
//	
//	WebUI.switchToFrame(findTestObject('Web/GEN5 GA/Frame'), 1)
//	
//	WebUI.waitForElementClickable(findTestObject('Web/GEN5 GMAKSES/Login/BTN_MasukGA'), 120)
//	
//	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_UserIDGA'), userid)
//	
//	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_PasswordGA'), password)
//	
//	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Login/BTN_MasukGA'))
//	
//	GlobalVariable.UserID = userid
//	
//	GlobalVariable.providerID = REA.getValueDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryUserProviderLogin.replace("GVUserID", userid), "ProviderID")
//	
//} else if (login == "QR"){
//
//	GEN5.AccessURLwithPlugin(GlobalVariable.URLGardaAkses, "QR Code Reader.crx")
//	
//	WebUI.switchToFrame(findTestObject('Web/GEN5 GA/Frame'), 1)
//	
//	WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Login/TXT_UserIDGA'), 30)
//	
//	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_UserIDGA'), userid)
//	
//	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_PasswordGA'), password)
//	
//	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Login/BTN_MasukGA'))
//
//	GlobalVariable.UserID = userid
//
//	GlobalVariable.providerID = REA.getValueDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryUserProviderLogin.replace("GVUserID", userid) , "ProviderID")
//	
//} else if (login == "NonQRDisharge"){
//	
//	WebUI.openBrowser(GlobalVariable.URLGardaAkses)	
//	
//	GlobalVariable.providerID = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryProviderIDLoginDischarge,'ProviderID')	
//	
//	GlobalVariable.UserID = REA.getValueDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryUserIDLoginDischarge.replace("GVproviderID", GlobalVariable.providerID), "UserID")
//	
//	WebUI.switchToFrame(findTestObject('Web/GEN5 GA/Frame'), 1)
//	
//	WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Login/TXT_UserIDGA'), 30)
//	
//	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_UserIDGA'), GlobalVariable.UserID)
//	
//	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_PasswordGA'), 'P@ssw0rd')
//	
//	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Login/BTN_MasukGA'))
//	
//}	

if (login == "NonQR"){
	
	WebUI.openBrowser(GlobalVariable.urlHealth)
	
	WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Login/txt-UserID'), 30)
	
	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/txt-UserID'), userid)
	
	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/txt-Password'), password)

	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Login/btn-LogIn'))
	
	GlobalVariable.UserID = userid
	
	println (GlobalVariable.QueryUserProviderLogin.replace("GVUserID", userid))
	
	GlobalVariable.ProviderID = REA.getValueDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryUserProviderLogin.replace("GVUserID", userid), "ProviderID")
	
} else if (login == "QR"){

	GEN5.AccessURLwithPlugin(GlobalVariable.urlHealth, "QR Code Reader.crx")
	
	WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Login/txt-UserID'), 30)
	
	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/txt-UserID'), userid)
	
	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/txt-Password'), password)

	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Login/btn-LogIn'))

	GlobalVariable.UserID = userid

	println (GlobalVariable.QueryUserProviderLogin.replace("GVUserID", userid))
	
	GlobalVariable.ProviderID = REA.getValueDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryUserProviderLogin.replace("GVUserID", userid) , "ProviderID")
	
	println (GlobalVariable.ProviderID)
	
} else if (login == "NonQRDisharge"){
	
	WebUI.openBrowser(GlobalVariable.URLGardaAkses)
	
	GlobalVariable.ProviderID = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryProviderIDLoginDischarge,'ProviderID')
	
	GlobalVariable.UserID = REA.getValueDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryUserIDLoginDischarge.replace("GVproviderID", GlobalVariable.providerID), "UserID")
	
	WebUI.switchToFrame(findTestObject('Web/GEN5 GA/Frame'), 1)
	
	WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Login/TXT_UserIDGA'), 30)
	
	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_UserIDGA'), GlobalVariable.UserID)
	
	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Login/TXT_PasswordGA'), 'P@ssw0rd')
	
	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Login/BTN_MasukGA'))
	
}






