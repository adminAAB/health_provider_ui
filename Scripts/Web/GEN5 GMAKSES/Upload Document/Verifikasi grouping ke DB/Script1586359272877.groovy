import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable



String RegID = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetNoBatchPCB, "SeqNo")

ArrayList sourceGCR = new ArrayList()

if (GlobalVariable.DataParamS2.length() > 500){
	
	GlobalVariable.DataParamS2 = GlobalVariable.DataParamS2.substring(0, 499)
	
	sourceGCR = [RegID, GlobalVariable.DataParamS2, "1", "Sys"]
	
} else {

	sourceGCR = [RegID, GlobalVariable.DataParamS2, "1", "Sys"]
	
}

ArrayList sourceGCT = new ArrayList()

ArrayList sourceGCI = new ArrayList()

String penampungID = ""

String penampungCNO = ""

if (cek == "1data"){
	
	penampungID = GlobalVariable.DataParamL2[0]
	
	println (penampungID)
	
	sourceGCT = [RegID, penampungID, "1", "Sys" ]
	
	ArrayList countClaimImage = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertGCI.replace("GVID",""+ penampungID +""))
	
	println(countClaimImage)
	
	if (countClaimImage != null){
	
		ArrayList baseGCI = [RegID, penampungID, "1", "Sys" ]
		
		for (i = 0; i < countClaimImage.size(); i++){
			
			sourceGCI.add(baseGCI)
			
		}
		
		println (GlobalVariable.QueryDataInsertGCI.replace("GVID",""+ penampungID +""))
		
		REA.compareAllDatabasetoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertGCI.replace("GVID",""+ penampungID +""), sourceGCI)
	}
	
	ArrayList screenGCR = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertGCR)
	
	println (sourceGCI)
	
	println (sourceGCR)
	
	println (sourceGCR[1].length())
	
	println (sourceGCT)
	
	WebUI.verifyMatch(screenGCR.size().toString(), sourceGCR.size().toString(), false)
	
	for (j = 0; j < sourceGCR.size(); j++){
		
		if (screenGCR[j] == sourceGCR[j]) {
			
			KeywordUtil.markPassed("Value " + screenGCR[j] +" from screen same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from screen = " + screenGCR[j] + " has different Value from database = " + sourceGCR[j])
		}
	}
	
	ArrayList ScreenGCT = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertGCT)
	
	println (ScreenGCT)
	
	println (sourceGCR)	

	WebUI.verifyMatch(ScreenGCT.size().toString(), sourceGCT.size().toString(), false)
	
	for (aa = 0; aa < sourceGCT.size(); aa++){
		
		if (ScreenGCT[aa] == sourceGCT[aa]) {
			
			KeywordUtil.markPassed("Value " + ScreenGCT[aa] +" from screen same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from screen = " + ScreenGCT[aa] + " has different Value from database = " + sourceGCT[aa])
		}
		
	}
	
	ArrayList SourceCount = REA.getOneRowDatabase("172.16.94.48", "LiTT", GlobalVariable.QuerySumAmountGrouping.replace("GVProviderID",GlobalVariable.ProviderID).replace("GVID", penampungID))
	
	ArrayList SourceDataPCB = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetProviderInfo.replace("GVProviderID",GlobalVariable.ProviderID))
	
	ArrayList SourcePCB = ['0','0','1','GMAKSES','GMAKSES','0','0.0','','0.0','','O','0','Created with GardaMedikaAkses - DataUpload' ,SourceCount[0],SourceCount[2].toFloat().toString(),RegID.toString(),RegID.toString(),SourceDataPCB[8].toString(),GlobalVariable.providerID,'',SourceDataPCB[1].toString(),SourceDataPCB[5].toString(),SourceDataPCB[2].toString(),SourceDataPCB[3].toString(),SourceDataPCB[4].toString(),SourceDataPCB[6].toString(),SourceDataPCB[7].toString()]
	
	println (SourcePCB)
	
	ArrayList ScreenPCB = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertPCB.replace("GVID", RegID))
	
	println ("ScreenPCB : "+ScreenPCB)
	
	println ("SourcePCB : "+SourcePCB)
	
	WebUI.verifyMatch(ScreenPCB.size().toString(), SourcePCB.size().toString(), false)
	
	for (aa = 0; aa < SourcePCB.size(); aa++){
	
		if (ScreenPCB[aa] == SourcePCB[aa]) {
	
			KeywordUtil.markPassed("Value " + ScreenPCB[aa] +" from screen same with Database.")
	
		} else {
	
			KeywordUtil.markFailedAndStop("Value from screen = " + ScreenPCB[aa] + " has different Value from database = " + SourcePCB[aa])
		}
	
	}
	
	ArrayList SourceClaimH = new ArrayList()
	
	println (penampungID)
	
	String CNO = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryGetCNO, "SeqNo")
	
	String ClaimNo = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryGetClaimNo,"ClaimNo")
	
	ArrayList SourceNominalDataClaimH = REA.getOneRowDatabase("172.16.94.48", "LiTT", GlobalVariable.QuerySourceDataInsertCH.replace("GVCNO",CNO))
	
	GlobalVariable.DataParamL3 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataEligibilityLiTT.replace("GVID", "'"+ GlobalVariable.DataParamL2[0] + "'"))
	
	ArrayList PolicyMemberInfo = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryGetPolicyMemberInfo.replace("GVProdType",GlobalVariable.DataParamL3[12]).replace("GVMemberNo", GlobalVariable.DataParamL3[0]))
	
	ArrayList ProviderInfo = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryGetProviderInfo.replace("GVProviderID",GlobalVariable.ProviderID))
	
	ArrayList SourceClaimH = ['','','','','','','','','','','','','','','V','I','0','0','','ONLINE','GMAKSES','GMAKSES','1','','0','0','0','0','0','1','-1','0','0','0','0','0','0','0','0','0','0','0','0','INA','0','0','0','0','P','','','','IDR','','0','0','0','','P','C','0','0','null','0','0','1','','','0','null','','1','null','','0','0','','','','','','','','null','GMAKSES','null','','','','','','0','','','','0','0','0','','','','','','0',ProviderInfo[6],ProviderInfo[7],CNO,ClaimNo,GlobalVariable.ProviderID,GlobalVariable.ProviderID,PolicyMemberInfo[2],GlobalVariable.DataParamL3[1],GlobalVariable.DataParamL3[17],SourceNominalDataClaimH[5],SourceNominalDataClaimH[6],PolicyMemberInfo[1],'',PolicyMemberInfo[3],CNO,ProviderInfo[1],ProviderInfo[5],ProviderInfo[2],ProviderInfo[3],ProviderInfo[4],GlobalVariable.DataParamL3[12],ProviderInfo[0],PolicyMemberInfo[2],SourceNominalDataClaimH[3].toFloat().toString(),'0.0', SourceNominalDataClaimH[2].toFloat().toString(), SourceNominalDataClaimH[3].toFloat().toString(), SourceNominalDataClaimH[4].toFloat().toString()]

	//SourceClaimH =     ['', '', '', '', '', '', '', '', '', '', '', '', '', '', 'V', 'I', '0', '0', '', 'ONLINE', 'GMAKSES','GMAKSES', '0', '', '0', '1', '-1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0.0', '0.0', '0.0', 'INA', '0.0', '0.0', '0.0', '0', 'P', '', '', '', 'IDR', '', '0.0', '0.0', '0.0', '', 'P', 'C', '0', '0', 'null', '0.0', '0.0', '1.0', '', '', '0.0', 'null', '', '1.0', 'null', '', '0.0', '0.0', '', '', '', '', '', '', '', 'GMAKSES','null', '', '', '', '', '', '0', '', '', '', '0', '0', '0', '', '', '', '', '', '0',DataCNO[0], RegID, SourceDataPCB[1], SourceDataPCB[5], SourceDataPCB[2], SourceDataPCB[3], SourceDataPCB[4], SourceDataPCB[0], SourceDataPCB[6], SourceDataPCB[7], DataCNO[3].toFloat().toString(),'0.0', DataCNO[2].toFloat().toString(), DataCNO[3].toFloat().toString(), DataCNO[4].toFloat().toString()]
		
	ArrayList DetAmountBenefit = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySourceDataInsertCDE.replace("GVCNO",CNO))
	
	ArrayList SourceCDE = new ArrayList() //claimdetail
	
	for (a = 0; a < DetAmountBenefit.size(); a++){
		
		ArrayList CDE = ['','1' ,'0','0','0','','','V','0','1900-01-01 00:00:00:000','0','0','0','0','0','','0','0','0','0','0',CNO,[a], (DetAmountBenefit[a])[2], SourceNominalDataClaimH[5], SourceNominalDataClaimH[6], DetAmountBenefit[3] , DetAmountBenefit[4], DetAmountBenefit[4], DetAmountBenefit[5]]
		
		SourceCDE.add(CDE)
		
	}
	
	println ("SourceCDE: "+SourceCDE)
	
	ArrayList SourceCDI = new ArrayList() //claim_diagnosis
	
	ArrayList DiagnosisID = new ArrayList()
	
	DiagnosisID.add(GlobalVariable.DataParamL2[27])
	
	if (GlobalVariable.DataParamL2[28] != 'null'){
		
		ArrayList dataDiagnosa = GlobalVariable.DataParamL2[28].split(",")
		
		for (c = 0; c < dataDiagnosa.size(); c++){
			
			DiagnosisID.add(dataDiagnosa[c])
			
		}
		
	}
	
	for (i = 0; i < DiagnosisID.size(); i++){
		
		ArrayList CDI = [CNO, DiagnosisID[i], '', '0']
		
		SourceCDI.add(CDI)
	}
	
	println ("SourceCDI: "+SourceCDI)
	
	ArrayList SourceCEH = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySourceDataInsertCEH.replace("GVCNO", CNO))
	
//		String QuerySourceEditHistory = "SELECT ISnull((SELECT MAX(ClaimHistoryID) + 1 FROM ClaimEditHistory WHERE CNO = '"+ (DataCNO[bb])[0] +"'),1),GETDATE(), 'GMAKSES', [CNO],[ClaimNo],[ID],[ClaimDate],[ClientNo],[MNO],[ProductID],[Start],[Finish],[Hospital],[Doctor],[Anamnesis], [FDiagnosis],[SDiagnosis],[DDiagnosis],[LDiagnosis],[Symptoms],[DiseaseH], [Physical],[Consultation],[Treatment],[Suggestion],[Therapist],[Status], [ClaimType],[Exclude],[Reimburse],[EDescription],[CCategory],[PNo],[FDate], [FUser],[LDate],[LUser],[HoldF],[Remarks],[Billed],[Verified],[Accepted], [Excess],[PreRegID],[RegID],[ProviderF],[SeqNo],[ExcFDiagnosisF], [ExcSDiagnosisF],[ExcDDiagnosisF],[ExcLDiagnosisF],[PreExistFDiagnosisF], [PreExistExcSDiagnosisF],[PreExistDDiagnosisF],[PreExistLDiagnosisF], [CoAsAmount],[TotalDiscount],[TotalLoading],[Country],[InsuredAmount], [InsuranceAmount],[Deductible],[branch],[IntSeqNo],[OCNO],[CorrespondenceType], [CorrespondenceID],[RefNo],[TerminalID],[Currency],[refPaymentDate], [RefPaymentID],[ManualVerified],[ManualAccepted],[ManualExcess], [refBatchId],[Payto],[ExcessPayor],[ASOF],[AlreadyReimburseASOF],[AccountNo], [BankAccount],[BankName],[ProductType],[EstDiscardDate],[Unpaid],[Discount], [rate],[TreatmentPlace],[ExcessPayorID],[CReason],[Reason],[Administration], [ApprovalDate],[ApprovalID],[PRate],[RDate],[RUser],[Reimbursementpct], [StampDuty],[EDCMerchantID],[EDCTID],[EDCBatchNoIN],[EDCTrcIDIN], [EDCBatchNoOUT],[EDCTrcIDOUT],[EDCApprovalCode],[Vdate],[Vuser],[InvoiceExcessDate], [InvoiceExcessNo],[ReceiptDate],[EDCTrIDIN],[EDCTrIDOUT],[edctidin],[edctidout], [BatchUploadNo],[ACCOUNTNO_EXCESS],[BANKACCOUNT_EXCESS],[BANKNAME_EXCESS], [AdjustF],[ExGratiaF],[Ftime],[LTime],[FollowUpF],[OtherTreatmentPlace], [TreatmentRemarks],[edctidid],[edcidout],[FollowUpDate],[FollowUpUser], [FollowingUpF], [Bank],[BankBranch],[Nationality],[AccountType] FROM ClaimH WHERE CNO = '"+ (DataCNO[bb])[0] +"'"
//		
//		ArrayList SourceEHistory = REA.getOneRowDatabase("172.16.94.70", "SEA", QuerySourceEditHistory)
//		
//		String QueryScreenEditHistory = "SELECT [ClaimHistoryID],[Edit_Date],[Edit_User],[CNO],[ClaimNo],[ID],[ClaimDate], [ClientNo],[MNO],[ProductID],[Start],[Finish],[Hospital],[Doctor],[Anamnesis], [FDiagnosis],[SDiagnosis],[DDiagnosis],[LDiagnosis],[Symptoms],[DiseaseH], [Physical],[Consultation],[Treatment],[Suggestion],[Therapist],[Status], [ClaimType],[Exclude],[Reimburse],[EDescription],[CCategory],[PNo],[FDate], [FUser],[LDate],[LUser],[HoldF],[Remarks],[Billed],[Verified],[Accepted], [Excess],[PreRegID],[RegID],[ProviderF],[SeqNo],[ExcFDiagnosisF], [ExcSDiagnosisF],[ExcDDiagnosisF],[ExcLDiagnosisF],[PreExistFDiagnosisF], [PreExistExcSDiagnosisF],[PreExistDDiagnosisF],[PreExistLDiagnosisF], [CoAsAmount],[TotalDiscount],[TotalLoading],[Country],[InsuredAmount], [InsuranceAmount],[Deductible],[branch],[IntSeqNo],[OCNO],[CorrespondenceType], [CorrespondenceID],[RefNo],[TerminalID],[Currency],[refPaymentDate], [RefPaymentID],[ManualVerified],[ManualAccepted],[ManualExcess], [refBatchId],[Payto],[ExcessPayor],[ASOF],[AlreadyReimburseASOF],[AccountNo], [BankAccount],[BankName],[ProductType],[EstDiscardDate],[Unpaid],[Discount], [rate],[TreatmentPlace],[ExcessPayorID],[CReason],[Reason],[Administration], [ApprovalDate],[ApprovalID],[PRate],[RDate],[RUser],[Reimbursementpct], [StampDuty],[EDCMerchantID],[EDCTID],[EDCBatchNoIN],[EDCTrcIDIN], [EDCBatchNoOUT],[EDCTrcIDOUT],[EDCApprovalCode],[Vdate],[Vuser],[InvoiceExcessDate], [InvoiceExcessNo],[ReceiptDate],[EDCTrIDIN],[EDCTrIDOUT],[edctidin],[edctidout], [BatchUploadNo],[ACCOUNTNO_EXCESS],[BANKACCOUNT_EXCESS],[BANKNAME_EXCESS], [AdjustF],[ExGratiaF],[Ftime],[LTime],[FollowUpF],[OtherTreatmentPlace], [TreatmentRemarks],[edctidid],[edcidout],[FollowUpDate],[FollowUpUser], [FollowingUpF], [Bank],[BankBranch],[Nationality],[AccountType] FROM dbo.ClaimEditHistory AS CEH WHERE CNO = '"+ (DataCNO[bb])[0] +"'' AND ClaimHistoryID = (SELECT MAX(ClaimHistoryID) FROM ClaimEditHistory WHERE CNO = '"+ (DataCNO[bb])[0] +"'')"
//		
//		ArrayList ScreenEHist = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryScreenEditHistory)
//		
//		for (i = 0; i < SourceEHistory.size(); i++){
//			
//			if (ScreenEHist[i] == SourceEHistory[i]) {
//				
//				KeywordUtil.markPassed("Value " + ScreenEHist[i] +" from screen same with Database.")
//				
//			} else {
//				
//				KeywordUtil.markFailedAndStop("Value from screen = " + ScreenEHist[i] + " has different Value from database = " + SourceEHistory[i])
//			}
//		}
		
	REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCH.replace("GVCNO",CNO), SourceClaimH)
		
	REA.compareAllDatabasetoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCDE.replace("GVCNO",CNO), SourceCDE)
		
	REA.compareAllDatabasetoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCDI.replace("GVCNO",CNO), SourceCDI)
		
	REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCEH.replace("GVCNO",CNO), SourceCEH)
		
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Ok_Notifikasi1'))
		
} else if (cek == "1page"){

	int i
	
	ArrayList c = ["1", "Sys"]
	
	for (i = 0 ; i < GlobalVariable.DataParamL2.size() ; i++) {
		
		ArrayList b = new ArrayList()
			
		b.add(RegID)
		
		b.add(GlobalVariable.DataParamL2[i])
		
		for (x = 0; x < c.size(); x++){
			
			b.add(c[x])
		}
		
		sourceGCT.add(b)
	}	
	
	int j
	
	for (j = 0 ; j < GlobalVariable.DataParamL2.size() ; j++) {
	
		if (j == GlobalVariable.DataParamL2.size()-1) {
	
			penampungID += "'" + GlobalVariable.DataParamL2[j] + "'"
		} else {
	
			penampungID += "'" + GlobalVariable.DataParamL2[j] + "',"
		}
	}
	
	println (penampungID)
	
	QueryClaimImage = "SELECT BatchID, ID, Status,EntryUsr FROM [GardaMedikaAkses].[Grouping_Claim_Image] WHERE ID IN ("+ penampungID +") AND status = '1'"
	
	println (QueryClaimImage)
	
	ArrayList countClaimImage = REA.getAllDataDatabase("172.16.94.70", "SEA", QueryClaimImage)
	
	if (countClaimImage != null){
	
		int ii
		
		ArrayList cc = ["1", "Sys"]
		
		for (ii = 0 ; ii < countClaimImage.size(); ii++) {
			
			ArrayList bb = new ArrayList()
				
			bb.add(RegID)
			
			bb.add((countClaimImage[ii])[1])
			
			for (y = 0; y < cc.size(); y++){
				
				bb.add(cc[y])
			}
			
			sourceGCI.add(bb)
		}	
		
		println (countClaimImage)
		println (sourceGCI)
	
		REA.compareAllDatabasetoArray("172.16.94.70", "SEA", QueryClaimImage, sourceGCI)
	}
	
	println (sourceGCR)
	
	ArrayList screenGCR = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertGCR)
	
	println (screenGCR)
	
	println (sourceGCT)
	
	WebUI.verifyMatch(screenGCR.size().toString(), sourceGCR.size().toString(), false)
	
	for (j = 0; j < sourceGCR.size(); j++){
		
		if (screenGCR[j] == sourceGCR[j]) {
			
			KeywordUtil.markPassed("Value " + screenGCR[j] +" from screen same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from screen = " + screenGCR[j] + " has different Value from database = " + sourceGCR[j])
		}
	}
	
	REA.compareAllDatabasetoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertGCT, sourceGCT)

/*
 * verifikasi ke post_claim_batch
 * 
 */

ArrayList SourceCount = REA.getOneRowDatabase("172.16.94.48", "LiTT", GlobalVariable.QuerySumAmountGrouping.replace("GVProviderID",GlobalVariable.ProviderID).replace("GVID", penampungID))

ArrayList SourceDataPCB = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetProviderInfo.replace("GVProviderID",GlobalVariable.ProviderID))

ArrayList SourcePCB = ['0','0','1','GMAKSES','GMAKSES','0','0.0','','0.0','','O','0','Created with GardaMedikaAkses - DataUpload' ,SourceCount[0],SourceCount[2].toFloat().toString(),RegID.toString(),RegID.toString(),SourceDataPCB[8].toString(),GlobalVariable.providerID,'',SourceDataPCB[1].toString(),SourceDataPCB[5].toString(),SourceDataPCB[2].toString(),SourceDataPCB[3].toString(),SourceDataPCB[4].toString(),SourceDataPCB[6].toString(),SourceDataPCB[7].toString()]

println (SourcePCB)

ArrayList ScreenPCB = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertPCB.replace("GVID", RegID))

println ("ScreenPCB : "+ScreenPCB)

println ("SourcePCB : "+SourcePCB)

WebUI.verifyMatch(ScreenPCB.size().toString(), SourcePCB.size().toString(), false)

for (aa = 0; aa < SourcePCB.size(); aa++){
	
	if (ScreenPCB[aa] == SourcePCB[aa]) {
		
		KeywordUtil.markPassed("Value " + ScreenPCB[aa] +" from screen same with Database.")
		
	} else {
		
		KeywordUtil.markFailedAndStop("Value from screen = " + ScreenPCB[aa] + " has different Value from database = " + SourcePCB[aa])
	}
	
}

ArrayList SourceClaimH = new ArrayList()

println (GlobalVariable.dataDinamis)

for (bb = 0; bb < GlobalVariable.dataDinamis.size(); bb++){
	
	String QueryCNO = "SELECT CNO, ID, SUM(Total_Tagihan) Total_Tagihan, Tagihan_Dicover, Excess_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ID = '"+ GlobalVariable.dataDinamis[bb] +"' GROUP BY ID, CNO, Tagihan_Dicover, Excess_Tagihan"
	
	println (QueryCNO)
	
	ArrayList DataCNO = REA.getOneRowDatabase("172.16.94.48", "LiTT", QueryCNO)
	
	println ("DataCNO: "+ DataCNO)
		
	SourceClaimH = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', 'V', 'I', '0', '0', '', 'ONLINE', 'GMAKSES','GMAKSES', '0', '', '0', '1', '-1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0.0', '0.0', '0.0', 'INA', '0.0', '0.0', '0.0', '0', 'P', '', '', '', 'IDR', '', '0.0', '0.0', '0.0', '', 'P', 'C', '0', '0', 'null', '0.0', '1.0', '', '', '0.0', 'null', '', '1.0', 'null', '', '0.0', '0.0', '', '', '', '', '', '', '', 'GMAKSES','null', '', '', '', '', '', '0', '', '', '', '0', '0', '0', '', '', '', '', '', '0',DataCNO[0], RegID, SourceDataPCB[1], SourceDataPCB[5], SourceDataPCB[2], SourceDataPCB[3], SourceDataPCB[4], SourceDataPCB[0], SourceDataPCB[6], SourceDataPCB[7], DataCNO[3].toFloat().toString(),'0.0'.toString(), DataCNO[2].toFloat().toString(), DataCNO[3].toFloat().toString(), DataCNO[4].toFloat().toString()]
			
	String QueryScreenClaimH = "SELECT Hospital, Doctor, Anamnesis, FDiagnosis, SDiagnosis, DDiagnosis, LDiagnosis, Symptoms, DiseaseH, Physical, Consultation, Treatment, Suggestion, Therapist, Status, ClaimType, Exclude, Reimburse, EDescription, CCategory, FUser, LUser, HoldF, Remarks, PreRegID, ProviderF, PrevCNO, SeqNo, ExcFDiagnosisF, ExcSDiagnosisF, ExcDDiagnosisF, ExcLDiagnosisF, PreExistFDiagnosisF, PreExistExcSDiagnosisF, PreExistDDiagnosisF, PreExistLDiagnosisF, CoAsAmount, TotalDiscount, TotalLoading, Country, InsuredAmount, InsuranceAmount, Deductible, IntSeqNo, CorrespondenceType, CorrespondenceID, RefNo, TerminalID, Currency, RefPaymentID, ManualVerified, ManualAccepted, ManualExcess, refBatchId, Payto, ExcessPayor, ASOF, AlreadyReimburseASOF, EstDiscardDate, Discount, rate, CReason, Reason, Administration, ApprovalDate, ApprovalID, PRate, RDate, RUser, Reimbursementpct, StampDuty, EDCMerchantID, EDCTID, EDCBatchNoIN, EDCTrcIDIN, EDCBatchNoOUT, EDCTrcIDOUT, EDCApprovalCode, Vuser, InvoiceExcessDate, InvoiceExcessNo, EDCTrIDIN, EDCTrIDOUT, edctidin, edctidout, BatchUploadNo, ACCOUNTNO_EXCESS, BANKACCOUNT_EXCESS, BANKNAME_EXCESS, AdjustF, ExGratiaF, FollowUpF, OtherTreatmentPlace, TreatmentRemarks, edctidid, edcidout, FollowUpUser, FollowingUpF, CNO, RegID, AccountNo, BankAccount, BankName, Bank, BankBranch, TreatmentPlace, Nationality, AccountType, Verified, Excess, Billed, Accepted, Unpaid FROM dbo.ClaimH AS CH WHERE CNO = '"+ DataCNO[0] +"'"

	println (QueryScreenClaimH)
	
	ArrayList ScreenClaimH = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryScreenClaimH)
	
	println ("ScreenClaimH : "+ScreenClaimH)
	
	println ("SouceClaimH : "+SourceClaimH)
	
	WebUI.verifyMatch(ScreenClaimH.size().toString(), SourceClaimH.size().toString(), false)
	
	for (j = 0; j < SourceClaimH.size(); j++){
		
		if (ScreenClaimH[j] == SourceClaimH[j]) {
			
			KeywordUtil.markPassed("Value " + ScreenClaimH[j] +" from screen same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from screen = " + ScreenClaimH[j] + " has different Value from database = " + SourceClaimH[j])
		}
	}

	String QuerySourceEditHistory = "SELECT ISnull((SELECT MAX(ClaimHistoryID) + 1 FROM ClaimEditHistory WHERE CNO = '"+ (DataCNO[bb])[0] +"'),1),GETDATE(), 'GMAKSES', [CNO],[ClaimNo],[ID],[ClaimDate],[ClientNo],[MNO],[ProductID],[Start],[Finish],[Hospital],[Doctor],[Anamnesis], [FDiagnosis],[SDiagnosis],[DDiagnosis],[LDiagnosis],[Symptoms],[DiseaseH], [Physical],[Consultation],[Treatment],[Suggestion],[Therapist],[Status], [ClaimType],[Exclude],[Reimburse],[EDescription],[CCategory],[PNo],[FDate], [FUser],[LDate],[LUser],[HoldF],[Remarks],[Billed],[Verified],[Accepted], [Excess],[PreRegID],[RegID],[ProviderF],[SeqNo],[ExcFDiagnosisF], [ExcSDiagnosisF],[ExcDDiagnosisF],[ExcLDiagnosisF],[PreExistFDiagnosisF], [PreExistExcSDiagnosisF],[PreExistDDiagnosisF],[PreExistLDiagnosisF], [CoAsAmount],[TotalDiscount],[TotalLoading],[Country],[InsuredAmount], [InsuranceAmount],[Deductible],[branch],[IntSeqNo],[OCNO],[CorrespondenceType], [CorrespondenceID],[RefNo],[TerminalID],[Currency],[refPaymentDate], [RefPaymentID],[ManualVerified],[ManualAccepted],[ManualExcess], [refBatchId],[Payto],[ExcessPayor],[ASOF],[AlreadyReimburseASOF],[AccountNo], [BankAccount],[BankName],[ProductType],[EstDiscardDate],[Unpaid],[Discount], [rate],[TreatmentPlace],[ExcessPayorID],[CReason],[Reason],[Administration], [ApprovalDate],[ApprovalID],[PRate],[RDate],[RUser],[Reimbursementpct], [StampDuty],[EDCMerchantID],[EDCTID],[EDCBatchNoIN],[EDCTrcIDIN], [EDCBatchNoOUT],[EDCTrcIDOUT],[EDCApprovalCode],[Vdate],[Vuser],[InvoiceExcessDate], [InvoiceExcessNo],[ReceiptDate],[EDCTrIDIN],[EDCTrIDOUT],[edctidin],[edctidout], [BatchUploadNo],[ACCOUNTNO_EXCESS],[BANKACCOUNT_EXCESS],[BANKNAME_EXCESS], [AdjustF],[ExGratiaF],[Ftime],[LTime],[FollowUpF],[OtherTreatmentPlace], [TreatmentRemarks],[edctidid],[edcidout],[FollowUpDate],[FollowUpUser], [FollowingUpF], [Bank],[BankBranch],[Nationality],[AccountType] FROM ClaimH WHERE CNO = '"+ (DataCNO[bb])[0] +"'"
	
	ArrayList SourceEHistory = REA.getOneRowDatabase("172.16.94.70", "SEA", QuerySourceEditHistory) 
	
	String QueryScreenEditHistory = "SELECT [ClaimHistoryID],[Edit_Date],[Edit_User],[CNO],[ClaimNo],[ID],[ClaimDate], [ClientNo],[MNO],[ProductID],[Start],[Finish],[Hospital],[Doctor],[Anamnesis], [FDiagnosis],[SDiagnosis],[DDiagnosis],[LDiagnosis],[Symptoms],[DiseaseH], [Physical],[Consultation],[Treatment],[Suggestion],[Therapist],[Status], [ClaimType],[Exclude],[Reimburse],[EDescription],[CCategory],[PNo],[FDate], [FUser],[LDate],[LUser],[HoldF],[Remarks],[Billed],[Verified],[Accepted], [Excess],[PreRegID],[RegID],[ProviderF],[SeqNo],[ExcFDiagnosisF], [ExcSDiagnosisF],[ExcDDiagnosisF],[ExcLDiagnosisF],[PreExistFDiagnosisF], [PreExistExcSDiagnosisF],[PreExistDDiagnosisF],[PreExistLDiagnosisF], [CoAsAmount],[TotalDiscount],[TotalLoading],[Country],[InsuredAmount], [InsuranceAmount],[Deductible],[branch],[IntSeqNo],[OCNO],[CorrespondenceType], [CorrespondenceID],[RefNo],[TerminalID],[Currency],[refPaymentDate], [RefPaymentID],[ManualVerified],[ManualAccepted],[ManualExcess], [refBatchId],[Payto],[ExcessPayor],[ASOF],[AlreadyReimburseASOF],[AccountNo], [BankAccount],[BankName],[ProductType],[EstDiscardDate],[Unpaid],[Discount], [rate],[TreatmentPlace],[ExcessPayorID],[CReason],[Reason],[Administration], [ApprovalDate],[ApprovalID],[PRate],[RDate],[RUser],[Reimbursementpct], [StampDuty],[EDCMerchantID],[EDCTID],[EDCBatchNoIN],[EDCTrcIDIN], [EDCBatchNoOUT],[EDCTrcIDOUT],[EDCApprovalCode],[Vdate],[Vuser],[InvoiceExcessDate], [InvoiceExcessNo],[ReceiptDate],[EDCTrIDIN],[EDCTrIDOUT],[edctidin],[edctidout], [BatchUploadNo],[ACCOUNTNO_EXCESS],[BANKACCOUNT_EXCESS],[BANKNAME_EXCESS], [AdjustF],[ExGratiaF],[Ftime],[LTime],[FollowUpF],[OtherTreatmentPlace], [TreatmentRemarks],[edctidid],[edcidout],[FollowUpDate],[FollowUpUser], [FollowingUpF], [Bank],[BankBranch],[Nationality],[AccountType] FROM dbo.ClaimEditHistory AS CEH WHERE CNO = '"+ (DataCNO[bb])[0] +"'' AND ClaimHistoryID = (SELECT MAX(ClaimHistoryID) FROM ClaimEditHistory WHERE CNO = '"+ (DataCNO[bb])[0] +"'')"
	
	ArrayList ScreenEHist = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryScreenEditHistory)
	
	for (i = 0; i < SourceEHistory.size(); i++){
		
		if (ScreenEHist[i] == SourceEHistory[i]) {
			
			KeywordUtil.markPassed("Value " + ScreenEHist[i] +" from screen same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from screen = " + ScreenEHist[i] + " has different Value from database = " + SourceEHistory[i])
		}
	}
}

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Ok_Notifikasi1'))
}




