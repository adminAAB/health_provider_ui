import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

	WebUI.delay(2)
		
	int JumlahPage
	
	int PageNow
	
	String Angka = WebUI.getText(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Angka_Page'))
	
	ArrayList angka0 = Angka.split(" of ")
	
	JumlahPage = angka0[1].toInteger()
	
	PageNow = angka0[0].toInteger()
	
	println ("JumlahPage : " + JumlahPage)
			
	//String Query = "SELECT Cek, ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) as NoUrut, Nama_Pasien,Tanggal_Admission_From,Tanggal_Admission_To,Treatment_Type,Diagnosa,REPLACE(REPLACE(FORMAT(Total_Tagihan, 'C0','zh-cn'), '¥','Rp '), ',', '.') Total_Tagihan,REPLACE(REPLACE(FORMAT(Tagihan_Dicover, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Tagihan_Dicover,REPLACE(REPLACE(FORMAT(Excess_Tagihan, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Excess_Tagihan FROM (SELECT '' Cek, ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) as NoUrut, GMADK.ID,Nama_Pasien, CONVERT(VARCHAR, Tanggal_Admission_From, 103) Tanggal_Admission_From , CONVERT(VARCHAR, Tanggal_Admission_To, 103) Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId =  '" + GlobalVariable.providerID +"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_From, Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan ) aa"
	
	ArrayList Screen = new ArrayList()
	
	ArrayList SourceDB = new ArrayList()	
		
	if (JumlahPage > 1){
		
		if (cek =="next"){
			
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Next_Page'))
			
			WebUI.delay(3)
			
			Screen = REA.getAllDataTable(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'))
			
			int PageNowFix = ((PageNow + 1) * 10) + 1
			
			GlobalVariable.QueryGetDataUpload = GlobalVariable.QueryGetDataUpload + " WHERE aa.NoUrut > " + PageNow * 10 + " AND aa.NoUrut < " + PageNowFix + ""
					
						
		} else if (cek =="last"){
		
			if (PageNow < JumlahPage || PageNow == JumlahPage){
				
				WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Last_Page'))
				
				WebUI.delay(3)
				
				Screen = REA.getAllDataTable(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'))
				
				int PageNowFix = (JumlahPage * 10) + 1
				
				GlobalVariable.QueryGetDataUpload = GlobalVariable.QueryGetDataUpload + " WHERE aa.NoUrut > " + (JumlahPage - 1) * 10 + " AND aa.NoUrut < "+ PageNowFix +""
				
			}
			
		} else if (cek =="first"){
		
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_First_Page'))
			
			WebUI.delay(3)
			
			Screen = REA.getAllDataTable(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'))
			
			GlobalVariable.QueryGetDataUpload = GlobalVariable.QueryGetDataUpload + " WHERE aa.NoUrut > " + 0 + " AND aa.NoUrut < "+ 11 +""
		
		} else if (cek =="prev"){
		
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Prev_Page'))
			
			WebUI.delay(3)
			
			Screen = REA.getAllDataTable(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'))
			
			int PageNowFix = ((PageNow - 1)  * 10 ) + 1
			
			GlobalVariable.QueryGetDataUpload = GlobalVariable.QueryGetDataUpload + " WHERE aa.NoUrut > " + (PageNow - 2) * 10 + " AND aa.NoUrut < "+ PageNowFix +""
		
		}
		
	}
	
			SourceDB = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload)
			
			println ("Query : " + GlobalVariable.QueryGetDataUpload)
	
			int i
	
			for (i = 0; i < SourceDB.size(); i++){
	
				String TanggalPerawatanFrom = CustomKeywords.'code.GetAllData.convertdateDB'((SourceDB[i])[3].toString())
	
				String TanggalPerawatanTo = CustomKeywords.'code.GetAllData.convertdateDB'((SourceDB[i])[4].toString())
	
				SourceDB[i].set(3, TanggalPerawatanFrom + " - " + TanggalPerawatanTo)
	
				SourceDB[i].remove((SourceDB[i])[4])
			}
	
			println ("SourceDB : " + SourceDB)
			
			println ("Screen : " + Screen)
			
	for (i = 0; i < SourceDB.size(); i++){
		
		for (j = 2; j < SourceDB[0].size(); j++) {
			
			if ((Screen[i])[j] == (SourceDB[i])[j]) {
				
				KeywordUtil.markPassed("Value " + (Screen[i])[j] +" from Grid Table same with Database.")
				
			} else {
			
				KeywordUtil.markFailedAndStop("Value from Grid Table = " + (Screen[i])[j] + " has different Value from database = " + (SourceDB[i])[j])
			
			}
			
		}
	}

