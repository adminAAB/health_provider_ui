import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.waitForElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Simpan'), 120)

WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Simpan'), 1)

WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Cancel'), 1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Simpan'), "value", "Save", 1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Cancel'), "value", "Cancel", 1)

ArrayList ScreenHeaderTable = CustomKeywords.'code.General.GetColumnNameTable'(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/TBL_Claim_Popup'))

ArrayList SourceHeaderTable = ['Nama Peserta', 'Tanggal Perawatan', 'Treatment Type', 'Diagnosa', 'Total Tagihan', 'Tagihan Dicover', 'Excess Tagihan','Upload Status']

ScreenHeaderTable.remove(0)

println ("ScreenHeaderTable: " +ScreenHeaderTable)

println ("SourceHeaderTable: "+ SourceHeaderTable)

WebUI.verifyMatch(ScreenHeaderTable.size().toString(), SourceHeaderTable.size().toString(), false)

int i

for (i = 1; i < SourceHeaderTable.size(); i++){
	
	if (ScreenHeaderTable[i] == SourceHeaderTable[i]) {
		
		KeywordUtil.markPassed("Value " + ScreenHeaderTable[i] +" from Grid Table same with Database.")
		
	} else {
	
		KeywordUtil.markFailedAndStop("Value from Grid Table = " + ScreenHeaderTable[i] + " has different Value from database = " + SourceHeaderTable[i])
	
	}
}


WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/LBL_Header_Popup'), 1)

WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/LBL_Header_Popup'), 'Dokumen yang akan diupload')







