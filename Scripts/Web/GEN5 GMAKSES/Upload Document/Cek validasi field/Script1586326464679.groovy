import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

	WebUI.delay(2)	
	
	if (Flag == "totaltagihan"){
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Clear'))
		
		if (cek == "tipedata") {
			
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), 'abs!@#$%^&*()_+-:=[]\\|]["\',./<;>?')
			
			WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), "value", "0", 1)
			
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), 'abs!@#$%^&*()_+-:=[]\\|]["\',./<;>?')
			
			WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), "value", "0", 1)
		
		} else if (cek == "inputMin"){
		
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), "300")
			
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), "150")
			
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Cari'))
			
			WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Notifikasi-notif',[('notif'):'Total Tagihan tidak sesuai']), 1)
			
			WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_OK_Notifikasi'), 1)
			
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_OK_Notifikasi'))
		
		} 
		
	} else if (Flag == "tglperawatan"){
	
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Clear'))
	
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'))
			
			today = new Date()
			
			String requestDate = today.format('yyyy-MM-dd')
			
			ArrayList today = requestDate.split('-')
			
			today.set(2, today[2].toInteger()-2)
			
			String backdate = today[0] + "-" + today[1] + "-" + today[2]
			
			println(backdate)
			
			CustomKeywords.'code.General.chooseDate'(requestDate)
			
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_To'))
			
			CustomKeywords.'code.General.chooseDate'(backdate)
			
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Cari'))
			
			WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Notifikasi-notif',[('notif'):'Tanggal Perawatan tidak sesuai']), 1)
			
			WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_OK_Notifikasi'), 1)
	
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_OK_Notifikasi'))
			
	} else if (Flag == "formattotaltagihan"){
	
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Clear'))
	
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), '12000000')
			
			WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), "value", "12.000.000", 1)
			
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), '12000000')
			
			WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), "value", "12.000.000", 1)
		
	} else if (Flag == 'maxremarks'){
	
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Grouping/TXT_Remarks'), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fermentum dolor ligula, id consequat lectus rhoncus sagittis. Integer pellentesque, ex non pellentesque blandit, nibh felis porttitor elit, nec malesuada dolor mauris ut odio. Proin at est quam. Donec eleifend elit vulputate, pharetra odio ut, eleifend elit. Nunc a sapien at nibh semper pharetra. Duis aliquet varius mi sed commodo. Aliquam aliquam, sapien nec placerat congue, sem tortor sodales nibh, vel interdum ipsum dui ut metus. Donec tortor orci, auctor et enim eget, consectetur tristique ligula.')
			
			String input = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fermentum dolor ligula, id consequat lectus rhoncus sagittis. Integer pellentesque, ex non pellentesque blandit, nibh felis porttitor elit, nec malesuada dolor mauris ut odio. Proin at est quam. Donec eleifend elit vulputate, pharetra odio ut, eleifend elit. Nunc a sapien at nibh semper pharetra. Duis aliquet varius mi sed commodo. Aliquam aliquam, sapien nec placerat congue, sem tortor sodales nibh, vel interdum ipsum dui ut metus. "
			
			WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Grouping/TXT_Remarks'), input)
	
	}
