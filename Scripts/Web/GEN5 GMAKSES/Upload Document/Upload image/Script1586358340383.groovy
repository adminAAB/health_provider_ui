import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.keyword.REA as REA
import com.keyword.UI as UI
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(2)

//String location = 'C:\\Users\\eum\\Pictures\\Saved Pictures'

GlobalVariable.DataParamL3 = ["a.jpg","b.jpg","c.jpg","d.jpg","e.jpg"]

if (skenario == "upload"){

	for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
	
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Add_Image'))
	
		WebUI.delay(1)

		UI.UploadFile2(GlobalVariable.DataParamL3[i].toString())
		
	}

} else if (skenario == "uploadsubmit"){

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Simpan'))

} else if (skenario == "uploaddelete"){

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Delete_Image-index',[('index'):'0']))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Upload Dokumen/BTN_Simpan'))
	
}




// belum ditambahkan verify status upload