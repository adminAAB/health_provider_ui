import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Nama_Peserta'), "dian")

WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), "100")

WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), "2000")

WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'))

WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Date_Today'))

WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_To'))

WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Date_Today'))

WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Clear'))

WebUI.delay(2)

WebUI.verifyElementAttributeValue(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Nama_Peserta'), "value", "", 1)

WebUI.verifyElementAttributeValue(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), "value", "", 1)

WebUI.verifyElementAttributeValue(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), "value", "", 1)

WebUI.verifyElementAttributeValue(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'), "value", "", 1)
	
WebUI.verifyElementAttributeValue(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_To'), "value", "", 1)

WebUI.verifyElementAttributeValue(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CMB_Jenis_Perawatan'), "value", "", 0)
