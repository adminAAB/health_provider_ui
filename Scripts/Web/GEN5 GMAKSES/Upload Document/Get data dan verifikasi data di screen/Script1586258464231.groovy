import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import code.GetAllData
import internal.GlobalVariable as GlobalVariable

if (Skenario == "Searching"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Clear'))
	
	ArrayList DataClaim = REA.getOneRowDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("SELECT  Cek","SELECT  TOP 1 SUBSTRING(Nama_Pasien, 1, 5) NamaPasien, Cek"))
		
	String replace = ""
	
	if (Param == "nama") {
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Nama_Peserta'), DataClaim[0])
		
		replace = "a.ID = GMADK.ID WHERE Nama_Pasien LIKE '"+ DataClaim[0] +"%' "
		
	} else if (Param == "jp") {
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CMB_Jenis_Perawatan'))
		
		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LST_Jenis_Perawatan-JP',['JP': DataClaim[6]]))
		
		replace = "a.ID = GMADK.ID WHERE Treatment_Type ='"+ DataClaim[6] +"'"
	
	} else if (Param == "tanggalFrom"){
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'))
				
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
		
		replace = "a.ID = GMADK.ID WHERE CAST(Tanggal_Admission_To AS Date) >= '"+ DataClaim[5] +"'"
				
	} else if (Param == "tanggalTo"){
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'))
				
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
		
		replace = "a.ID = GMADK.ID WHERE CAST(Tanggal_Admission_To AS Date) <= '"+ DataClaim[5] +"'"
		
	} else if (Param == "tanggalFromTo"){
	
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'))
		
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_To'))
		
		println ("DataClaim1 : "+ DataClaim[5])
		
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
	
		replace = "a.ID = GMADK.ID WHERE CAST(Tanggal_Admission_To AS Date) >= '"+ DataClaim[5] +"' AND CAST(Tanggal_Admission_To AS Date) <= '"+ DataClaim[5] +"' "
				
	} else if (Param == "tagihanMin") {
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), DataClaim[8])

		replace = "a.ID = GMADK.ID WHERE a.Total_Tagihan > = "+ DataClaim[8] +""
		
	} else if (Param == "tagihanMax") {
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), DataClaim[8])
					
		replace = "a.ID = GMADK.ID WHERE a.Total_Tagihan < = "+ DataClaim[8] +""
				
	} else if (Param == "tagihanMinMax") {
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), DataClaim[8])
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), DataClaim[8])
	
		replace = "a.ID = GMADK.ID WHERE a.Total_Tagihan > = "+ DataClaim[8] +"  AND a.Total_Tagihan <= "+ DataClaim[8] +""
		
	} else if (Param == "allAdaData") {
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Nama_Peserta'), DataClaim[0])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CMB_Jenis_Perawatan'))
		
		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LST_Jenis_Perawatan-JP',['JP': DataClaim[6]]))
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), DataClaim[8])
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), DataClaim[8])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'))
		
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_To'))
		
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
		
		replace = "a.ID = GMADK.ID WHERE Nama_Pasien LIKE '"+ DataClaim[0] +"%'  AND Treatment_Type = '"+ DataClaim[6] +"' AND a.Total_Tagihan > = "+ DataClaim[8] +"  AND a.Total_Tagihan <= "+ DataClaim[8] +" AND CAST(GMADK.Tanggal_Admission_To AS Date) >= '"+ DataClaim[5] +"' AND CAST(GMADK.Tanggal_Admission_To AS Date) <='"+ DataClaim[5] +"' "
		
	}
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Cari'))
	
	WebUI.delay(4)
	
	ArrayList SourceClaim = new ArrayList()
	
	SourceClaim = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("a.ID = GMADK.ID", replace))
	
	for (x = 0; x < SourceClaim.size(); x++){
		
		String TanggalPerawatanFrom = GetAllData.convertdateDB((SourceClaim[x])[3].toString())
		
		String TanggalPerawatanTo = GetAllData.convertdateDB((SourceClaim[x])[4].toString())
		
		SourceClaim[x].set(3, TanggalPerawatanFrom + " - " + TanggalPerawatanTo)
		
		SourceClaim[x].remove((SourceClaim[x])[4])
		
	}
	
	ArrayList ScreenData = REA.getAllDataTableMultiPage(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'), findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Next_Page'))
	
	println ("Count source : " + SourceClaim.size() + " SourceClaim : " + SourceClaim)
	
	println ("Count screen : " + ScreenData.size() + " ScreenData : " + ScreenData)
		
	WebUI.verifyMatch(ScreenData.size().toString(), SourceClaim.size().toString(), false)
	
	for (i = 0; i < SourceClaim.size(); i++){
		
		for (j = 0; j < SourceClaim[0].size(); j++) {
						
			if ((ScreenData[i])[j] == (SourceClaim[i])[j]) {
				
				KeywordUtil.markPassed("Value " + (ScreenData[i])[j] +" from Grid Table same with Database.")
				
			} else {
			
				KeywordUtil.markFailedAndStop("Value from Grid Table = " + (ScreenData[i])[j] + " has different Value from database = " + (SourceClaim[i])[j])
			
			}
			
		}
	}
	
	if (Param == "allNoData") {

		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Nama_Peserta'), DataClaim[0])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CMB_Jenis_Perawatan'))
		
		ArrayList JP = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryTreatmentType.replace("TreatmentType'","TreatmentType' AND Description <> '"+ DataClaim[6] +"'").replace("SELECT ","SELECT TOP 1"))
		
		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LST_Jenis_Perawatan-JP',['JP': JP[1]]))
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), DataClaim[8])
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Max'), DataClaim[8])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'))
		
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_To'))
		
		CustomKeywords.'code.General.chooseDate'(DataClaim[5])
		
		//replace = "a.ID = GMADK.ID WHERE Nama_Pasien LIKE '"+ DataClaim[0] +"%'  AND Treatment_Type = '"+ JP[1] +"' AND a.Total_Tagihan > = "+ DataClaim[8] +"  AND a.Total_Tagihan <= "+ DataClaim[8] +" AND CAST(GMADK.Tanggal_Admission_To AS Date) >= '"+ DataClaim[5] +"' AND CAST(GMADK.Tanggal_Admission_To AS Date) <='"+ DataClaim[5] +"' "
	
		WebUI.delay(2)
		
		//ArrayList SourceClaim = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("a.ID = GMADK.ID", replace))
		
		WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_NotFound'), "Data tidak ditemukan")
		
	}
	
} else if (Skenario == "Grouping"){

	if (param == "1data"){
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("Cek , NoUrut","Cek , NoUrut, ID"))
		
		GlobalVariable.DataParamL2 = GlobalVariable.DataParamL1[2]
		
		println (GlobalVariable.DataParamL1)
		
		if (GlobalVariable.DataParamL1.size() > 0){
				
			REA.ClickExpectedRow(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'), "No", GlobalVariable.DataParamL1[1].toString())
	
		}

	} else if (param == "1page"){

		GlobalVariable.DataParamL1 = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("Cek , NoUrut","TOP 10 Cek , NoUrut, ID"))
		
		for (i = 0; i < GlobalVariable.DataParamL1.size(); i++){

			REA.ClickExpectedRow(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'), "No", (GlobalVariable.DataParamL1[i])[1].toString())
	
		}
		
		for (j = 0 ; j < GlobalVariable.DataParamL1.size() ; j++) {
			
			GlobalVariable.DataParamL2.add(GlobalVariable.DataParamL1[2])
			
		}
		
	} else if (param == "jmlhpagecustom" && jumlahpage > 0){
	
		GlobalVariable.dataParam2 = jumlahpage*10
		
		GlobalVariable.DataParamL1 = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("Cek , NoUrut","TOP "+ GlobalVariable.dataParam2 +" Cek , NoUrut, ID"))
		
		int data = GlobalVariable.DataParamL1.size()/10
		
		if (data >= jumlahpage){
			
			for (i = 0; i < jumlahpage; i++){
			
				WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CHK_All1page'))
			
				WebUI.delay(2)
			
				if (i < jumlahpage){
					
					println ("i : " + i + " jumlahpage : "+ jumlahpage)
				
					WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Next_Page'))
				}
							
			}
		}
		
		for (j = 0 ; j < GlobalVariable.DataParamL1.size() ; j++) {
			
			GlobalVariable.DataParamL2.add(GlobalVariable.DataParamL1[2])
			
		}
		
	} else if (param == "jmlhdatacustom" && jumlahdata > 0){
					
		GlobalVariable.dataParam2 = jumlahdata
		
		GlobalVariable.DataParamL1 = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("Cek , NoUrut","TOP "+ GlobalVariable.dataParam2 +" Cek , NoUrut, ID"))
		
		println (GlobalVariable.DataParamL1)
		
		if (GlobalVariable.DataParamL1.size().toInteger() >= jumlahdata){
			
			for (i = 0; i < GlobalVariable.DataParamL1.size(); i++){
			
				REA.ClickExpectedRowWithNext(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'), "No", GlobalVariable.DataParamL1[i][1].toString(), findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Next_Page'))
			
			}
		}
		
		for (j = 0 ; j < GlobalVariable.DataParamL1.size() ; j++) {
			
			GlobalVariable.DataParamL2.add(GlobalVariable.DataParamL1[2])
			
		}
	
	}
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Gabungkan'))

} else if (Skenario == "VerifikasiTreatmentType"){

	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CMB_Jenis_Perawatan'))
	
	ArrayList sourceJP = REA.getOneColumnDatabase("172.16.94.70", "GMA", GlobalVariable.QueryTreatmentType, "Description")
	
	ArrayList screenJP = REA.getArrayButton(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CMB_Jenis_Perawatan'), "tagname", "option")
	
	screenJP.remove(screenJP[0])
	
	println ("screenJP: "+ screenJP)
	
	println ("sourceJP: "+ sourceJP)
	
	WebUI.verifyMatch(screenJP.size().toString(), sourceJP.size().toString(), false)
	
	for (i = 0; i < sourceJP.size; i++){
	
		WebUI.verifyMatch(screenJP[i], sourceJP[i], false)
	}	

} else if (Skenario == "Upload"){

	if (param == "pilihdata"){
		
		GlobalVariable.DataParamL1 = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("Cek , NoUrut","Cek , NoUrut, ID"))
		
		String cek
		
		println(GlobalVariable.DataParamL1)
		
		for (y = 0; y < GlobalVariable.DataParamL1.size(); y++){
			
			if ((GlobalVariable.DataParamL1[y])[10] == 'Not Uploaded') {
				
				println("(GlobalVariable.DataParamL1[y])[1]: "+(GlobalVariable.DataParamL1[y])[1])
				
				println (GlobalVariable.DataParamL1[y])
				
				cek = (GlobalVariable.DataParamL1[y])[1].toInteger() -1
				
				GlobalVariable.DataParamS1 = (GlobalVariable.DataParamL1[y])[2]
				
				GlobalVariable.DataParamL2 = GlobalVariable.DataParamL1[y]
				
				GlobalVariable.dataid = cek
				
				println ("cek : "+cek + "GlobalVariable.dataid : "+ GlobalVariable.dataid)
				
				break
	
			}
			
		}
		println (cek)
		
		if (GlobalVariable.DataParamL1.size() > 0 && cek.toInteger() < 10){
						
			WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Upload-index',[('index'): cek]))
			
		} else if (GlobalVariable.DataParamL1.size() > 0 && cek.toInteger() >= 10){
		
			cek = cek.toInteger() - 10
			
			println (cek)
		
			WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Next_Page'))
			
			WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Upload-index',[('index'): cek]))
			
		}
		
	} else if (param == "CekDataScreenUpload"){
	
		ArrayList SourceClaim = new ArrayList()
		
		SourceClaim = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace(") aa",") aa WHERE  NoUrut = '"+ GlobalVariable.DataParamS1 +"'"))
		
		println (SourceClaim)
		
		for (x = 0; x < SourceClaim.size(); x++){
			
			String TanggalPerawatanFrom = GetAllData.convertdateDB((SourceClaim[x])[3].toString())
			
			String TanggalPerawatanTo = GetAllData.convertdateDB((SourceClaim[x])[4].toString())
			
			SourceClaim[x].set(3, TanggalPerawatanFrom + " - " + TanggalPerawatanTo)
			
			SourceClaim[x].remove((SourceClaim[x])[4])
			
		}
		
		println (SourceClaim)
		
		ArrayList ScreenData = REA.getAllDataTable(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'))
	
		println ("Count source : " + SourceClaim.size() + " SourceClaim : " + SourceClaim)
		
		println ("Count screen : " + ScreenData.size() + " ScreenData : " + ScreenData)
			
		WebUI.verifyMatch(ScreenData.size().toString(), SourceClaim.size().toString(), false)
		
		for (i = 0; i < SourceClaim.size(); i++){
			
			for (j = 0; j < SourceClaim[0].size(); j++) {
							
				if ((ScreenData[i])[j] == (SourceClaim[i])[j]) {
					
					KeywordUtil.markPassed("Value " + (ScreenData[i])[j] +" from Grid Table same with Database.")
					
				} else {
				
					KeywordUtil.markFailedAndStop("Value from Grid Table = " + (ScreenData[i])[j] + " has different Value from database = " + (SourceClaim[i])[j])
				
				}
				
			}
		}
		
	} else if (param == "deleteupload"){
		
		println (GlobalVariable.dataid)
	
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Tutup_Notifikasi'))
		
		if (GlobalVariable.dataid.toInteger() < 10){
		
			WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Upload-index',[('index'): GlobalVariable.dataid]))
			
		} else if (GlobalVariable.dataid.toInteger() >= 10){
		
			GlobalVariable.dataid = GlobalVariable.dataid.toInteger() - 10
			
			println (GlobalVariable.dataid)
			
			WebUI.click(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Upload-index',[('index'): GlobalVariable.dataid]))
		}
		
	}

} else if (Skenario == "LoadScreen"){

	ArrayList SourceClaim = new ArrayList()
	
	SourceClaim = REA.getAllDataDatabase("172.16.94.48", "LiTT", GlobalVariable.QueryGetDataUpload.replace("GVProviderId",GlobalVariable.ProviderID).replace("SELECT  Cek","SELECT TOP 10 Cek"))
	
	println (SourceClaim)
	
	for (x = 0; x < SourceClaim.size(); x++){
		
		String TanggalPerawatanFrom = GetAllData.convertdateDB((SourceClaim[x])[3].toString())
		
		String TanggalPerawatanTo = GetAllData.convertdateDB((SourceClaim[x])[4].toString())
		
		SourceClaim[x].set(3, TanggalPerawatanFrom + " - " + TanggalPerawatanTo)
		
		SourceClaim[x].remove((SourceClaim[x])[4])
		
	}
	
	println (SourceClaim)
	
	ArrayList ScreenData = REA.getAllDataTable(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'))

	println ("Count source : " + SourceClaim.size() + " SourceClaim : " + SourceClaim)
	
	println ("Count screen : " + ScreenData.size() + " ScreenData : " + ScreenData)
		
	WebUI.verifyMatch(ScreenData.size().toString(), SourceClaim.size().toString(), false)
	
	for (i = 0; i < SourceClaim.size(); i++){
		
		for (j = 0; j < SourceClaim[0].size(); j++) {
						
			if ((ScreenData[i])[j] == (SourceClaim[i])[j]) {
				
				KeywordUtil.markPassed("Value " + (ScreenData[i])[j] +" from Grid Table same with Database.")
				
			} else {
			
				KeywordUtil.markFailedAndStop("Value from Grid Table = " + (ScreenData[i])[j] + " has different Value from database = " + (SourceClaim[i])[j])
			
			}
			
		}
	}
}