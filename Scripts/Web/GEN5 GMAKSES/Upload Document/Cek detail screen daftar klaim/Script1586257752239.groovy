import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


ArrayList ScreenHeaderTable = CustomKeywords.'code.General.GetColumnNameTable'(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TBL_Claim'))

ArrayList SourceHeaderTable = ['', 'No', 'Nama Peserta', 'Tanggal Perawatan', 'Treatment Type', 'Diagnosa', 'Total Tagihan', 'Tagihan Dicover', 'Excess Tagihan', 'Upload Status','']

println ("ScreenHeaderTable "+ScreenHeaderTable)

WebUI.verifyMatch(ScreenHeaderTable.size().toString(), SourceHeaderTable.size().toString(), false)

int i

for (i = 0; i < SourceHeaderTable.size(); i++){
	
	WebUI.verifyMatch(ScreenHeaderTable[i], SourceHeaderTable[i], false, FailureHandling.STOP_ON_FAILURE)
	
}

WebUI.verifyElementText(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Jenis_Perawatan'), "Jenis Perawatan")

WebUI.verifyElementText(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Nama_Peserta'), "Nama Peserta")

WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Tanggal_Perawatan'), 1)

WebUI.verifyElementText(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/LBL_Total_Tagihan'), "Total Tagihan")

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Nama_Peserta'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Tgl_Admission_From'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/TXT_Total_Tagihan_Min'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Cari'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Clear'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Gabungkan'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/BTN_Kembali'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Upload Document/Daftar Klaim/CMB_Jenis_Perawatan'), 1)






