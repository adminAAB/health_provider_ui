import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (skenario == "upload"){ 

	ArrayList dataimage = ["-1", GlobalVariable.DataParamS1, "0", "Sys"]

	ArrayList sourceGCI = new ArrayList()

	for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
	
		sourceGCI.add(dataimage)
	
	}

	println (sourceGCI)

	//String QueryClaimImage = "SELECT BatchID, ID, ImageID,ImageExt,PathFile,ImageData,ThumbnailData,Status,EntryUsr FROM [GardaMedikaAkses].[Grouping_Claim_Image] WHERE EntryDt = (SELECT MAX(EntryDt) FROM GardaMedikaAkses.Grouping_Claim_Image)"

	String QueryClaimImage = "SELECT BatchID, ID, Status,EntryUsr FROM [GardaMedikaAkses].[Grouping_Claim_Image] WHERE ID = '"+ GlobalVariable.DataParamS1 +"'"
	
	println (QueryClaimImage)
	
	REA.compareAllDatabasetoArray("172.16.94.70", "GMA", QueryClaimImage, sourceGCI)
	
} else if (skenario == 'uploadsubmit'){

	ArrayList dataimage = ["-1", GlobalVariable.DataParamS1, "1", "Sys"]

	ArrayList sourceGCI = new ArrayList()

	for (i = 0; i < GlobalVariable.DataParamS1.size(); i++){
	
		sourceGCI.add(dataimage)
	
	}

	println (sourceGCI)

	//String QueryClaimImage = "SELECT BatchID, ID, ImageID,ImageExt,PathFile,ImageData,ThumbnailData,Status,EntryUsr FROM [GardaMedikaAkses].[Grouping_Claim_Image] WHERE EntryDt = (SELECT MAX(EntryDt) FROM GardaMedikaAkses.Grouping_Claim_Image)"

	String QueryClaimImage = "SELECT BatchID, ID, Status,EntryUsr FROM [GardaMedikaAkses].[Grouping_Claim_Image] WHERE ID = '"+ GlobalVariable.DataParamS1 +"'"

	REA.compareAllDatabasetoArray("172.16.94.70", "GMA", QueryClaimImage, sourceGCI)
	
} else if (skenario == "delete"){

	ArrayList sourceGCI = ["-1", GlobalVariable.DataParamS1, "2", "Sys"]
	
//	ArrayList sourceGCI = new ArrayList()
//
//	for (i = 0; i < 1; i++){
//	
//		sourceGCI.add(dataimage1)
//	
//	}

	println (sourceGCI)

	//String QueryClaimImage = "SELECT BatchID, ID, ImageID,ImageExt,PathFile,ImageData,ThumbnailData,Status,EntryUsr FROM [GardaMedikaAkses].[Grouping_Claim_Image] WHERE EntryDt = (SELECT MAX(EntryDt) FROM GardaMedikaAkses.Grouping_Claim_Image)"

	String QueryClaimImage = "SELECT BatchID, ID, Status,EntryUsr FROM [GardaMedikaAkses].[Grouping_Claim_Image] WHERE ID = '"+ GlobalVariable.DataParamS1 +"' and Status = '2'"

	ArrayList screenGCI = REA.getOneRowDatabase("172.16.94.70", "GMA", QueryClaimImage)
	
	for (i = 0; i < sourceGCI.size(); i++){
		
		if (screenGCI[i] == sourceGCI[i]) {
			
			KeywordUtil.markPassed("Value " + screenGCI[i] +" from screen same with Database.")
			
		} else {
			
			KeywordUtil.markFailedAndStop("Value from screen = " + screenGCI[i] + " has different Value from database = " + sourceGCI[i])
		}
		
	}	

}