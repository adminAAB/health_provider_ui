import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


if (skenario == "NomorPesertaInvalid1"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("and GMAE.TreatmentType in ('GVTreatmentType')", ""))
	
	println (GlobalVariable.DataParamL1)
	
	nomorMember = GlobalVariable.DataParamL1[1] + "XXX"
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		nomorMember)
	
	WebUI.delay(1)
	
	WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Notif_Not_Matches'), "Nama / Nomor Peserta tidak ditemukan")

} else if (skenario == "NomorPesertaInvalid2"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'))
		
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
		
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
			
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
			'0aaxxc')
		
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))
		
	WebUI.delay(1)
		
	WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Validasi_Mandatory-notifmandatory',
		[('notifmandatory') : 'Mohon memilih salah satu Peserta']), 1)

	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'), 1)
		
} else if (skenario == "allfieldkosong"){
		
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'))
			
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
			
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))

	WebUI.delay(1)
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("and GMAE.TreatmentType in ('GVTreatmentType')", ""))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		GlobalVariable.DataParamL1[1])

	String inputan = GlobalVariable.DataParamL1[1] + " - " + GlobalVariable.DataParamL1[6]
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): inputan]))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Discharge'))
			
	WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Validasi_Mandatory-notifmandatory',
			[('notifmandatory') : 'Mohon mengisi Diagnosa dan Biaya atas transaksi ini']), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'), 1)
			
} else if (skenario == "ProviderInvalid"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	println (GlobalVariable.QuerySearchNomorPesertaD.replace("='GVProviderId'", "<> '" + GlobalVariable.ProviderID + "'").replace("AND (MemberNo LIKE '%GVMember%' OR MemberName LIKE '%GVMember%')", "").replace("CONCAT(MemberNo,' - ', MemberName) peserta","TOP 1 MemberNo, CONCAT(MemberNo,' - ', MemberName) peserta"))
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySearchNomorPesertaD.replace("='GVProviderId'", "<> '" + GlobalVariable.ProviderID + "'").replace("AND (MemberNo LIKE '%GVMember%' OR MemberName LIKE '%GVMember%')", "").replace("CONCAT(MemberNo,' - ', MemberName) peserta","TOP 1 MemberNo, CONCAT(MemberNo,' - ', MemberName) peserta"))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		GlobalVariable.DataParamL1[0])
	
	WebUI.delay(3)
	
	//WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): GlobalVariable.DataParamL1[0]]))
	
	WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Notif_Not_Matches'), "Nama / Nomor Peserta tidak ditemukan")
	
} else if (skenario == "NomorPesertaValid"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("and GMAE.TreatmentType in ('GVTreatmentType')", ""))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		GlobalVariable.DataParamL1[0])
	
	ArrayList SourceNomorPeserta = REA.getOneColumnDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySearchNomorPesertaD.replace("GVProviderId",GlobalVariable.ProviderID).replace("GVMember",GlobalVariable.DataParamL1[0]), "peserta")
	
	WebUI.delay(5)
	
	def xpath = findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TBL_Nomor_Peserta')
	
	ArrayList screenNoPeserta = REA.getArrayButton(xpath, "tagname", "li")
	
	println (screenNoPeserta)
	
	println (SourceNomorPeserta)
	
	WebUI.verifyMatch(screenNoPeserta.size().toString(), SourceNomorPeserta.size().toString(), false)
	
	for (j = 0 ; j < SourceNomorPeserta.size() ; j++) {
	
		WebUI.verifyMatch(screenNoPeserta[j], SourceNomorPeserta[j], false)

	}
	
	
} else if (skenario == "NamaPesertaValid"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("CE.MemberNo, 2, 3", "CE.MemberName, 2, 4").replace("and GMAE.TreatmentType in ('GVTreatmentType')", ""))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		GlobalVariable.DataParamL1[0])
	
	ArrayList SourceNomorPeserta = REA.getOneColumnDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySearchNomorPesertaD.replace("GVProviderId",GlobalVariable.ProviderID).replace("GVMember",GlobalVariable.DataParamL1[0]), "peserta")
	
	WebUI.delay(3)
	
	def xpath = findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TBL_Nomor_Peserta')
	
	ArrayList screenNoPeserta = REA.getArrayButton(xpath, "tagname", "li")
	
	println (screenNoPeserta)
	
	println (SourceNomorPeserta)
	
	WebUI.verifyMatch(screenNoPeserta.size().toString(), SourceNomorPeserta.size().toString(), false)
	
	for (j = 0 ; j < SourceNomorPeserta.size() ; j++) {
	
		WebUI.verifyMatch(screenNoPeserta[j], SourceNomorPeserta[j], false)

	}
	
} else if (skenario == "nomorpesertakosong"){

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))

	WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Validasi_Mandatory-notifmandatory',
			[('notifmandatory') : 'Mohon isi Nomor Peserta']), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'), 1)

} else if (skenario == "diagnosakosong"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("and GMAE.TreatmentType in ('GVTreatmentType')", ""))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		GlobalVariable.DataParamL1[1])

	String inputan = GlobalVariable.DataParamL1[1] + " - " + GlobalVariable.DataParamL1[6]
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): inputan]))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))
	
	println (GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType", GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]))
	
	ArrayList ChargeFieldName = REA.getOneColumnDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType", GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]), 'ChargeFieldName')
	
	println (ChargeFieldName)
	
	WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Biaya-NamaField', [('NamaField') : ChargeFieldName[0]]),
		'1000000')
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Discharge'))

	WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Validasi_Mandatory-notifmandatory',
			[('notifmandatory') : 'Mohon mengisi Diagnosa atas transaksi ini']), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'), 1)

} else if (skenario == "biayakosong"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("and GMAE.TreatmentType in ('GVTreatmentType')", ""))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'), GlobalVariable.DataParamL1[1])

	String inputan = GlobalVariable.DataParamL1[1] + " - " + GlobalVariable.DataParamL1[6]
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): inputan]))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))
	
	println (GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType", GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa'), "aci")

    ArrayList screenDiagnosa = REA.getArrayButton(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Diagnosa'), 
        'tagname', 'li')
	
	println (screenDiagnosa)

	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/DTA_Diagnosa-diagnosa',[('diagnosa') : screenDiagnosa[1]]))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Discharge'))

	WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Validasi_Mandatory-notifmandatory',
			[('notifmandatory') : 'Mohon mengisi Biaya atas transaksi ini']), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'), 1)

} else if (skenario == "diagnosainvalid"){
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_OK'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("and GMAE.TreatmentType in ('GVTreatmentType')", ""))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		GlobalVariable.DataParamL1[1])

	String inputan = GlobalVariable.DataParamL1[1] + " - " + GlobalVariable.DataParamL1[6]
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): inputan]))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa'), "abcxx")

	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Notif_Diagnosa_Invalid'), 2)
		
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Diagnosa_Tambahan'))
		
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa_Tambahan'))
		
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa_Tambahan'), 'abcxx')
			
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Notif_Diagnosa_Invalid'), 2)
	
}

