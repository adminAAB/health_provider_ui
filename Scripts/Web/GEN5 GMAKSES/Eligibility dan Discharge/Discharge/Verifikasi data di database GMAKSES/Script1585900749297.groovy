import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


		String diagnosaTambahan = ''

		for (xx = 1; xx < GlobalVariable.DataParamL2.size(); xx ++){

			if (xx == GlobalVariable.DataParamL2.size()-1) {

				diagnosaTambahan += GlobalVariable.DataParamL2[xx]
			} else {

				diagnosaTambahan += GlobalVariable.DataParamL2[xx] + ","
			}
		}

		ArrayList SourceCE = [GlobalVariable.DataParamS1.toString(), GlobalVariable.DataParamL1[1].toString(), GlobalVariable.DataParamL1[2].toString(), GlobalVariable.DataParamL1[6].toString(), GlobalVariable.ProviderID, GlobalVariable.DataParamL1[11].toString(), GlobalVariable.StatusEligible[0], GlobalVariable.DataParamL2[0], diagnosaTambahan, '1','SYS']
		
		println (SourceCE)
			
		ArrayList SourceHCE = [[GlobalVariable.DataParamL1[1], GlobalVariable.StatusEligible[2], '1','SYS'],[GlobalVariable.DataParamL1[1], GlobalVariable.StatusEligible[0], '1','SYS']]
		
		WebUI.waitForElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Header_Discharge'), GlobalVariable.maxDelay)
		
		println (REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[1])))
		
		ArrayList sourceDCT = GlobalVariable.DataParamL3
		
		println (GlobalVariable.DataParamL3)
		
		println (GlobalVariable.QueryDataInsertDCT.replace("GVMemberNo",GlobalVariable.DataParamL1[1]))
		
		println ("sourceDCT: "+sourceDCT)
		
		WebUI.delay(5)
		
		println (REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertDCT.replace("GVMemberNo",GlobalVariable.DataParamL1[1])))
				
		println (GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[1]))
		
		println (REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[1])))
		
		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[1]), SourceCE)
		
		REA.compareAllDatabasetoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[1]), SourceHCE)
		
		REA.compareAllDatabasetoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertDCT.replace("GVMemberNo",GlobalVariable.DataParamL1[1]), sourceDCT)

		REA.updateValueDatabase("172.16.94.70", "GMA", GlobalVariable.SPInsertDaftarKlaimLiTT)

	
		
		