import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

	
	WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'), 120)
	
	WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'), 120)

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'))
	
	if (skenario == "inputdata"){
		
		println ("GlobalVariable.QueryMemberValidStatusE : " +GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("GVTreatmentType", TreatmentType))
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("GVTreatmentType", TreatmentType))
		
		println ("GlobalVariable.DataParamL1 : " +GlobalVariable.DataParamL1)
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
			GlobalVariable.DataParamL1[0])
		
		String inputan = GlobalVariable.DataParamL1[1] + " - " + GlobalVariable.DataParamL1[6]
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): inputan]))
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))
		
	} else if (skenario == "pencarian"){
	
		if (input == "nomor"){
		
			GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("GVTreatmentType", TreatmentType))
			
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
				GlobalVariable.DataParamL1[0])

		} else {

			GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID).replace("(CE.MemberNo, 2, 3) AS MemberNo","(CE.MemberName, 2, 3) AS MemberName").replace("GVTreatmentType", TreatmentType)

			GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE)
			
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
				GlobalVariable.DataParamL1[0])
	
		}
	
	}
	
	
	
	
	