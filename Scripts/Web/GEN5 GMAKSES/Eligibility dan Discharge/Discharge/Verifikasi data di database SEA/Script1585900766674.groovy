import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


String CNO = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetCNO, "SeqNo")

String ClaimNo = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetClaimNo,"ClaimNo")

ArrayList SourceNominalDataClaimH = REA.getOneRowDatabase("172.16.94.48", "LiTT", GlobalVariable.QuerySourceDataInsertCH.replace("GVCNO",CNO))

println (SourceNominalDataClaimH)

ArrayList PolicyMemberInfo = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetPolicyMemberInfo.replace("GVProdType",GlobalVariable.DataParamL1[13]).replace("GVMemberNo", GlobalVariable.DataParamL1[1]))

println (PolicyMemberInfo)

ArrayList ProviderInfo = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetProviderInfo.replace("GVProviderID",GlobalVariable.ProviderID))

println (ProviderInfo)

ArrayList SourceCH = ['','','','','','','','','','','','','','','H','I','0','0','','ONLINE','GMAKSES','GMAKSES','1','',SourceNominalDataClaimH[2].toFloat().toString(),SourceNominalDataClaimH[3].toFloat().toString(),SourceNominalDataClaimH[3].toFloat().toString(),'0.0','0','1','-1','0','0','0','0','0','0','0','0','0','0.0','0.0','0.0','INA','0.0','0.0','0.0','0','P','','','','IDR','','0.0','0.0','0.0','','P','C','0','0','null','0.0','0.0','1.0','','','0.0','null','','1.0','null','','0.0','0.0','','','','','','','','null','','null','','','','','','0','','','','0','0','0','','','','','','0',ProviderInfo[6],ProviderInfo[7],CNO,ClaimNo,GlobalVariable.ProviderID,GlobalVariable.ProviderID,PolicyMemberInfo[2],GlobalVariable.DataParamL1[2],GlobalVariable.DataParamL1[18],PolicyMemberInfo[1],'0',PolicyMemberInfo[3],CNO,ProviderInfo[1],ProviderInfo[5],ProviderInfo[2],ProviderInfo[3],ProviderInfo[4],GlobalVariable.DataParamL1[13],ProviderInfo[0],PolicyMemberInfo[2],SourceNominalDataClaimH[3].toFloat().toString(),'0.0', SourceNominalDataClaimH[2].toFloat().toString(), SourceNominalDataClaimH[3].toFloat().toString(), SourceNominalDataClaimH[4].toFloat().toString()]

ArrayList DetAmountBenefit = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySourceDataInsertCDE.replace("GVCNO",CNO))

println (DetAmountBenefit)

ArrayList SourceCDE = new ArrayList() //claimdetail

for (a = 0; a < DetAmountBenefit.size(); a++){
	
	ArrayList CDE = ['','1.0' ,'0','0.0','0.0','','','V','0.0','1900-01-01 00:00:00.0','0','0.0','0.0','0.0','0','','0.0','0.0','0.0','0.0','0.0',CNO, (a+1).toString() , (DetAmountBenefit[a])[2], (DetAmountBenefit[a])[3].toFloat().toString(), (DetAmountBenefit[a])[3].toFloat().toString(), (DetAmountBenefit[a])[3].toFloat().toString(), (DetAmountBenefit[a])[5].toFloat().toString()] 
	
	//ArrayList CDE = ['','1.0' ,'0','0.0','0.0','','','V','0.0','1900-01-01 00:00:00.0','0','0.0','0.0','0.0','0','','0.0','0.0','0.0','0.0','0.0',CNO, (a+1).toString() , (DetAmountBenefit[a])[2], SourceNominalDataClaimH[5], SourceNominalDataClaimH[6], DetAmountBenefit[3] , DetAmountBenefit[4], DetAmountBenefit[4], DetAmountBenefit[5]]
	
	SourceCDE.add(CDE)
	
}

println ("SourceCDE: "+SourceCDE)

println (SourceCH)

ArrayList SourceCDI = new ArrayList() //claim_diagnosis

for (i = 0; i < GlobalVariable.DataParamL2.size(); i++){
	
	ArrayList CDI = [CNO, GlobalVariable.DataParamL2[i], '', '0']
	
	SourceCDI.add(CDI)	
}

println ("SourceCDI: "+SourceCDI)

println (REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertCDI.replace("GVCNO",CNO)) + " - Query - " + GlobalVariable.QueryDataInsertCDI.replace("GVCNO",CNO))

ArrayList SourceCEH = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QuerySourceDataInsertCEH.replace("GVCNO", CNO))

//REA.compareRowDBtoArray("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertCH.replace("GVCNO",CNO), SourceCH)
//
//REA.compareAllDatabasetoArray("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertCDE.replace("GVCNO",CNO), SourceCDE)
//
//REA.compareAllDatabasetoArray("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertCDI.replace("GVCNO",CNO), SourceCDI)

println (SourceCEH)

println ("Query : "+ GlobalVariable.QueryDataInsertCEH.replace("GVCNO",CNO) + " - " + REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertCEH.replace("GVCNO",CNO)))

REA.compareRowDBtoArray("172.16.94.70", "SEA", GlobalVariable.QueryDataInsertCEH.replace("GVCNO",CNO), SourceCEH)


