import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.keyword.REA as REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.apache.commons.lang.RandomStringUtils


String[] diagnosa = ['A06.0', 'A17.8+', 'A17.0', 'C14.0', 'C16.4', 'C69.0', 'C31.0'] //[ 'A06.0', 'A17.0','C14.0','C16.4','C69.0','C31.0']]

int random = new Random().nextInt(diagnosa.length)

if (skenario == "diagnosa"){

	GlobalVariable.DataParamL2.add(diagnosa[random].toString())

	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa'), GlobalVariable.DataParamL2[0])

	ArrayList screenDiagnosa = REA.getArrayButton(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Diagnosa'),
			'tagname', 'li')

	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/DTA_Diagnosa-diagnosa',
			[('diagnosa') : screenDiagnosa[0]]))

	int JumlahDiagnosa = 4

	for (i = 0; i < JumlahDiagnosa; i++){

		GlobalVariable.DataParamL2.add(diagnosa[i])

		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa_Tambahan'), GlobalVariable.DataParamL2[i+1])

		ArrayList screenDiagnosaTambahan = REA.getArrayButton(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Diagnosa_Tambahan'),
				'tagname', 'li')

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/DTA_Diagnosa-diagnosa',
				[('diagnosa') : screenDiagnosaTambahan[0]]))

	}

	println (GlobalVariable.DataParamL2)

	println (GlobalVariable.DataParamL1)

	println (GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType",GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]))

	ArrayList benefitID = REA.getOneColumnDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType",GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]), 'BenefitID')

	ArrayList ChargeFieldName = REA.getOneColumnDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType",GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]), 'ChargeFieldName')

	ArrayList NamaFieldBiaya = ChargeFieldName

	ArrayList ListBenefitID = benefitID

	for (cf = 0; cf < benefitID.size(); cf++){

		if (ChargeFieldName[cf] == ChargeFieldName[cf+1]){

			ListBenefitID[cf] = ListBenefitID[cf] + "_" + ListBenefitID[cf+1]

			ListBenefitID.remove(ListBenefitID[cf+1])

			NamaFieldBiaya.remove(NamaFieldBiaya[cf+1])

		}

	}

	println (NamaFieldBiaya)

	println(benefitID)

	GlobalVariable.DataParamL3 = REA.getAllDataDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType",GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]))

	String totalInput = ''

	if (cek == 'allbenefit') {

		ArrayList asalNama = new ArrayList()

		for (a = 0; a < NamaFieldBiaya.size(); a++) {

			ArrayList sourceBenefit = new ArrayList()

			int inputan = Math.floor(Math.random()*9999)

			WebUI.setText(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Biaya-NamaField', [('NamaField') : ChargeFieldName[
					a]]), inputan.toString())

			sourceBenefit.add(NamaFieldBiaya[a])

			sourceBenefit.add(benefitID[a])

			sourceBenefit.add(inputan.toString())

			asalNama.add(sourceBenefit)

		}

		int count = asalNama.size()

		for (b = 0; b < count; b++){

			ArrayList dataAddSplit = new ArrayList()

			if ((asalNama[b])[1].contains("_")){

				ArrayList split = (asalNama[b])[1].split("_")

				dataAddSplit = asalNama[b]

				dataAddSplit.set(1, split[1])

				asalNama.add(b+1, dataAddSplit)

				asalNama[b].set(1, split[0].toString())

			}

			count = asalNama.size()

		}

		GlobalVariable.DataParamS1 = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetCNO, "SeqNo")

		GlobalVariable.DataParamS1 = GlobalVariable.DataParamS1.toInteger() + 1

		int countDataSource = GlobalVariable.DataParamL3.size()

		for (h = 0 ; h < countDataSource ; h++ ) {

			GlobalVariable.DataParamL3[h].remove((GlobalVariable.DataParamL3[h])[2])

			GlobalVariable.DataParamL3[h].remove((GlobalVariable.DataParamL3[h])[2])

			if (GlobalVariable.DataParamL3[h].contains((asalNama[h])[0])) {

				GlobalVariable.DataParamL3[h].add((asalNama[h])[2])

			}

		}

		int totBiaya = 0

		for (c = 0; c < countDataSource; c++) {

			totBiaya += GlobalVariable.DataParamL3[c][2].toInteger()

		}

		println (GlobalVariable.DataParamL3)

		String GetLimit = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetLimit.replace("GVMNO",GlobalVariable.DataParamL1[2]).replace("GVProductType",GlobalVariable.DataParamL1[13]), "Limit")

		println (GetLimit)

		for (d = 0; d < countDataSource; d++) {

			if (GetLimit.replace('.00','').toInteger() == -1){

				GlobalVariable.DataParamL3[d].add(totBiaya.toString())

				GlobalVariable.DataParamL3[d].add("0")

				GlobalVariable.DataParamL3[d].add("Sys")

			} else if (GetLimit.replace('.00','').toInteger() > 1){

				if (totBiaya > GetLimit.replace('.00','').toInteger()){

					GlobalVariable.DataParamL3[d].add(GetLimit.replace('.00',''))
					
					int cover = totBiaya - GetLimit.replace('.00','').toInteger()

					GlobalVariable.DataParamL3[d].add(cover.toString())

					GlobalVariable.DataParamL3[d].add("Sys")

				} else {

					GlobalVariable.DataParamL3[d].add(totBiaya.toString())

					GlobalVariable.DataParamL3[d].add("0")

					GlobalVariable.DataParamL3[d].add("Sys")

				}

			}

		}

		println (GlobalVariable.DataParamL3)

		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Discharge'))

	}

} else if (skenario == "alldiagnosa"){

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa'))

	ArrayList screenDiagnosa = REA.getArrayButton(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Diagnosa'),'tagname', 'li')

	WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/DTA_Diagnosa-diagnosa',
			[('diagnosa') : screenDiagnosa[0]]))

	GlobalVariable.dataParam1 = diagnosatambahan

	for (i = 0; i < GlobalVariable.dataParam1.size(); i++){

		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa_Tambahan'), GlobalVariable.dataParam1[i])

		ArrayList screenDiagnosa1 = REA.getArrayButton(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Diagnosa_Tambahan'),
				'tagname', 'li')

		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa_Tambahan'))

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/DTA_Diagnosa-diagnosa',
				[('diagnosa') : screenDiagnosa1[0]]))

	}

}