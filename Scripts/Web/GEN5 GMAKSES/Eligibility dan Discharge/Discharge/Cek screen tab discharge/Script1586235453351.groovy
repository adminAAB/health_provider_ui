import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


	WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Nomor_Peserta'), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Kembali'),
		1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'),
		1)
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusE.replace("GVProviderId",GlobalVariable.ProviderID))
	
	println ("GlobalVariable.DataParamL1 : " +GlobalVariable.DataParamL1)
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Nomor_Peserta'),
		GlobalVariable.DataParamL1[1])
	
	String inputan = GlobalVariable.DataParamL1[1] + " - " + GlobalVariable.DataParamL1[6]
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): inputan]))
	
	WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/BTN_Check'))
	
	WebUI.delay(2)
		
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Header_Treatment_Type_Discharge-jenisperawatan',[('jenisperawatan'): GlobalVariable.DataParamL1[12]]), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Diagnosa'), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LBL_Diagnosa_Tambahan'), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa'), 1)
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TXT_Diagnosa_Tambahan'), 1)
	
	ArrayList benefitID = REA.getOneColumnDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType",GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]), 'BenefitID')
	
	ArrayList ChargeFieldName = REA.getOneColumnDatabase('172.16.94.70', 'GMA', GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType",GlobalVariable.DataParamL1[11]).replace("GVMNO",GlobalVariable.DataParamL1[2]), 'ChargeFieldName')
	
	ArrayList sourceDataBiaya = ChargeFieldName
	
	ArrayList ListBenefitID = benefitID
	
	for (cf = 0; cf < benefitID.size(); cf++){
		
		if (ChargeFieldName[cf] == ChargeFieldName[cf+1]){
			
			ListBenefitID[cf] = ListBenefitID[cf] + "_" + ListBenefitID[cf+1]
			
			ListBenefitID.remove(ListBenefitID[cf+1])
			
			sourceDataBiaya.remove(sourceDataBiaya[cf+1])
			
		}
		
	}
	
	ArrayList screenFieldBiaya = REA.getAllText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/LST_Field_Biaya'), "div[contains(@class,\"discharge-form-value-item\")]")
		
	println ("sourceDataBiaya: "+ sourceDataBiaya)
	
	println ("screenFieldBiaya: "+ screenFieldBiaya)
	
	WebUI.verifyMatch(screenFieldBiaya.size().toString(), sourceDataBiaya.size().toString(), false)
	
	for (i = 0 ; i < sourceDataBiaya.size() ; i++) {
		
		WebUI.verifyMatch(screenFieldBiaya[i], sourceDataBiaya[i], false)
		
	}

	