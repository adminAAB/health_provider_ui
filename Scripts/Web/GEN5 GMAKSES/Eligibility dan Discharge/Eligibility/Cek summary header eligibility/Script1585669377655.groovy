import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

	WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), GlobalVariable.maxDelay)

	WebUI.verifyElementText(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), "GARDA MEDIKA AKSES")
	
	ArrayList SourceLabelSummary = ['Nama', 'Member No', 'NPK', 'Tanggal Lahir', 'Client', 'Effective Date', 'Tanggal Transaksi']
	
	println ("GlobalVariable.DataParamL1 : " + GlobalVariable.DataParamL1)
	
	ArrayList SourceDataSummary = new ArrayList()
	
	if (skenario == "JPInvalid"){
		
		SourceDataSummary = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySummaryHeaderEligibility.replace("GVMemberNo",GlobalVariable.DataParamL1[0]).replace("AND TreatmentType ='GVTreatmentType'",""))
	
	} else {
	
		SourceDataSummary = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QuerySummaryHeaderEligibility.replace("GVMemberNo",GlobalVariable.DataParamL1[0]).replace("GVTreatmentType",GlobalVariable.DataParamL1[10]))
	
	}
	
	ArrayList ScreenLabelSummary = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Header_Eligibility'), 1, true)
	
	ArrayList ScreenDataSummary = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Header_Eligibility'), 2, true)
		
	String aa = ScreenDataSummary[5].replace(': ','')
	
	ArrayList a = aa.split(" - ")
	
	String b = ": " + REA.convertDate(a[0], false)
	
	String c = REA.convertDate(a[1], false)
	
	String d = b + " - "+ c
	
	ScreenDataSummary.set(5, d)
	
	String PeriodePolis = ': ' + REA.convertDate(ScreenDataSummary[3].replace(': ',''), false)
	
	String TglTransaksi = ': ' + REA.convertDate(ScreenDataSummary[6].replace(': ',''), false)
	
	ScreenDataSummary.set(3, PeriodePolis)
	
	ScreenDataSummary.set(6, TglTransaksi)
	
	println ("screen : "+ ScreenLabelSummary + "source : "+ SourceLabelSummary)
	
	println ("screen : "+ ScreenDataSummary + "source : "+ SourceDataSummary)
	
	WebUI.verifyMatch(ScreenLabelSummary.size().toString(), SourceLabelSummary.size().toString(), false)
	
	WebUI.verifyMatch(ScreenDataSummary.size().toString(), SourceDataSummary.size().toString(), false)
	
	int i
	
	for (i = 0; i < SourceLabelSummary.size(); i++){
	
		WebUI.verifyMatch(ScreenLabelSummary[i], SourceLabelSummary[i], false, FailureHandling.STOP_ON_FAILURE)
	
		WebUI.verifyMatch(ScreenDataSummary[i], SourceDataSummary[i], false, FailureHandling.STOP_ON_FAILURE)
	
	}
	
	