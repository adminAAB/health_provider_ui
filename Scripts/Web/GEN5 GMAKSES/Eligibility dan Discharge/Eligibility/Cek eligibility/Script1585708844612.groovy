import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.DateFormat as DateFormat
import java.text.SimpleDateFormat as SimpleDateFormat
import java.util.Date as Date
import com.keyword.REA


/* PARAMETER YANG DIPAKAI
 * GlobalVariable.ProviderID = Parameter global yang berisi data ID provider (tidak perlu diinput)
 * nomorpeserta = parameter lokal yang diinput di skenario (pilihan data: unregistered, sudah terdaftar status E, sudah terdaftar status D, invalid sudah daftar, valid, terdaftar non H)
 * GlobalVariable.dataParam1, GlobalVariable.dataParam11 = Parameter global kosong yang tidak perlu diinput
 * 
 */

	WebUI.waitForElementVisible(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'), 10)
	
	WebUI.delay(10)
	
	if (skenario == "valid"){
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Clear'))
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValid.replace("GVTreatmentType", TreatmentType))
		
		println (GlobalVariable.QueryMemberValid.replace("GVTreatmentType", TreatmentType))

		println (GlobalVariable.DataParamL1)
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
				GlobalVariable.DataParamL1[0])

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		ArrayList SourceCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.DataParamL1[1], GlobalVariable.DataParamL1[5], GlobalVariable.ProviderID, GlobalVariable.DataParamL1[10], GlobalVariable.StatusEligible[2], 'null', 'null', '1','SYS']

		println (SourceCE)

		ArrayList SourceHCE = [GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[2], '1','SYS']

		println (SourceHCE)
		
		println (GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		println (GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		WebUI.delay(5)

		WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), GlobalVariable.maxDelay)
		
		println (REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0])))
		
		println (REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0])))
		
//		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceCE)

		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
		
	} else if (skenario == "validLimitMuncul"){
	
		println(GlobalVariable.QueryMemberValid.replace("Code IS NULL","Code IS NOT NULL"))
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValid.replace("Code IS NULL","Code IS NOT NULL").replace("GVTreatmentType", TreatmentType))
	
		println (GlobalVariable.DataParamL1)
			
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
				GlobalVariable.DataParamL1[0])

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		ArrayList SourceCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.DataParamL1[1], GlobalVariable.DataParamL1[5], GlobalVariable.ProviderID, GlobalVariable.DataParamL1[10], GlobalVariable.StatusEligible[2], 'null', 'null', '1','SYS']

		println (SourceCE)

		ArrayList SourceHCE = [GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[2], '1','SYS']

		println (SourceHCE)
		
		WebUI.delay(10)

		WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), GlobalVariable.maxDelay)
		
		println (GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		println (GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		println (REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0])))
		
//		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceCE)

		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
		
	
	} else if (skenario == "valid0") {	
		
		println (GlobalVariable.QueryMemberValid.replace("RemainingLimit = 0","RemainingLimit <> 0").replace("AND CE.MemberNo IS NULL AND MTT.ClientID IS NOT NULL AND MG.Code IS NULL AND GMAE.TreatmentType = 'GVTreatmentType'", "").replace(">=", "<="))
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValid.replace("RemainingLimit = 0","RemainingLimit <> 0").replace("AND CE.MemberNo IS NULL AND MTT.ClientID IS NOT NULL AND MG.Code IS NULL AND GMAE.TreatmentType = 'GVTreatmentType'", "").replace(">=", "<="))

		println (GlobalVariable.DataParamL1)
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
				GlobalVariable.DataParamL1[0])

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		ArrayList SourceCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.DataParamL1[1], GlobalVariable.DataParamL1[5], GlobalVariable.ProviderID, GlobalVariable.DataParamL1[10], GlobalVariable.StatusEligible[1], 'null', 'null', '1','SYS']
		
		println (SourceCE)
			
		ArrayList SourceHCE = [[GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[2], '1','SYS'], [GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[1], '1','SYS']]
		
		println (SourceHCE)
				
		WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), GlobalVariable.maxDelay)
		
		println (GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		println (REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0])))
		
		println (GlobalVariable.QueryDataInsertHCEStatusN.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceCE)
		
		REA.compareAllDatabasetoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
		
		//REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
	
	} else if (skenario == "Unregistered"){
	
			GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValid.replace("GVTreatmentType", TreatmentType))
	
			if (Flag == "XXX"){
	
				GlobalVariable.DataParamL1.set(0, GlobalVariable.DataParamL1[0] + "XXX")
	
			} else {
	
				GlobalVariable.DataParamL1.set(0, "'" + GlobalVariable.DataParamL1[0])
				
				WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_OK_PopUp_Member_Unregistered'))
				
			}
			
			WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
					GlobalVariable.DataParamL1[0])
	
			WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
					[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))
	
			WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
			
			WebUI.waitForElementVisible(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleUnregistered]), 120)
		
			WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleUnregistered]), 2)
				
			ArrayList SourceHCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[1], '1','SYS']
			
			println (SourceHCE)
			
			if (Flag == "XXX"){
				
				REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCEStatusN.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
				
			} else {
				
				REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCEStatusN.replace("GVMemberNo","'" + GlobalVariable.DataParamL1[0]), SourceHCE)
							
			}
					
	} else if (skenario == "StatusE"){
		
		println(GlobalVariable.QueryMemberValidStatusD.replace("ResultStatusClaim = 'D'","ResultStatusClaim = 'E'").replace("2","1"))
	
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusD.replace("ResultStatusClaim = 'D'","ResultStatusClaim = 'E'").replace("2","1"))
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
			GlobalVariable.DataParamL1[0])
	
		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
			[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))
	
		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleAlreadyRegistered]), 120)
	
		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleAlreadyRegistered]), 1)
		
		ArrayList SourceHCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[1], '1','SYS']
				
		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCEStatusN.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
		
	} else if (skenario == "InvalidB"){
	
		println (GlobalVariable.QueryPolisExpired)
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryPolisExpired)
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
			GlobalVariable.DataParamL1[0])

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
			[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		WebUI.waitForElementVisible(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleUnregistered]), 120)

		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleUnregistered]), 1)
		
		ArrayList SourceHCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[1], '1','SYS']
				
		WebUI.delay(5)
		
		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCEStatusN.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
		
	} else if (skenario == "StatusD") {
	
		println (GlobalVariable.QueryMemberValidStatusD)
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusD)

		println (GlobalVariable.DataParamL1)
		
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
				GlobalVariable.DataParamL1[0])
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))

		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		ArrayList SourceCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.DataParamL1[1], GlobalVariable.DataParamL1[5], GlobalVariable.ProviderID, GlobalVariable.DataParamL1[10], GlobalVariable.StatusEligible[2], 'null', 'null', '1','SYS']
		
		println (SourceCE)
		
		ArrayList SourceHCE = [GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[2], '1','SYS']
		
		println (SourceHCE)
		
		println (GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
				
		println (GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), GlobalVariable.maxDelay)
		
		println (REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0])))
		
		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceCE)
		
		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
	
	} else if (skenario == "StatusENonH") {
	
		println (GlobalVariable.QueryMemberValidStatusD.replace("= 'D'","= 'E'").replace(">","<").replace("2","1"))
		
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValidStatusD.replace("= 'D'","= 'E'").replace("CE.EntryDt >","CE.EntryDt <").replace("2","1"))

		println (GlobalVariable.DataParamL1)

		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
				GlobalVariable.DataParamL1[0])

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		ArrayList SourceCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.DataParamL1[1], GlobalVariable.DataParamL1[5], GlobalVariable.ProviderID, GlobalVariable.DataParamL1[10], GlobalVariable.StatusEligible[2], 'null', 'null', '1','SYS']

		println (SourceCE)

		ArrayList SourceHCE = [GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[2], '1','SYS']

		println (SourceHCE)
		
		WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), GlobalVariable.maxDelay)

		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceCE)

		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCE.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
		
	} else if (skenario == "JPInvalid") {
	
		GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.QueryTreatmentTypeMember)

		println("'"+GlobalVariable.DataParamL1[0]+"'")
		
		println(GlobalVariable.DataParamL1)
		
		println(GlobalVariable.QueryTreatmentTypeMember)
		
		ArrayList JP = REA.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.QueryTreatmentTypeMember, "TreatmentType")

		println (JP)
		
		String penampungJP = ''

		int i

		for (i = 0 ; i < JP.size() ; i++) {

			if (i == JP.size()-1) {

				penampungJP += "'" + JP[i] + "'"
			} else {

				penampungJP += "'" + JP[i] + "',"
			}
		}

		println (penampungJP)
		
		String TreatmentType = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryTreatmentType.replace("Code","TOP 1 Code").replace("ORDER BY","AND Code NOT IN ("+ penampungJP +") ORDER BY"), "Code")
		
		println (GlobalVariable.QueryTreatmentType.replace("Code","TOP 1 Code").replace("ORDER BY","AND Code NOT IN ("+ penampungJP +") ORDER BY"))
		
		GlobalVariable.DataParamL1.set(10, TreatmentType)

		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'),
				GlobalVariable.DataParamL1[0])

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.DataParamL1[10]]))

		WebUI.click(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))

		ArrayList SourceHCE = ['null', GlobalVariable.DataParamL1[0], GlobalVariable.StatusEligible[1], '1','SYS']
			
		println (SourceHCE)

		println (GlobalVariable.QueryDataInsertHCEStatusN.replace("GVMemberNo",GlobalVariable.DataParamL1[0]))
		
		WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Header_Eligibility'), GlobalVariable.maxDelay)
		
		REA.compareRowDBtoArray("172.16.94.70", "GMA", GlobalVariable.QueryDataInsertHCEStatusN.replace("GVMemberNo",GlobalVariable.DataParamL1[0]), SourceHCE)
			
	}

	
	
	
	
	
	
	
	
	