import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.delay(1)

	if (skenario == 'allblank'){
	
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_OK_Popup_Mandatory'))
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Clear'))
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Validasi_Mandatory'), 2)
		
		WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Validasi_Mandatory'), "Mohon isi Nomor Peserta & pilih Jenis Perawatan terlebih dahulu")
		
		WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_OK_Popup_Mandatory'), "Ok")
			
	} else if (skenario == "nomorpesertablank") {
    
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Jenis_Perawatan-jenisperawatan', 
            [('jenisperawatan') : jenisperawatan]))

		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Validasi_Mandatory'), 2)
		
		WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Validasi_Mandatory'), "Mohon isi Nomor Peserta")
		
		WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_OK_Popup_Mandatory'), "Ok")
			
	} else if (skenario == "jpblank") {
	
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_OK_Popup_Mandatory'))
		
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Clear'))
	
		String nopeserta = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryMemberValid.replace("GVTreatmentType", TreatmentType), "MemberNo")
	
	    WebUI.setText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'), nopeserta)
	
		WebUI.click(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'))
		
		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Validasi_Mandatory'), 2)
			
		WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_Validasi_Mandatory'), "Mohon pilih Jenis Perawatan terlebih dahulu")
		
		WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_OK_Popup_Mandatory'), "Ok")
		
	}
	
	

	

