import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.text.NumberFormat
import java.util.Locale


		if (Flag == "Biaya"){

			ArrayList SourceLabelSummary = new ArrayList()

			ArrayList SourceDataSummary = new ArrayList()
			
			SourceLabelSummary.add(GlobalVariable.DataParamL1[11])

			String countLimit = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryCountFlagLimit.replace("GVClientID", GlobalVariable.DataParamL1[3]), "countL")

			ArrayList ScreenLabelSummary = REA.getColumnDataTableUI(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility'), 1 , false)
			
			ArrayList ScreenFieldBiaya = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility_Biaya'), 1 , false)
			
			ArrayList ScreenDataBiaya = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility_Biaya'), 2 , false)
			
			ArrayList ScreenFieldKeteranganTambahan = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility_KeteranganTambahan'), 1 , false)
			
			ArrayList ScreenDataKeteranganTambahan = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility_KeteranganTambahan'), 2 , false)		
			
			ArrayList ScreenDataSummary = new ArrayList()
								
			if (countLimit != '0'){

				ArrayList Label = ["Limit Saat ini", "Coverage:"]

				for (a= 0 ; a < Label.size(); a++) {

					SourceLabelSummary.add(Label[a])

				}

				String GetLimit = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetLimitFormat.replace("GVMNO",GlobalVariable.DataParamL1[1]).replace("GVProductType",GlobalVariable.DataParamL1[10]),"Limit")
				
				SourceDataSummary.add(GetLimit)
				
				ScreenDataSummary = REA.getColumnDataTableUI(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility'), 2 , false)
			

			} else {

				ArrayList Label = ["Coverage:"]

				for (a= 0 ; a < Label.size(); a++) {

					SourceLabelSummary.add(Label[a])
					
				}

			}
			
			for (b= 0 ; b < ScreenFieldBiaya.size(); b++) {
				
				ScreenLabelSummary.add(ScreenFieldBiaya[b])
				
			}
			
			for (bb= 0 ; bb < ScreenDataBiaya.size(); bb++) {
				
				ScreenDataSummary.add(ScreenDataBiaya[bb])
				
			}
			
			for (c= 0 ; c < ScreenFieldKeteranganTambahan.size(); c++) {
									
				ScreenLabelSummary.add(ScreenFieldKeteranganTambahan[c])
									
			}
			
			for (cc= 0 ; cc < ScreenDataKeteranganTambahan.size(); cc++) {
								
				ScreenDataSummary.add(ScreenDataKeteranganTambahan[cc])
							
			}
			
			ArrayList SourceBiaya = REA.getOneColumnDatabase("172.16.94.70", "GMA", GlobalVariable.QueryFieldBiaya.replace("GVTreatmentType",GlobalVariable.DataParamL1[10]).replace("GVMNO", GlobalVariable.DataParamL1[1]), "ChargeFieldName")
			
			ArrayList SourceLabelTambahan = REA.getOneColumnDatabase("172.16.94.70", "GMA", GlobalVariable.QueryLabelRemarks.replace("GVClientID", GlobalVariable.DataParamL1[3]).replace("GVTreatmentType", GlobalVariable.DataParamL1[10]), "Remarks")

			ArrayList SourceDescTambahan = REA.getOneColumnDatabase("172.16.94.70", "GMA", GlobalVariable.QueryDescRemarks.replace("GVClientID", GlobalVariable.DataParamL1[3]).replace("GVTreatmentType", GlobalVariable.DataParamL1[10]), "Remarks")

			for (cf = 0; cf < SourceBiaya.size(); cf++){
				
				if (SourceBiaya[cf] == SourceBiaya[cf+1]){
					
					SourceBiaya.remove(SourceBiaya[cf+1])
					
					println (SourceBiaya)
					
				}
				
			}

			for (d= 0 ; d < SourceBiaya.size(); d++) {

				SourceLabelSummary.add(SourceBiaya[d])

				SourceDataSummary.add("Sesuai Tagihan")
			}

			String StringTambahan3 = "Keterangan Tambahan:"

			if (SourceLabelTambahan.size() > 0){

					SourceLabelSummary.add(StringTambahan3)
					
			}

			for (e = 0 ; e < SourceLabelTambahan.size(); e++) {

				SourceLabelSummary.add(SourceLabelTambahan[e])

				SourceDataSummary.add(SourceDescTambahan[e])
			}

			println ("SourceLabelSummary: " + SourceLabelSummary)

			println ("ScreenLabelSummary: " + ScreenLabelSummary)

			println ("SourceDataSummary: " + SourceDataSummary)

			println ("ScreenDataSummary: " + ScreenDataSummary)

			WebUI.verifyMatch(ScreenLabelSummary.size().toString(), SourceLabelSummary.size().toString(), false)

			WebUI.verifyMatch(ScreenDataSummary.size().toString(), SourceDataSummary.size().toString(), false)

			for (f = 0; f < SourceLabelSummary.size(); f++){

				WebUI.verifyMatch(ScreenLabelSummary[f], SourceLabelSummary[f], false, FailureHandling.STOP_ON_FAILURE)

			}

			for (g = 0; g < SourceDataSummary.size(); g++){

				WebUI.verifyMatch(ScreenDataSummary[g], SourceDataSummary[g], false, FailureHandling.STOP_ON_FAILURE)

			}

		} else if (Flag == "tanpaBiaya") {

			ArrayList SourceLabelSummary = new ArrayList()
			
			ArrayList SourceDataSummary = new ArrayList()

			ArrayList ScreenLabelSummary = new ArrayList()

			ArrayList ScreenDataSummary = new ArrayList()		

			if (cek =="limit0"){
				
				SourceLabelSummary.add (GlobalVariable.DataParamL1[11])
				
				String countLimit = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryCountFlagLimit.replace("GVClientID", GlobalVariable.DataParamL1[3]), "countL")

				println (countLimit)

				if (countLimit != "0"){

					ArrayList Label = ["Limit Saat ini"]

					int a

					for (a= 0 ; a < Label.size(); a++) {

						SourceLabelSummary.add(Label[a])
						
						SourceDataSummary.add(GlobalVariable.DataParamL1[18].replace('Rp','').replace('.','').replace(' ','') + ".00")
						
					}

					ScreenDataSummary = REA.getColumnDataTableUI(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility'), 2, false)

				}

				ScreenLabelSummary = REA.getColumnDataTableUI(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility'), 1, false)
				
				ArrayList ScreenFieldKeterangan = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility_Biaya'), 1 , false)
				
				for (ss = 0; ss < ScreenFieldKeterangan.size(); ss++){
					
					ScreenLabelSummary.add(ScreenFieldKeterangan[ss])
					
				}

				String wordingL = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryWordingLimitHabisOrBenefit.replace("Eligibility","EligibilityL"), "Description")

				SourceLabelSummary.add(wordingL)

				WebUI.verifyMatch(SourceLabelSummary.size().toString(), ScreenLabelSummary.size().toString(), false)

				WebUI.verifyMatch(SourceDataSummary.size().toString(), ScreenDataSummary.size().toString(), false)

				for (f = 0; f < SourceLabelSummary.size(); f++){

					WebUI.verifyMatch(ScreenLabelSummary[f], SourceLabelSummary[f], false, FailureHandling.STOP_ON_FAILURE)

				}

				for (g = 0; g < SourceDataSummary.size(); g++){

					WebUI.verifyMatch(ScreenDataSummary[g], SourceDataSummary[g], false, FailureHandling.STOP_ON_FAILURE)

				}

			} 	else if (cek =="benefit") {
			
				String DescTreatmentType = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryTreatmentType.replace("Code","TOP 1 Code").replace("ORDER BY","AND Code = '" + GlobalVariable.DataParamL1[10] + "' ORDER BY"), "Description")
				
				SourceLabelSummary.add(DescTreatmentType)

				ScreenLabelSummary = REA.getColumnDataTableUI(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility'), 1, false)
			
				ArrayList ScreenFieldKeterangan = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TBL_Detail_Eligibility_Biaya'), 1 , false)
				
				for (ss = 0; ss < ScreenFieldKeterangan.size(); ss++){
					
					ScreenLabelSummary.add(ScreenFieldKeterangan[ss])
					
				}
				
				String wordingB = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.QueryWordingLimitHabisOrBenefit.replace("Eligibility","EligibilityB"), "Description")
			
				SourceLabelSummary.add(wordingB)

				println ("ScreenLabelSummary: " + ScreenLabelSummary)

				println ("SourceLabelSummary: " + SourceLabelSummary)

				WebUI.verifyMatch(SourceLabelSummary.size().toString(), ScreenLabelSummary.size().toString(), false)

				for (f = 0; f < SourceLabelSummary.size(); f++){

					WebUI.verifyMatch(ScreenLabelSummary[f], SourceLabelSummary[f], false, FailureHandling.STOP_ON_FAILURE)

				}


			}


		}

	