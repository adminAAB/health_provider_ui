import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.keyword.GEN5 as GEN5
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'), GlobalVariable.maxDelay)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TAB_Eligibility'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/TAB_Discharge'), 1)

WebUI.verifyElementText(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LBL_NomorPeserta'), 'Nomor Peserta')

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/TXT_NomorPeserta'), 1)

WebUI.verifyElementNotPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_QR_Code'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Kembali'), 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'), 1)

WebUI.verifyElementAttributeValue(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Kembali'), 'value',
	'Kembali', 1)

WebUI.verifyElementAttributeValue(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BTN_Daftar'), 'value',
	'Daftar', 1)

WebUI.verifyElementPresent(findTestObject('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/BOX_Status_Eligibility'),
	1)
