import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

if (cek == "textAttribute") {

	String QueryTextLabel = "select MenuCaption from general.applicationmenus where menucaption in (\'status pembayaran\',\'daftar transaksi\',\'riwayat transaksi\') ORDER BY OrderingNo ASC"
	
	ArrayList sourceMenu = REA.getOneColumnDatabase("172.16.94.74", "a2isAuthorizationDB", QueryTextLabel, "MenuCaption")
	
	ArrayList screenMenu = REA.getArrayButton(xpath, tagname,value)
	
	println (screenMenu)
	
	for (i = 0 ; i < sourceMenu.size() ; i++) {
		
		println (screenMenu[i])
		
		WebUI.verifyMatch(screenMenu[i], sourceMenu[i], false)
		
	}

} else if (cek == "tagtAttribute") {

	String QueryIcon = query
	
	ArrayList sourceTextLabel = REA.getOneColumnDatabase(ipDB, NamaDB, QueryIcon, NamaKolom)
	
	ArrayList screenTextLabel = REA.getArrayIcon(xpath)
	
	for (i = 0 ; i < sourceTextLabel.size() ; i++) {
		
		WebUI.verifyMatch(screenTextLabel[i], sourceTextLabel[i], false)
		
	}
	
}