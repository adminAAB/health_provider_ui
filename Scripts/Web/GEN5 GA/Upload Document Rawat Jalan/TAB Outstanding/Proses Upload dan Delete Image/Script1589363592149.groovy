import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.keyword.GEN5
import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'), 2, FailureHandling.OPTIONAL)

if (processing) {
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
	
}

ArrayList AllDokValid = ["a.jpg","b.jpg","c.jpg","d.jpg","e.pdf","f.pdf","g.pdf","h.pdf","i.pdf","j.pdf","k.pdf","l.pdf","jpeg_74-2.jpeg","image2.PNG","image3.PNG","image5.PNG","aa.jpg","2020-03-01_171659.jpg","download (1).jpg","download (2).jpg","download (3).jpg","download (4).jpg","download (5).jpg"]

ArrayList DokValid = ["a.jpg","j.pdf","jpeg_74-2.jpeg","image2.PNG"]

ArrayList DokInvalidDataType = ["powerpoint.ppt","test.gif","BMP-01.bmp","MP.docx"]

String DokValidMaxSize = 'Valid.png'

String DokInvalidSize = 'Invalid-16MB.jpg'

if (Skenario == "RandomUploadDocument"){
	
	int JumlahUpload = (Math.random() * 4)+1 as int
	
	if (JumlahUpload == 1){
		
		JumlahUpload = 2
		
	}
	
	for (x = 0; x < JumlahUpload; x++){
		
		int random = Math.floor(Math.random()* AllDokValid.size())
		
		String Image = AllDokValid[random].toString()
		
		AllDokValid.remove(random)
		
		GlobalVariable.DataParamL3.add(Image)
	}
	
	println (JumlahUpload)
	
	println (GlobalVariable.DataParamL3)
	
	for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
		
		WebUI.delay(1)
	
		UI.UploadFile2(GlobalVariable.DataParamL3[i].toString())
			
		GEN5.ProcessingCommand()
		
	}
	
} else if (Skenario == "DeleteDocument"){

	ArrayList img1 = new ArrayList()
	
	String index = 1
	
	println (GlobalVariable.DataParamL3)
		
	img1 = GlobalVariable.DataParamL3[0].toString().split("\\.")
		
	GlobalVariable.DataParamS2 = img1[0]
	
	GlobalVariable.DataParamS3 = GlobalVariable.DataParamL3[0]
	
	GlobalVariable.DataParamL4 = GlobalVariable.DataParamL3
	
	if (GlobalVariable.DataParamL3.size() > 1) {
		
		GlobalVariable.DataParamL4.remove(0)
		
	}
	
	println (GlobalVariable.DataParamL3)
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BOX_Image-index', [('index'): index]))

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Delete_Document'))
	
} else if (Skenario == "RandomUploadDocumentSubmit"){
	
	int JumlahUpload = (Math.random() * 4)+1 as int
	
	if (JumlahUpload == 1){
		
		JumlahUpload = 2
		
	}
	
	for (x = 0; x < JumlahUpload; x++){
		
		int random = Math.floor(Math.random()* AllDokValid.size())
		
		String Image = AllDokValid[random].toString()
		
		AllDokValid.remove(random)
		
		GlobalVariable.DataParamL3.add(Image)
		
	}
	
	println (JumlahUpload)
	
	println (GlobalVariable.DataParamL3)
	
	for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
		
		WebUI.delay(1)
	
		UI.UploadFile2(GlobalVariable.DataParamL3[i].toString())
			
		GEN5.ProcessingCommand()
		
	}
	
	int runningnumber = Math.floor(Math.random()*99999)
	
	GlobalVariable.DataParamS1 =  "EUM/SPRINT9/20/"+ runningnumber
	
	WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'), GlobalVariable.DataParamS1)
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))
	
} else if (Skenario == "InputInvoice"){

	int runningnumber = Math.floor(Math.random()*99999)
	
	GlobalVariable.DataParamS1 =  "EUM/SPRINT9/20/"+ runningnumber
	
	WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'), GlobalVariable.DataParamS1)
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))

} else if (Skenario == "MaxUploadDocument"){
	
	int JumlahUpload = 20 
	
	GlobalVariable.DataParamL3 = []
	
	println (AllDokValid)
	
	println (GlobalVariable.DataParamL3)
	
	for (x = 0; x < JumlahUpload; x++){
		
		int random = Math.floor(Math.random()* AllDokValid.size())
		
		String Image = AllDokValid[random].toString()
		
		AllDokValid.remove(random)
		
		GlobalVariable.DataParamL3.add(Image)
	}
	
	println (JumlahUpload)
	
	println (GlobalVariable.DataParamL3)
	
	for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
		
		WebUI.delay(1)
	
		UI.UploadFile2(GlobalVariable.DataParamL3[i].toString())
			
		GEN5.ProcessingCommand()
		
	}
	
} else if (Skenario == "Upload_InvalidMaxSize"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
	
	UI.UploadFile2(DokInvalidSize)
		
	GEN5.ProcessingCommand()

} else if (Skenario == "Upload_ValidMaxSize"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
	
	GlobalVariable.DataParamL3 = DokValidMaxSize
	
	UI.UploadFile2(DokValidMaxSize)
		
	GEN5.ProcessingCommand()
	
} else if (Skenario == "Upload_InvalidDataType"){
	
	if (Param == "bmp"){
		
		GlobalVariable.DataParamS1 = DokInvalidDataType[2].toString()
		
	} else if (Param == "ppt"){
	
		GlobalVariable.DataParamS1 = DokInvalidDataType[2].toString()
		
	} else if (Param == "gif"){
	
		GlobalVariable.DataParamS1 = DokInvalidDataType[1].toString()
		
	}

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
	
	UI.UploadFile2(GlobalVariable.DataParamS1)
		
	GEN5.ProcessingCommand()

}
