import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.keyword.GEN5
import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

GEN5.ProcessingCommand()

if (Skenario == "CekScreenUpload"){
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/LBL_Nama_Peserta'), "Nama Peserta")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/LBL_Tanggal_Perawatan'), "Tanggal Perawatan")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Clear'), "Clear")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'), "Search")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Back'), "Back")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Upload'), "Upload")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TAB_Pending'), "Pending")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TAB_Outstanding'), "Outstanding")
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'))
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Tanggal_Perawatan_To'))
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Tanggal_Perawatan_From'))	
	
	WebUI.verifyElementVisible(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'))
	
	WebUI.verifyElementVisible(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_Member'))
	
	WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/LBL_Nomor_GL'), "Nomor GL")
	
	WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/LBL_Nomor_Member'), "Nomor Member")
	
	WebUI.switchToFrame(findTestObject('Object Repository/Web/GEN5 GA/Frame'), GlobalVariable.maxDelay)	
	
	ArrayList ScreenHeaderTable = CustomKeywords.'code.General.GetColumnNameTable'(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/TBL_Grid'))
	
	WebUI.switchToDefaultContent()
	
	ArrayList SourceHeaderTable = ['No','Nama Peserta', 'Perusahaan','Tanggal Perawatan', 'Diagnosa']
	
	println ("ScreenHeaderTable: " +ScreenHeaderTable)
	
	println ("SourceHeaderTable: "+ SourceHeaderTable)
	
	WebUI.verifyMatch(ScreenHeaderTable.size().toString(), SourceHeaderTable.size().toString(), false)
	
	for (i = 1; i < SourceHeaderTable.size(); i++){
		
		if (ScreenHeaderTable[i] == SourceHeaderTable[i]) {
			
			KeywordUtil.markPassed("Value " + ScreenHeaderTable[i] +" from Grid Table same with Database.")
			
		} else {
		
			KeywordUtil.markFailedAndStop("Value from Grid Table = " + ScreenHeaderTable[i] + " has different Value from database = " + SourceHeaderTable[i])
		
		}
	}
	
} else if (Skenario == "SearchNotFound"){
	
	WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), "axxcd")
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
	
	GEN5.ProcessingCommand()

	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Data tidak ditemukan.")
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))

} else if (Skenario == "ClearDataParam"){

	ArrayList DataCNO = REA.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID), "CNO")
	
	String CNO = DataCNO[0].toString()
	
	ArrayList DataParam = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID + "' AND CH2.CNO = '"+ CNO ))

	ArrayList a = DataParam[4].split(" ")
	
	String Date = a[0] + "/" + a[1] + "/" + a[2]
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_From'))	
	
	GEN5.DatePicker(Date, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_From'))
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_To'))
	
	GEN5.DatePicker(Date, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_To'))
	
	WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), DataParam[2])
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_Member'), " /00599")
	
	WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'), "358984-1/GL/HID/IV/2020")	
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Clear'))
	
	GEN5.ProcessingCommand()
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), "")
	
	WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_Member'), "")
	
	WebUI.verifyElementText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'), "")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Tanggal_Perawatan_From'), "")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Tanggal_Perawatan_To'), "")

} else if (Skenario == "KlikBack"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Back'))
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/LBL_PopUp_Back'),"Close application and go back to Menu page?")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Cancel_PopUp_Back'),"Cancel")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Yes_PopUp_Back'),"Yes, Close Application")
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Yes_PopUp_Back'))
	
	WebUI.verifyElementVisible(findTestObject('Object Repository/Web/GEN5 GA/Homepage/BTN_Upload_Document_Rawat_Jalan'))
	
} else if (Skenario == "MaxFieldNamaPeserta"){

	boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'), 2, FailureHandling.OPTIONAL)
	
	if (processing) {
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	}

	String input = "BERNADETA TITI NUR'ANI NADIA ALITA SYAHNAZ ALICIACAROLITA"

	WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), input)
	
	String ScreenNamaPeserta = WebUI.getAttribute(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), "value")

	String SourceNamaPeserta = input.substring(0, 50)
	
	println (SourceNamaPeserta + " - "+ ScreenNamaPeserta)
	
	if (ScreenNamaPeserta == SourceNamaPeserta) {
		
			KeywordUtil.markPassed("Value " + ScreenNamaPeserta +" from screen same with source.")
			
	} else {
						
			KeywordUtil.markFailedAndStop("Value from Grid Table = " + ScreenNamaPeserta + " has different Value from source = " + SourceNamaPeserta)
			
	}

} else if (Skenario == "KlikOKNotif"){

	//boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Upload_Document'), 2,FailureHandling.OPTIONAL)
	
	boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Notifikasi'), 2,FailureHandling.OPTIONAL)
	
	if (processing){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	}

} else if (Skenario == "KlikUpload"){ 

	ArrayList DataNo = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID).replace("DESC","ASC"))
	
	String RowNo

	if (Param == "NotUpload"){
	
		for (i = 0; i < DataNo.size(); i++){
			
			if ((DataNo[i])[6] == "Not Uploaded"){
							
				RowNo = (DataNo[i])[1]
				
				break
				
			}
			
		}
	
	} else {
	
		for (i = 0; i < DataNo.size(); i++){
			
			if ((DataNo[i])[6] == "Uploaded"){
							
				RowNo = (DataNo[i])[1]
				
				break
				
			}
		}	
	}
	
	//GEN5.ClickExpectedRow(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/TBL_Grid'), "No", RowNo)
	
	GEN5.ClickExpectedRowWithNext(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/TBL_Grid'), "No", RowNo, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Next_Page'))
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Upload'))

	GEN5.ProcessingCommand()
	
	GlobalVariable.DataParamL1 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID).replace("AA","AA WHERE AA.ROW = '"+ RowNo +"'"))
	
	println (GlobalVariable.DataParamL1)
	
} else if (Skenario == "CekScreenPopUpUpload"){

	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Header_Screen'), "Upload Document")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nomor_Invoice'), "Nomor Invoice")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'), "Submit")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'), "Upload Document")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Delete_Document'), "Delete Document")
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_List_Dokumen'))
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'))
	
} else if (Skenario == "ValidasiPopUpUpload"){

	if (Param == "AllBlank"){
	
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))
		
		WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), 2)
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Harap mengisi nomor invoice terlebih dahulu.")
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	} else if (Param == "ImageBlank"){
	
		//WebUI.clearText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'))
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))

		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Harap upload minimal 1 foto dokumen.")
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	} else if (Param == "NomorInvoiceBlank"){
	
		WebUI.clearText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'))
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
		
		UI.UploadFile2("jpeg_74-2.jpeg")
			
		GEN5.ProcessingCommand()
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Harap mengisi nomor invoice terlebih dahulu.")
	
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	} else if (Param == "MaxFieldNomorInvoice"){
	
		String input = "EUM/01/24/2020/00012001" 
	
		WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'), input)
		
		String ScreenNoInvoice = WebUI.getAttribute(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'), "value") 
	
		String SourceNoInvoice = input.substring(0, 20)
		
		if (ScreenNoInvoice == SourceNoInvoice) {
			
				KeywordUtil.markPassed("Value " + ScreenNoInvoice +" from screen same with source.")
				
		} else {
							
				KeywordUtil.markFailedAndStop("Value from Grid Table = " + ScreenNoInvoice + " has different Value from source = " + SourceNoInvoice)
				
		}

	} else if (Param == "Upload_InvalidDataType"){
	
		WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), GlobalVariable.maxDelay)

		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "File yang boleh diupload: JPG, JPEG, PNG, TIFF, PDF.") 
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
	
	} else if (Param == "Upload_InvalidMaxSize"){
	
		GEN5.ProcessingCommand()
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Maksimum ukuran file yang boleh diupload 15 MB.") 
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	}	
	
} else if (Skenario == "KlikX"){

	boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Upload_Document'), 2,FailureHandling.OPTIONAL)
	
	//boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Notifikasi'), 2,FailureHandling.OPTIONAL)
	
	if (processing){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_X_PopUp_Upload'))		
		
	} 	
	
} else if (Skenario == "UploadImage"){
		
	println (GlobalVariable.DataParamL3)
	
	if (GlobalVariable.DataParamL3 == "Valid.png"){
		
		ArrayList img = GlobalVariable.DataParamL3.toString().split("\\.")
		
		println (img)
		
		GlobalVariable.DataParamL2.add(img[0])
				
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-index', [('index') : "1"]),
			img[0].toString())
		
	} else {
	
		for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
			
			ArrayList img = (GlobalVariable.DataParamL3[i]).toString().split("\\.")
			
			println (img)
			
			GlobalVariable.DataParamL2.add(img[0])
					
			WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-index', [('index') : (i+1).toString()]),
				img[0].toString())
			
		}
	
	}

} else if (Skenario == "DeleteImage"){

	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Apakah anda yakin ingin menghapus dokumen ini?")
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Delete'))
	
} else if (Skenario == "YesDeleteImage"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Delete_Document'))

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Delete'))
	
	WebUI.delay(2)
	
	GEN5.ProcessingCommand()
	
	println (GlobalVariable.DataParamL4)
	
	if (GlobalVariable.DataParamL4.size() == 1){
		
		println(GlobalVariable.DataParamS2)
		
		boolean processing = WebUI.waitForElementNotPresent(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): GlobalVariable.DataParamS2]), 2,FailureHandling.OPTIONAL)
		
		if (processing){
			
			KeywordUtil.markPassed("Document is deleted")
			
		} else {
		
			KeywordUtil.markFailedAndStop("Failed..document not deleted")
		
		}
		
	} else {
	
		boolean processing = WebUI.waitForElementNotPresent(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): GlobalVariable.DataParamS2]), 2, FailureHandling.OPTIONAL)
		
		if (processing){			
			KeywordUtil.markPassed("Document is deleted")			
		} else {		
			KeywordUtil.markFailedAndStop("Failed..document not deleted")
		}
	
		for (i = 0; i < GlobalVariable.DataParamL4.size(); i++){
		
			ArrayList image = (GlobalVariable.DataParamL4[i]).toString().split("\\.")
			
			boolean muncul = WebUI.waitForElementPresent(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): image[0]]), 2)
			
			if (muncul){
				KeywordUtil.markPassed("Document dispayed")
			} else {
				KeywordUtil.markFailedAndStop("Failed..document not dispayed")
			}
			
		}
	
	}

} else if (Skenario == "NoDeleteImage"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
	
	WebUI.delay(2)

	for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
	
		ArrayList img = (GlobalVariable.DataParamL3[i]).toString().split("\\.")
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): img[0]]))
	}

} else if (Skenario == "SubmitImage"){
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Apakah anda yakin ingin mengirimkan dokumen claim ini?")

	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Notifikasi'))	

} else if (Skenario == "YesPopUpSubmitTanpaVerify"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Notifikasi'))
	
} else if (Skenario == "YesPopUpSubmit"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Notifikasi'))	
	
	WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Notifikasi'), 2, FailureHandling.OPTIONAL)
	
	boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), 2, FailureHandling.OPTIONAL)
	
	if (processing) {
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Dokumen berhasil di-Submit.")
		
	}

	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'), FailureHandling.OPTIONAL)
	
} else if (Skenario == "NoPopUpSubmit"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
		
} else if (Skenario == "ListDocument"){

	String ScreenListDokumen = WebUI.getAttribute(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_List_Dokumen'), "value")
	
	println (ScreenListDokumen)
	
	ArrayList ScreenData = new ArrayList()
	
	ArrayList xx = ScreenListDokumen.split("- ")
	
	for (ee = 0; ee < xx.size(); ee++){
		
		ScreenData.add(xx[ee].trim())
		
	}

	println (GlobalVariable.DataParamL1)
	
	ArrayList SourceData = new ArrayList()	
	
	String QueryPrimary 
	
	String QueryOptional 
	
	String bill
	
	if (GlobalVariable.DataParamL1[8].toString().contains("E")) {
		
//		String aa = GlobalVariable.DataParamL1[8].toString().replace(".", "")
//	
//		String bill = aa.substring(0, aa.length()-2)
		
		BigDecimal a = new BigDecimal(GlobalVariable.DataParamL1[8].toString())
		
		bill = a.longValue()
		
		println (bill)
		
		QueryPrimary = GlobalVariable.GA_QueryGetListDocument.replace("@0 = '', @1 = '', @2 = '', @3 = 0, @4 = '', @5 = '', @6 = '0', @7 = 0, @8 = 0, @9 = '0', @10 = 0, @11 = 0, @12 = '', @13 = '', @14 = '', @15 = 2, @16 = 0, @17 = 0, @18 = ''","@0 = '"+ GlobalVariable.DataParamL1[6] +"', @1 = '', @2 = 'C', @3 = "+ bill +", @4 = '"+ GlobalVariable.DataParamL1[9] +"', @5 = '"+ GlobalVariable.DataParamL1[10] +"', @6 = '0', @7 = 0, @8 = 0, @9 = '0', @10 = 0, @11 = 0, @12 = '"+ GlobalVariable.DataParamL1[11] +"', @13 = '"+ GlobalVariable.DataParamL1[12] +"', @14 = '"+ GlobalVariable.DataParamL1[13] +"', @15 = '"+ GlobalVariable.DataParamL1[14] +"', @16 = '"+ GlobalVariable.DataParamL1[15] +"', @17 = 0, @18 = ''")
		
		QueryOptional = GlobalVariable.GA_QueryGetListDocument.replace("@0 = '', @1 = '', @2 = '', @3 = 0, @4 = '', @5 = '', @6 = '0', @7 = 0, @8 = 0, @9 = '0', @10 = 0, @11 = 0, @12 = '', @13 = '', @14 = '', @15 = 2, @16 = 0, @17 = 0, @18 = ''","@0 = '"+ GlobalVariable.DataParamL1[6] +"', @1 = 'CTSCN|CMRI|CPA|CLAB|CRONT|CUSG', @2 = 'C', @3 = "+ bill +", @4 = '"+ GlobalVariable.DataParamL1[9] +"', @5 = '"+ GlobalVariable.DataParamL1[10] +"', @6 = '1', @7 = 1, @8 = 1, @9 = '1', @10 = 1, @11 = 1, @12 = '"+ GlobalVariable.DataParamL1[11] +"', @13 = '"+ GlobalVariable.DataParamL1[12] +"', @14 = '"+ GlobalVariable.DataParamL1[13] +"', @15 = '"+ GlobalVariable.DataParamL1[14] +"', @16 = '"+ GlobalVariable.DataParamL1[15] +"', @17 = 1, @18 = ''")
		
	} else {
	
		QueryPrimary = GlobalVariable.GA_QueryGetListDocument.replace("@0 = '', @1 = '', @2 = '', @3 = 0, @4 = '', @5 = '', @6 = '0', @7 = 0, @8 = 0, @9 = '0', @10 = 0, @11 = 0, @12 = '', @13 = '', @14 = '', @15 = 2, @16 = 0, @17 = 0, @18 = ''","@0 = '"+ GlobalVariable.DataParamL1[6] +"', @1 = '', @2 = 'C', @3 = "+ GlobalVariable.DataParamL1[8] +", @4 = '"+ GlobalVariable.DataParamL1[9] +"', @5 = '"+ GlobalVariable.DataParamL1[10] +"', @6 = '0', @7 = 0, @8 = 0, @9 = '0', @10 = 0, @11 = 0, @12 = '"+ GlobalVariable.DataParamL1[11] +"', @13 = '"+ GlobalVariable.DataParamL1[12] +"', @14 = '"+ GlobalVariable.DataParamL1[13] +"', @15 = '"+ GlobalVariable.DataParamL1[14] +"', @16 = '"+ GlobalVariable.DataParamL1[15] +"', @17 = 0, @18 = ''")
		
		QueryOptional = GlobalVariable.GA_QueryGetListDocument.replace("@0 = '', @1 = '', @2 = '', @3 = 0, @4 = '', @5 = '', @6 = '0', @7 = 0, @8 = 0, @9 = '0', @10 = 0, @11 = 0, @12 = '', @13 = '', @14 = '', @15 = 2, @16 = 0, @17 = 0, @18 = ''","@0 = '"+ GlobalVariable.DataParamL1[6] +"', @1 = 'CTSCN|CMRI|CPA|CLAB|CRONT|CUSG', @2 = 'C', @3 = "+ GlobalVariable.DataParamL1[8] +", @4 = '"+ GlobalVariable.DataParamL1[9] +"', @5 = '"+ GlobalVariable.DataParamL1[10] +"', @6 = '1', @7 = 1, @8 = 1, @9 = '1', @10 = 1, @11 = 1, @12 = '"+ GlobalVariable.DataParamL1[11] +"', @13 = '"+ GlobalVariable.DataParamL1[12] +"', @14 = '"+ GlobalVariable.DataParamL1[13] +"', @15 = '"+ GlobalVariable.DataParamL1[14] +"', @16 = '"+ GlobalVariable.DataParamL1[15] +"', @17 = 1, @18 = ''")
		
	} 	
	
	ArrayList DataP = REA.getAllDataDatabase("172.16.94.70", "SEA", QueryPrimary)
	
	ArrayList DataO = REA.getAllDataDatabase("172.16.94.70", "SEA", QueryOptional)
	
	println (DataP)
	
	println (DataO)
	
	String keterangan = " (Jika dokumen ada, wajib dilampirkan)"
	
	SourceData.add("List dokumen yang wajib diupload :")
	
	for (bb = 0; bb < DataP.size(); bb++){
				
		SourceData.add((DataP[bb])[3])
		
	} 
	
	println (SourceData)
	
	for (dd = 0; dd < DataO.size(); dd++){
		
		for (cc = 0; cc < SourceData.size(); cc++){
			
			if ((DataO[dd])[3] == SourceData[cc]){
				
				DataO.remove(DataO[dd])
				
				break
				
			}

			
		}
		
		SourceData.add((DataO[dd])[3] + keterangan)
		
	}
	
	if (GlobalVariable.DataParamL1[8] == "0.0"){
		
		String DescMaterai = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetMaterai, "DocumentTypeName")
				
		for (ee = 0; ee < SourceData.size(); ee++){
			
			println (bill)
			
			if (SourceData[ee] == "Kuitansi Asli Tanpa Materai / Rp3.000"){
				
				println (DescMaterai)
				
				println (SourceData[ee])
				
				SourceData.set(ee , DescMaterai)
			}
			
		}	
		
	}
	
	println (SourceData)
	
	println (ScreenData)
	
//	WebUI.verifyMatch(QueryOptional, SourceData.size(), false)
	
	for (ii = 0; ii < SourceData.size(); ii++) {
		
		if (SourceData[ii] == ScreenData[ii]) {
			
				KeywordUtil.markPassed("Value " + ScreenData[ii] +" from screen same with source.")
				
		} else {
							
				KeywordUtil.markFailedAndStop("Value from screen = " + ScreenData[ii] + " has different Value from source = " + SourceData[ii])
				
		}
		
	}
	
	
}







