import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

ArrayList SourceDataCEID = new ArrayList()

ArrayList ScreenDataCEID = new ArrayList()

ArrayList SourceDataCEIH = new ArrayList()

ArrayList ScreenDataCEIH = new ArrayList()

//ArrayList SourceDataCEIH = new ArrayList()
//
//ArrayList ScreenDataCEIH = new ArrayList()
 
if (Skenario == "UploadDokumen") {
	
	for (x = 0; x < GlobalVariable.DataParamL3.size(); x++){
		
		ArrayList img = (GlobalVariable.DataParamL3[x]).toString().split("\\.")
		
		ArrayList Data = new ArrayList()
		
		Data.add(GlobalVariable.DataParamL3[x].toString()) 
		
		Data.add(img[1].toUpperCase())
		
		Data.add("/")
		
		Data.add("0")
		
		SourceDataCEID.add(Data)
		
	}
	
	ScreenDataCEID = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryDataInsertCEID.replace("GVCNO",GlobalVariable.DataParamL1[0]))
	
	println (SourceDataCEID)
	
	println (ScreenDataCEID)
	
	WebUI.verifyMatch(ScreenDataCEID.size().toString(), SourceDataCEID.size().toString(), false)
	
	for (i = 0 ; i < SourceDataCEID.size(); i++) {
		
		for (o = 0 ; o < SourceDataCEID[0].size() ; o++) {
		
			if ((SourceDataCEID[i])[o] == (ScreenDataCEID[i])[o]) {
			
				KeywordUtil.markPassed("Value " + (ScreenDataCEID[i])[o] +" from screen data same with Database.")
				
			} else {
							
				KeywordUtil.markFailedAndStop("Value from screen data = " + (ScreenDataCEID[i])[o] + " has different Value from database = " + (SourceDataCEID[i])[o])
				
			}
					
		}
		
	}
	
} else if (Skenario == "DeleteDokumen") {
	
	//ArrayList BasedData = new ArrayList()
		
	println(GlobalVariable.DataParamL3)
	
//	for (x = 0; x < GlobalVariable.DataParamL3.size(); x++){
//		
//		ArrayList img = (GlobalVariable.DataParamL3[x]).toString().split("\\.")
//		
//		Data.add(GlobalVariable.DataParamL3[x].toString())
//		
//		println (img[1])
//		
//		Data.add(img[1].toUpperCase())
//		
//		Data.add("/")
//		
//		if (x == 0){
//			
//			Data.add("1")
//			
//		} else {
//		
//			Data.add("0")
//		
//		}
//	
//		SourceDataCEID.add(Data)
//		
//	}
	
	println (GlobalVariable.DataParamL4)
	
	println (GlobalVariable.DataParamS3 + " - " + GlobalVariable.DataParamS2)
	
	if (GlobalVariable.DataParamL4.size() == 1){
		
		ArrayList Data = new ArrayList()
		
		Data.add(GlobalVariable.DataParamL4[0].toString())

		Data.add(GlobalVariable.DataParamS3)
		
		Data.add("/")
		
		Data.add("1")
		
		SourceDataCEID.add(Data)
		
	} else {
	
		ArrayList DataX = new ArrayList()
	
		ArrayList ss = GlobalVariable.DataParamS3.toString().split("\\.")
	
		DataX.add(GlobalVariable.DataParamS3.toString())
		
		DataX.add(ss[1].toUpperCase())
			
		DataX.add("/")
			
		DataX.add("1")
			
		SourceDataCEID.add(DataX)
	
		for (x = 0; x < GlobalVariable.DataParamL4.size(); x++){
			
			ArrayList Data = new ArrayList()
		
			ArrayList img = (GlobalVariable.DataParamL4[x]).toString().split("\\.")
		
			Data.add(GlobalVariable.DataParamL4[x].toString())
		
			Data.add(img[1].toUpperCase())
		
			Data.add("/")
		
			Data.add("0")
			
			SourceDataCEID.add(Data)
		
		}
	
	}
	
	ScreenDataCEID = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryDataInsertCEID.replace("GVCNO",GlobalVariable.DataParamL1[0]))
	
	println (SourceDataCEID)
	
	println (ScreenDataCEID)
	
	WebUI.verifyMatch(ScreenDataCEID.size().toString(), SourceDataCEID.size().toString(), false)
	
	for (i = 0 ; i < SourceDataCEID.size(); i++) {
		
		for (o = 0 ; o < SourceDataCEID[0].size() ; o++) {
		
			if ((SourceDataCEID[i])[o] == (ScreenDataCEID[i])[o]) {
			
				KeywordUtil.markPassed("Value " + (ScreenDataCEID[i])[o] +" from screen data same with Database.")
				
			} else {
							
				KeywordUtil.markFailedAndStop("Value from screen data = " + (ScreenDataCEID[i])[o] + " has different Value from database = " + (SourceDataCEID[i])[o])
				
			}
					
		}
		
	}

} else if (Skenario == "SubmitUpload") {

	SourceDataCEIH = [GlobalVariable.DataParamS1, GlobalVariable.DataParamL3.size().toString(),"0","0","0"]

	ScreenDataCEIH = REA.getOneRowDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryDataInsertCEIH.replace("GVCNO",GlobalVariable.DataParamL1[0]))
	
	WebUI.verifyMatch(ScreenDataCEIH.size().toString(), SourceDataCEIH.size().toString(), false)
	
	for (j = 0; j < SourceDataCEIH.size(); j++){
		
		if (SourceDataCEIH[j] == ScreenDataCEIH[j]) {
			
			KeywordUtil.markPassed("Value " + ScreenDataCEIH[j] +" from screen data same with Database.")
				
		} else {
							
			KeywordUtil.markFailedAndStop("Value from screen data = " + ScreenDataCEIH[j] + " has different Value from database = " + SourceDataCEIH[j])
				
		}
		
	}

} else if (Skenario == "PenurunanKeCore") {





}