import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

ArrayList SourceData = new ArrayList()

ArrayList ScreenData = new ArrayList()

GEN5.ProcessingCommand()

if (Skenario == "searching"){
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Clear'))
	
	String[] DataCNO = REA.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID), "CNO")
	
	int random = new Random().nextInt(DataCNO.length)
	
	String CNO = DataCNO[random].toString()
	
	ArrayList DataParam = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID + "' AND CH2.CNO = '"+ CNO +""))
	
	ArrayList DataParam1 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID + "' AND CH2.CNO = '"+ CNO +"").replace("CONVERT(VARCHAR, Start, 106)","").replace("CONVERT(VARCHAR, Finish, 106)",""))
	
	if (Param == "TGFrom"){
		
		ArrayList a = DataParam[4].split(" ")
				
		String Date = a[0] + "/" + a[1] + "/" + a[2]
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_From'))
		
		GEN5.DatePicker(Date, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_From')) 
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("'GVProviderID'","'"+ GlobalVariable.ProviderID + "' AND Start >= CAST('" + DataParam1[4] + "' AS DATE)"))
	
	} else if (Param == "TGTo"){
	
		ArrayList DataCNO3 = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID).replace("DESC","ASC"))
		
		ArrayList dataInput = new ArrayList()
		
		ArrayList DataParam4 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID + "' AND CH2.CNO = '"+ DataCNO3[10][0] +"").replace("CONVERT(VARCHAR, Start, 106)","").replace("CONVERT(VARCHAR, Finish, 106)",""))
		
		ArrayList a = DataCNO3[10][5].split(" ")
		
		String Date = a[0] + "/" + a[1] + "/" + a[2]
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_To'))
		
		GEN5.DatePicker(Date, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_To')) 
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		println (GlobalVariable.GA_QueryGetDataPending.replace("'GVProviderID'","'"+ GlobalVariable.ProviderID + "' AND Finish <= CAST('" + DataParam4[5] + "' AS DATE)"))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("'GVProviderID'","'"+ GlobalVariable.ProviderID + "' AND Finish <= CAST('" + DataParam4[5] + "' AS DATE)"))
		
	} else if (Param == "TGFromTo"){
		
		ArrayList DataCNO2 = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID).replace("DESC","ASC"))
		
		ArrayList dataInput = new ArrayList()
		
		ArrayList DataParam2 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID + "' AND CH2.CNO = '"+ DataCNO2[1][0] +"").replace("CONVERT(VARCHAR, Start, 106)","").replace("CONVERT(VARCHAR, Finish, 106)",""))
		
		ArrayList DataParam3 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearch.replace("GVProviderID",GlobalVariable.ProviderID + "' AND CH2.CNO = '"+ DataCNO2[20][0] +"").replace("CONVERT(VARCHAR, Start, 106)","").replace("CONVERT(VARCHAR, Finish, 106)",""))
		
		ArrayList a = DataCNO2[1][4].split(" ")
		
		ArrayList b = DataCNO2[20][5].split(" ")
		
		String Date = a[0] + "/" + a[1] + "/" + a[2]
		
		String Date1 = b[0] + "/" + b[1] + "/" + b[2]
		
		println (Date + " to : " + Date1)
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_From'))
		
		GEN5.DatePicker(Date, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_From'))
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_To'))
		
		GEN5.DatePicker(Date1, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_To'))
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		println (GlobalVariable.GA_QueryGetDataPending.replace("'GVProviderID'","'"+ GlobalVariable.ProviderID + "' AND Start >= CAST('" + DataParam2[4] + "' AS DATE) AND Finish <= CAST('" + DataParam3[5] + "' AS DATE)"))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("'GVProviderID'","'"+ GlobalVariable.ProviderID + "' AND Start >= CAST('" + DataParam2[4] + "' AS DATE) AND Finish <= CAST('" + DataParam3[5] + "' AS DATE)"))
	
	} else if (Param == "NamaPeserta"){
	
		WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), DataParam[2]) 
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID + "' AND PM.Name LIKE '"+ DataParam1[2] +"%"))
	
	} else if (Param == "AllDataIsi"){
	
		ArrayList a = DataParam[4].split(" ")
		
		String Date = a[0] + "/" + a[1] + "/" + a[2]
		
		ArrayList b = DataParam[5].split(" ")
		
		String Date1 = b[0] + "/" + b[1] + "/" + b[2]
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_From'))
		
		GEN5.DatePicker(Date, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_From'))
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_DatePicker_Tanggal_Perawatan_To'))
		
		GEN5.DatePicker(Date1, findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_DatePicker_Tanggal_Perawatan_To'))
		
		WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), DataParam[2]) 
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		println (GlobalVariable.GA_QueryGetDataPending.replace("'GVProviderID'","'"+ GlobalVariable.ProviderID + "' AND PM.Name LIKE '"+ DataParam1[2] +"%' AND Start >= CAST('" + DataParam1[4] + "' AS DATE) AND Finish <= CAST('" + DataParam1[5] + "' AS DATE)"))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("'GVProviderID'","'"+ GlobalVariable.ProviderID + "' AND PM.Name LIKE '"+ DataParam1[2] +"%' AND Start >= CAST('" + DataParam1[4] + "' AS DATE) AND Finish <= CAST('" + DataParam1[5] + "' AS DATE)"))
	
	} else if (Param == "AllDataBlank"){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID))
	
	} else if (Param == "GLNoSubTengah"){
	
		String GLNO = DataParam1[7].trim().substring(3,10)
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'), GLNO)
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID + "' AND GLNO LIKE '%"+ GLNO +"%"))
		
	} else if (Param == "GLNoSubAwal"){
	
		String GLNO = DataParam1[7].trim().substring(0,5)
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'), GLNO)
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID + "' AND GLNO LIKE '%"+ GLNO +"%"))
	
	} else if (Param == "GLNoSubAkhir"){
	
		String GLNO = DataParam1[7].trim().substring(DataParam1[7].trim().size()-5,DataParam1[7].trim().size())
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'), GLNO)
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID + "' AND GLNO LIKE '%"+ MemberNo +"%"))

	} else if (Param == "MemberNoSubAwal"){
	
		String MemberNo = DataParam1[6].trim().substring(0,DataParam1[6].trim().size()-1)
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'), MemberNo)
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID + "' AND C.MemberNo LIKE '%"+ MemberNo +"%"))
	
	} else if (Param == "MemberNoFull"){
	
		String MemberNo = DataParam1[6].trim()
	
		WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nomor_GL'), MemberNo)
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
		
		SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID + "' AND C.MemberNo LIKE '%"+ MemberNo +"%"))
	
	}
		
} else if (Skenario == "loadscreen"){

	SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataPending.replace("GVProviderID",GlobalVariable.ProviderID))
	
}

println (SourceData)

for (x = 0; x < SourceData.size(); x++){
	
	SourceData[x].remove((SourceData[x])[0])

	for (yy = 0; yy < SourceData[x].size(); yy++){

		println (SourceData[x][3] + " isi : " + SourceData[x][3].substring(SourceData[x][3].size()-2, SourceData[x][3].size()))
		
		if (SourceData[x][3].substring(SourceData[x][3].size()-2, SourceData[x][3].size()) == "- "){

			SourceData[x][3].substring(SourceData[x][3].size()-3)
			
			SourceData[x][3].replace(" - ", "")
	
		}
	}
	
	SourceData[x].removeRange(4, 15)

}

println (SourceData)

ScreenData = GEN5.getAllDataTableMultiPage(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/TBL_Grid'), findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Next_Page')) 

for (xx = 0; xx < ScreenData.size(); xx++){
	
	for (int xy = 0 ; xy < ScreenData[0].size() ; xy++) {
	
		String test = (ScreenData[xx])[xy].trim()
		
		ScreenData[xx].set(xy, test)
		
	}

}

println (ScreenData)

println (SourceData)

WebUI.verifyMatch(ScreenData.size().toString(), SourceData.size().toString(), false)

for (i = 0 ; i < SourceData.size() ; i++) {
	
	for (o = 0 ; o < SourceData[0].size() ; o++) {
		
		if ((SourceData[i])[o] == (ScreenData[i])[o]) {
		
			KeywordUtil.markPassed("Value " + (ScreenData[i])[o] +" from Grid Table same with Database.")
			
		} else {
						
			KeywordUtil.markFailedAndStop("Value from Grid Table = " + (ScreenData[i])[o] + " has different Value from database = " + (SourceData[i])[o])
			
		}
				
	}
	
}

