import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


if (Skenario == "CompareSpecificGMA"){
	
	ArrayList Source = new ArrayList()
	
	String penampungCNO = ''
	
	int i
	
	for (i = 0 ; i < GlobalVariable.DataParamL1.size() ; i++) {
	
		if (i == GlobalVariable.DataParamL1.size()-1) {
	
			penampungCNO += "'" + GlobalVariable.DataParamL1[i] + "'"
		} else {
	
			penampungCNO += "'" + GlobalVariable.DataParamL1[i] + "',"
		}
	}
	
	println (penampungCNO + " - " + GlobalVariable.GA_QueryGetBasedDataPenurunanCore.replace("StatusSendToCore = '0'","CNO IN ("+ penampungCNO +")"))
	
	ArrayList Screen = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryGetBasedDataPenurunanCore.replace("StatusSendToCore = '0'","CNO IN ("+ penampungCNO +")"))
	
	for (i = 0; i < GlobalVariable.DataParamL1.size(); i++){
		
		ArrayList data = new ArrayList()
		
		data.add(GlobalVariable.DataParamL1[i])
		
		data.add("1")
		
		data.add("1")
		
		Source.add(data)
		
	}
	
	println (Source)
	
	println (Screen)
	
	WebUI.verifyMatch(Screen, Source, false)
	
	for (x = 0; x < Source.size(); x++){
		
		WebUI.verifyMatch(Screen[x], Source[x], false)
		
	}
	
}



