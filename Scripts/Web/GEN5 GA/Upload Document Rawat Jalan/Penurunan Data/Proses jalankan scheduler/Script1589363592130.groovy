import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.UI
import com.keyword.GEN5
import com.keyword.REA

if (Skenario == "CekValidGMA"){
	
	ArrayList ListCNOGMA = REA.getOneColumnDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryGetBasedDataPenurunanCore,"CNO")
	
	ArrayList ListCNOSEA = REA.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataCoreGMA,"CNO")
	
	println (ListCNOGMA + " - " + ListCNOSEA)
	
	ArrayList SourceGMA = new ArrayList()
	
	ArrayList SourceSEA = new ArrayList()
	
	for (i = 0; i < ListCNOGMA.size(); i++){
		
		ArrayList data = new ArrayList()
		
		data.add(ListCNOGMA[i])
		
		data.add("1")
		
		data.add("1")
		
		SourceGMA.add(data)
		
	}
	
	for (a = 0; a < ListCNOGMA.size(); a++){
		
		for (ab = 0; ab < SourceSEA.size(); ab++){
			
			if (SourceSEA[ab] != ListCNOGMA[a]){
				
				SourceSEA.add(ListCNOGMA[a])
			}
			
		}	
	
	}
	
	String CNOGMA = ''
	
	int i
	
	for (i = 0 ; i < ListCNOGMA.size() ; i++) {
	
		if (i == ListCNOGMA.size()-1) {
	
			CNOGMA += "'" + ListCNOGMA[i] + "'"
		} else {
	
			CNOGMA += "'" + ListCNOGMA[i] + "',"
		}
	}
	
	println (SourceGMA)
	
	println (SourceSEA)
	
	println (CNOGMA + " - " + GlobalVariable.GA_QueryGetBasedDataPenurunanCore.replace("StatusSendToCore = '0'","CNO IN ("+ CNOGMA +")"))
		
	String path = '\\\\172.16.94.33\\c$\\Installer\\SchedulerUploadDocs\\a2is.HealthProviderUploadDocument.SchedulerUploadDocs.exe'
	
	UI.RunScheduler(path)	
	
	ArrayList Screen = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryGetBasedDataPenurunanCore.replace("StatusSendToCore = '0'","CNO IN ("+ CNOGMA +")"))
	
	ArrayList SchedulerSEA = REA.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataCoreGMA,"CNO")
	
	println (SourceGMA)
	
	println (Screen)
	
	WebUI.verifyMatch(Screen.size(), SourceGMA.size(), false)
	
	for (x = 0; x < SourceGMA.size(); x++){
		
		WebUI.verifyMatch(Screen[x], SourceGMA[x], false)
		
	}
	
	WebUI.verifyMatch(SchedulerSEA.size(), SourceSEA.size(), false)
	
	for (y = 0; y < SourceSEA.size(); y++){
		
		WebUI.verifyMatch(SchedulerSEA[y], SourceSEA[y], false)
		
	}
	
} else if (Skenario == "CheckSpesificData"){

	String CNOGMA = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryGetBasedDataPenurunanCore.replace("CNO","TOP 1 CNO"),"CNO")
	
	println (CNOGMA)
	
	ArrayList SourceGMA = [CNOGMA, "1", "1"]
	
	ArrayList SourceSEA = new ArrayList()
	
	String DocumentNo = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDocumentNo , "DocumentNo") 
	
	String RegID = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.QueryGetCNO.replace("ClaimH","PostClaimBatch"), "SeqNo")
	
	String InvoiceNo = REA.getValueDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryDataInsertIncomplete.replace("GVCNO", CNOGMA).replace("Select","Select TOP 1 "), "InvoiceNo")
	
	String Step = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetStep.replace("GVDocumentNo",DocumentNo), "Step")
	
	ArrayList Screen = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryGetBasedDataPenurunanCore.replace("StatusSendToCore = '0'","CNO IN ("+ CNOGMA +")"))
	
	ArrayList data1 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamBasedData.replace("GVCNO", CNOGMA))
	
	println (data1 + " / RegID : " + RegID + " / InvoiceNo: " + InvoiceNo + " / DocumentNo : " + DocumentNo)
	
	REA.updateValueDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSPDocRegClaim.replace("2515984",CNOGMA))
		
	ArrayList data2 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSPDocRegClaim1)
	
	println (data2)
	
	REA.updateValueDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSPDocRegClaim2)
	
	//ArrayList data2 = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSPDocRegClaim.replace("@1=2830762", "@1="+ CNOGMA ))
	
	//ArrayList SourceTDRTDRD = [data2[14],'NULL','Created with Cisadane','','GMAKSES','GMAKSES',InvoiceNo,data2[36],data2[12],data2[18],'','', (RegID + 1).toString(),'null',data2[9],data1[1],data2[33],DocumentNo,'','GMAKSES','null','null',data2[10],'O','REG',data2[36],'Created with Cisadane','R',GLNo,data1[6].toString(),data2[12],data2[18],data2[6],data2[7],data2[9],'GMAKSES',data2[15],'GMAKSES','GMAKSES','Created with Cisadane','','','0','0',data1[1],'REGISTERED','GMAKSES','0','Created with Cisadane','null',data2[39],'null','null','null',data2[15],data2[16],data2[3],data2[20],data2[33],data2[21],data2[22],data2[23],data2[24],data2[25],data2[26],data2[14],data2[38],data2[30],data2[31],data2[32],data2[29],data2[28],data2[27],data2[40],data2[42],data2[41],data2[43],'','NULL','0','NULL']
	
	if (data2[37] == 'null'){
		
		data2[37] = ''
		
	} else if ( data2[41] == 'null'){
	
		data2[41] = ''
		
	} else if ( data2[34] == 'null'){
	
		data2[34] = ''
		
	} else if ( data2[35] == 'null'){
	
		data2[35] = ''
		
	} else if ( data2[36] == 'null'){
	
		data2[36] = ''
		
	} else if ( data2[33] == 'null'){
	
		data2[33] = ''
		
	} else if ( data2[32] == 'null'){
	
		data2[32] = ''
		
	} 
   
	ArrayList SourceTDRTDRD = [data2[22], 'P', 'Created with Cisadane', 'NULL', 'GMAKSES', 'GMAKSES', InvoiceNo, data2[7], data2[5], data2[3], '', '',  (RegID.toInteger() + 1).toString(), 'null', data2[16], data2[8], data2[37], DocumentNo, '', 'GMAKSES', 'null', 'null', data2[0], 'O', 'REG', data2[7], 'Created with Cisadane', 'G', data2[2],  (RegID + 1).toString(), data2[5], data2[3], data2[9], data2[10], data2[16], 'GMAKSES', data2[23], 'GMAKSES', 'GMAKSES', 'Created with Cisadane', '', '', '0', '0', data2[8], 'REGISTERED', 'GMAKSES', '0', 'Created with Cisadane', 'null', data2[42], 'null', 'null', 'null', data2[23], data2[11], data2[13], data2[15], data2[37], data2[24], data2[25], data2[26], data2[27], data2[28], data2[29], data2[22], data2[41], data2[34], data2[35], data2[36], data2[33], data2[32], data2[31], data2[30], data2[18], data2[17], data2[19], data2[43], data2[44], data2[45], data2[53]]
	
	println (SourceTDRTDRD)
	
	ArrayList SourceTDRDH = [DocumentNo, Step, 'REG', 'GMAKSES', 'GMAKSES', 'Created with Cisadane', '0', '0', 'null', 'REGISTERED', 'GMAKSES', '0']
	
	println (SourceTDRDH)
	
	ArrayList SourceCH = [(RegID + 1).toString(), data2[24], data2[25], data2[26], data2[27], data2[28], data2[29], data2[8], data2[30], data2[13], '1', '0', data2[15], data2[9], data2[10], data2[16], data2[3], data1[7], data1[8], data2[34], data2[35], data2[36], data2[33], data2[32], data2[31], data2[41]]
	
	//ArrayList SourceCH = [data1[6].toString(), data2[21], data2[22], data2[23], data2[24], data2[25], data2[26], data1[1], data2[40], data2[3], '1', '0', data2[20], data2[6], data2[7], data2[9], data2[18], data1[7], data1[8], data2[30], data2[31], data2[32], data2[29], data2[28], data2[27], data2[38]]
	
	println (SourceCH)
	
	ArrayList SourceCAI = new ArrayList()
	
	ArrayList SourceCDO = new ArrayList() // ClaimDocuments
	
	ArrayList SourceCDF = new ArrayList() // ClaimDocumentsFile
	
	ArrayList SourceF = new ArrayList() // a2isFileDB.dbo.Files
	
	ArrayList AdditionlInfo = REA.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetCAI.replace("GVDocumentNo",DocumentNo), "AdditionalInfoID")
	
	ArrayList data4 = [DocumentNo,'0', '1', 'GMAKSES', 'GMAKSES']
	
	for (xx = 0; xx < AdditionlInfo.size(); xx++){
		
		data4.set(1, AdditionlInfo[xx])
		
		SourceCAI.add(data4)
	}
	
	ArrayList SourceCDI = [[CNOGMA, data2[11], '', '0']]
	
	println (GlobalVariable.GA_QueryGetClaimDocuments.replace("@0='',@1='CTSCN|CMRI|CRONT',@2='C',@3=5000000,@4='OP',@5=NULL,@6=NULL,@7=NULL,@8=NULL,@9=NULL,@10=NULL,@11=NULL,@12='CJKPP0009',@13='OJKRI00001',@14='A91',@15=0,@16=6932183,@17=0", "@0='',@1='',@2='"+ data2[22] +"',@3="+ data2[16] +",@4='"+ data2[13] +"',@5='"+ data2[15] +"',@6='',@7='',@8='',@9='',@10='',@11='',@12='"+data2[5]+"',@13='"+data2[7]+"',@14='"+data2[11]+"',@15=2,@16="+data2[3]+",@17=0"))
	
	String FileName = "File-"+ CNOGMA +"-"+ InvoiceNo +".PDF"
	
	String ShareFolder = REA.getValueDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetShareFolder, "PermanentShareFolderName")
	
	ArrayList ListDocument = REA.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetClaimDocuments.replace("@0='',@1='CTSCN|CMRI|CRONT',@2='C',@3=5000000,@4='OP',@5=NULL,@6=NULL,@7=NULL,@8=NULL,@9=NULL,@10=NULL,@11=NULL,@12='CJKPP0009',@13='OJKRI00001',@14='A91',@15=0,@16=6932183,@17=0", "@0='',@1='',@2='"+ data2[22] +"',@3="+ data2[16] +",@4='"+ data2[13] +"',@5='"+ data2[15] +"',@6='',@7='',@8='',@9='',@10='',@11='',@12='"+data2[5]+"',@13='"+data2[7]+"',@14='"+data2[11]+"',@15=2,@16="+data2[3]+",@17=0"), "DocumentTypeID")
	
	for (ld = 0; ld < ListDocument.size(); ld++){

		ArrayList baseddataCDO = ['', ListDocument[ld], '1', '1', ShareFolder, FileName, '1', 'GMAKSES', '0', '0', '1', 'GMAKSES', 'GMAKSES', '', '']
		
		ArrayList baseddataCDF = [ListDocument[ld], DocumentNo, FileName, '1' , 'GMAKSES' ]
		
		ArrayList baseddataF = ['13', '1', ShareFolder, '1' , 'GMAKSES' ]
		
		SourceCDO.add(baseddataCDO)
		
		SourceCDF.add(baseddataCDF)
		
		SourceF.add(baseddataF)
		
	}
	
	
	println (SourceCDO)
	
	println (SourceCDF)
	
	println (SourceF)
	
	String path = '\\\\172.16.94.33\\c$\\Installer\\SchedulerUploadDocs\\a2is.HealthProviderUploadDocument.SchedulerUploadDocs.exe'
	
	UI.RunScheduler(path)
	
	//println (GlobalVariable.GA_QueryDataInsertTDRTDRD.replace("GVClaimNo",data2[0]))
	
	ArrayList SchedulerTDRTDRD = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataInsertTDRTDRD.replace("GVClaimNo",data2[0])) 
		
	ArrayList SchedulerTDRDH = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataInsertTDRDH.replace("GVClaimNo",data2[0]))
	
	ArrayList SchedulerCAI = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataInsertCAI.replace("GVClaimNo",data2[0]))
	
	ArrayList SchedulerCH = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataUpdateCH.replace("GVCNO",CNOGMA))
	
	ArrayList SchedulerCDI = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataInsertCD.replace("GVCNO",CNOGMA))
	
	ArrayList SchedulerCDO = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataInsertCDO.replace("GVDocumentNo", DocumentNo))
	
	ArrayList SchedulerCDF = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataInsertCDF.replace("GVDocumentNo", DocumentNo))
	
	ArrayList SchedulerF = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryDataInsertF.replace("GVDocumentNo", DocumentNo))
	
	println (SourceTDRTDRD)
	
	println (SchedulerTDRTDRD)
	
	println (SourceTDRDH)
	
	println (SchedulerTDRDH)
	
	println (SourceCH)
	
	println (SchedulerCH)
	
	println (SourceCAI)
	
	println (SchedulerCAI)
	
	println (SourceCDI)
	
	println (SchedulerCDI)
	
	println (SourceCDO)
	
	println (SchedulerCDO)
	
	println (SourceCDF)
	
	println (SchedulerCDF)
	
	println (SourceF)
	
	println (SchedulerF)
	
	println (SourceGMA)
	
	println (Screen)
	
	WebUI.verifyMatch(SchedulerTDRTDRD.size().toString(), SourceTDRTDRD.size().toString(), false)
	
	for (y = 0; y < SourceTDRTDRD.size(); y++){
		
		WebUI.verifyMatch(SchedulerTDRTDRD[y], SourceTDRTDRD[y], false)
		
	}
	
	WebUI.verifyMatch(SchedulerTDRDH.size().toString(), SourceTDRDH.size().toString(), false)
	
	for (bb = 0; bb < SourceTDRDH.size(); bb++){
		
		WebUI.verifyMatch(SchedulerTDRDH[bb], SourceTDRDH[bb], false)
		
	}
	
	WebUI.verifyMatch(SchedulerCH.size().toString(), SourceCH.size().toString(), false)
	
	for (cc = 0; cc < SourceCH.size(); cc++){
		
		WebUI.verifyMatch(SchedulerCH[cc], SourceCH[cc], false)
		
	}
	
	WebUI.verifyMatch(SchedulerCAI.size().toString(), SourceCAI.size().toString(), false)
	
	for (z = 0; z < SourceCAI.size(); z++){
		
		for (aa = 0; aa < SourceCAI[0].size(); aa++){
			
			WebUI.verifyMatch(SchedulerCAI[aa], SourceCAI[aa], false)
		}		
		
	}

	WebUI.verifyMatch(SchedulerCDI.size().toString(), SourceCDI.size().toString(), false)
	
	for (dd = 0; dd < SourceCDI.size(); dd++){
		
		for (ee = 0; ee < SourceCDI[0].size(); ee++){
			
			WebUI.verifyMatch(SchedulerCDI[ee], SourceCDI[ee], false)
		}	
		
	}
	
	WebUI.verifyMatch(Screen.size().toString(), SourceGMA.size().toString(), false)
	
	for (x = 0; x < SourceGMA.size(); x++){
	
		WebUI.verifyMatch(Screen[x], SourceGMA[x], false)
	
	}
}

