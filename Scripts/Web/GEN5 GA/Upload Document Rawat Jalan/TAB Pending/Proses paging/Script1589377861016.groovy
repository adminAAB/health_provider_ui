import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.GEN5
import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

	ArrayList SourceData = new ArrayList()
	
	ArrayList ScreenData = new ArrayList()
	
	GEN5.ProcessingCommand()
	
	WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/LBL_Angka_Page'), GlobalVariable.maxDelay)

	int JumlahPage
	
	int PageNow
	
	String Angka = WebUI.getText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/LBL_Angka_Page'))
	
	println (Angka)
	
	ArrayList angka0 = Angka.split(" of ")
	
	JumlahPage = angka0[1].toInteger()
	
	PageNow = angka0[0].toInteger()
		
	if (JumlahPage > 1){
		
		if (Skenario =="next"){
			
			WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Next_Page'))
			
			GEN5.ProcessingCommand()
			
			int PageNowFix = ((PageNow + 1) * 10) + 1
			
			println (GlobalVariable.GA_QueryGetDataIncomplete.replace("GVProviderID",GlobalVariable.ProviderID).replace("AA","AA WHERE AA.ROW > " + PageNow * 10 + " AND AA.ROW < " + PageNowFix +""))
			
			SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataIncomplete.replace("GVProviderID",GlobalVariable.ProviderID).replace("AA","AA WHERE AA.ROW > " + PageNow * 10 + " AND AA.ROW < " + PageNowFix +""))
											
			println (SourceData)
			
		} else if (Skenario =="last"){
		
			if (PageNow < JumlahPage || PageNow == JumlahPage){
				
				WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Last_Page'))
				
				GEN5.ProcessingCommand()
				
				int PageNowFix = (JumlahPage * 10) + 1
				
				SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataIncomplete.replace("GVProviderID",GlobalVariable.ProviderID).replace("AA","AA WHERE AA.ROW > " + (JumlahPage - 1) * 10 + " AND AA.ROW < " + PageNowFix +""))
				
			}
			
		} else if (Skenario =="first"){
		
			WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_First_Page'))
			
			GEN5.ProcessingCommand()
			
			SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataIncomplete.replace("GVProviderID",GlobalVariable.ProviderID).replace("AA","AA WHERE AA.ROW < 11"))
		
		} else if (Skenario =="prev"){
		
			WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Prev_Page'))
			
			GEN5.ProcessingCommand()
			
			int PageNowFix = ((PageNow - 1)  * 10 ) + 1
			
			SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataIncomplete.replace("GVProviderID",GlobalVariable.ProviderID).replace("AA","AA WHERE AA.ROW > " + (PageNow - 2) * 10 + " AND AA.ROW < " + PageNowFix +""))
		
		}

	
		
		for (x = 0; x < SourceData.size(); x++){
			
			SourceData[x].remove((SourceData[x])[0])
		
			for (yy = 0; yy < SourceData[x].size(); yy++){
				
				//dipakai untuk akomodir kalau ada tgl finish yg kosong
				
				if (SourceData[x][3].substring(SourceData[x][3].size()-2, SourceData[x][3].size()) == "- "){
		
					SourceData[x][3].substring(SourceData[x][3].size()-3)
					
					SourceData[x][3].replace(" - ", "")
			
				}
			}
			
			SourceData[x].removeRange(4, 16)
		
		}
				
		ScreenData = GEN5.getAllDataTable(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/TBL_Grid_Pending'))
		
		for (xx = 0; xx < ScreenData.size(); xx++){
			
			for (int xy = 0 ; xy < ScreenData[0].size() ; xy++) {
			
				String test = (ScreenData[xx])[xy].trim()
				
				ScreenData[xx].set(xy, test)
				
			}
		
		}
		
		println (ScreenData)
		
		println (SourceData)
		
		WebUI.verifyMatch(ScreenData.size().toString(), SourceData.size().toString(), false)
		
		for (i = 0 ; i < SourceData.size() ; i++) {
			
			for (o = 0 ; o < SourceData[0].size() ; o++) {
				
				if ((SourceData[i])[o] == (ScreenData[i])[o]) {
				
					KeywordUtil.markPassed("Value " + (ScreenData[i])[o] +" from Grid Table same with Database.")
					
				} else {
								
					KeywordUtil.markFailedAndStop("Value from Grid Table = " + (ScreenData[i])[o] + " has different Value from database = " + (SourceData[i])[o])
					
				}
						
			}
			
		}
		
	}