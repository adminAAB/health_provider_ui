import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.UI
import com.keyword.GEN5
import com.keyword.REA
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

GEN5.ProcessingCommand()

if (Skenario == "CekScreenUpload"){
			
	WebUI.switchToFrame(findTestObject('Object Repository/Web/GEN5 GA/Frame'), GlobalVariable.maxDelay)	
	
	ArrayList ScreenHeaderTable = CustomKeywords.'code.General.GetColumnNameTable'(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/TBL_Grid_Pending'))
	
	WebUI.switchToDefaultContent()
	
	ArrayList SourceHeaderTable = ['No','Nama Peserta', 'Perusahaan','Tanggal Perawatan', 'Diagnosa']
	
	println ("ScreenHeaderTable: " +ScreenHeaderTable)
	
	println ("SourceHeaderTable: "+ SourceHeaderTable)
	
	WebUI.verifyMatch(ScreenHeaderTable.size().toString(), SourceHeaderTable.size().toString(), false)
	
	for (i = 1; i < SourceHeaderTable.size(); i++){
		
		if (ScreenHeaderTable[i] == SourceHeaderTable[i]) {
			
			KeywordUtil.markPassed("Value " + ScreenHeaderTable[i] +" from Grid Table same with Database.")
			
		} else {
		
			KeywordUtil.markFailedAndStop("Value from Grid Table = " + ScreenHeaderTable[i] + " has different Value from database = " + SourceHeaderTable[i])
		
		}
	}
	
} else if (Skenario == "KlikTabPending"){

	WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TAB_Pending'))

} else if (Skenario == "SearchNotFound"){

	WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), "axxcd")
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
	
	GEN5.ProcessingCommand()

	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Data tidak ditemukan.")
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))

} else if (Skenario == "KlikOKNotif"){

	boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Notifikasi'), 2,FailureHandling.OPTIONAL)
	
	if (processing){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	}
} else if (Skenario == "SearchData"){

	ArrayList DataParam = REA.getOneRowDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetParamSearchIncomplete.replace("GVProviderID",GlobalVariable.ProviderID))

	WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/TXT_Nama_Peserta'), DataParam[2])
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Search'))
	
	SourceData = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDataIncomplete.replace("GVProviderID",GlobalVariable.ProviderID + "' AND PM.Name LIKE '%"+ DataParam[2] +"%"))
	
	GlobalVariable.DataParamL1 = SourceData[0]
	
	println (GlobalVariable.DataParamL1)
	
} else if (Skenario == "KlikUpload"){ 
		
	GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/GEN5 GA/Upload Document Rawat Jalan/TAB Pending/TBL_Grid_Pending'), "No", GlobalVariable.DataParamL1[1])
	
	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BTN_Upload'))

	GEN5.ProcessingCommand()
	
	GlobalVariable.DataParamL2 = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryDataInsertIncomplete.replace("GVCNO",GlobalVariable.DataParamL1[0] + "' AND CEID.Status = '0"))
	
	GlobalVariable.DataParamL3 = REA.getAllDataDatabase("172.16.94.70", "GMA", GlobalVariable.GA_QueryDataInsertIncomplete.replace("GVCNO",GlobalVariable.DataParamL1[0]))
	
	println (GlobalVariable.DataParamL2)
	
	println (GlobalVariable.DataParamL3)
	
} else if (Skenario == "CekScreenPopUpUpload"){

	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Header_Screen'), "Upload Document")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nomor_Invoice'), "Nomor Invoice")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'), "Submit")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'), "Upload Document")
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Delete_Document'), "Delete Document")
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_List_Dokumen'))
	
	String SourceNoInvoice = GlobalVariable.DataParamL2[0][0]
	
	String ScreenNoInvoice = WebUI.getAttribute(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'), "value")
	
	WebUI.verifyMatch(ScreenNoInvoice, SourceNoInvoice, false)
	
	for (i = 0; i < GlobalVariable.DataParamL2.size(); i++){
		
		ArrayList img = (GlobalVariable.DataParamL2[i])[6].toString().split("\\.")
		
		println (img)
						
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-index', [('index') : (i+1).toString()]),
			img[0].toString())
		
	}
	
} else if (Skenario == "ValidasiPopUpUpload"){

	if (Param == "AllBlank"){
		
		WebUI.clearText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'))
	
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))
		
		WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), 2)
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Harap mengisi nomor invoice terlebih dahulu.")
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	} else if (Param == "ImageBlank"){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))

		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Harap upload minimal 1 foto dokumen.")
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	} else if (Param == "NomorInvoiceBlank"){
	
		WebUI.clearText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'))
		
		if (GlobalVariable.DataParamL2.size() <= 0){
			
			WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Upload_Document'))
			
			UI.UploadFile2("jpeg_74-2.jpeg")
			
			GEN5.ProcessingCommand()
			
		} 
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Harap mengisi nomor invoice terlebih dahulu.")
	
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	} else if (Param == "MaxFieldNomorInvoice"){
	
		String input = "EUM/01/24/2020/00012001" 
	
		WebUI.setText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'), input)
		
		String ScreenNoInvoice = WebUI.getAttribute(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_Nomor_Invoice'), "value") 
	
		String SourceNoInvoice = input.substring(0, 20)
		
		if (ScreenNoInvoice == SourceNoInvoice) {
			
				KeywordUtil.markPassed("Value " + ScreenNoInvoice +" from screen same with source.")
				
		} else {
							
				KeywordUtil.markFailedAndStop("Value from Grid Table = " + ScreenNoInvoice + " has different Value from source = " + SourceNoInvoice)
				
		}

	} else if (Param == "Upload_InvalidDataType"){
	
		WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), GlobalVariable.maxDelay)

		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "File yang boleh diupload: JPG, JPEG, PNG, TIFF, PDF.") 
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
	
	} else if (Param == "Upload_InvalidMaxSize"){
	
		GEN5.ProcessingCommand()
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Maksimum ukuran file yang boleh diupload 15 MB.") 
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'))
		
	}	
	
} else if (Skenario == "KlikX"){

	boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Upload_Document'), 2,FailureHandling.OPTIONAL)
	
	if (processing){
		
		WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_X_PopUp_Upload'))		
		
	} 	
	
} else if (Skenario == "UploadImage"){
		
	println (GlobalVariable.DataParamL4)
	
	if (GlobalVariable.DataParamL4 == "Valid.png"){
		
		GlobalVariable.DataParamL4 = []
		
		for (x = 0; x < GlobalVariable.DataParamL2.size(); x++){
			
			GlobalVariable.DataParamL4.add(GlobalVariable.DataParamL2[x][6])
			
		}
		
		GlobalVariable.DataParamL4.add("Valid.png")
			
		for (i = 0; i < GlobalVariable.DataParamL4.size(); i++){
			
			ArrayList img = GlobalVariable.DataParamL4[i].toString().split("\\.")
			
			println (img)
							
			WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-index', [('index') : (i+1).toString()]),
				img[0].toString())
			
		}
		
	} else {

		ArrayList DocActive = new ArrayList()
		
		ArrayList DocInactive = new ArrayList()
		
		println (GlobalVariable.DataParamL3) // image yg dihapus 
		
		for (k = 0; k < GlobalVariable.DataParamL3.size(); k++){
			
			ArrayList img = GlobalVariable.DataParamL3[k][6].toString().split("\\.")
			
			DocInactive.add(img[0])
			
		}
		
		for (l = 0; l < GlobalVariable.DataParamL3.size(); l++){
			
			ArrayList img = GlobalVariable.DataParamL3[l][6].toString().split("\\.")
			
			DocInactive.add(img[0])
			
		}
		
		println (GlobalVariable.DataParamL2) // document yg aktif
		
		for (j = 0; j < GlobalVariable.DataParamL2.size(); j++){
			
			DocActive.add(GlobalVariable.DataParamL2[j][6])
			
		}
		
		for (h = 2; h < GlobalVariable.DataParamL4.size(); h++){
			
			DocActive.add(GlobalVariable.DataParamL4[j][6])
			
		}
	
		for (i = 0; i < DocActive.size(); i++){
			
			ArrayList img = DocActive[i].toString().split("\\.")
			
			println (img)
						
			WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-index', [('index') : GlobalVariable.DataParamL2.size()+i+1]),
				img[0].toString())
			
		}
	
	}

} else if (Skenario == "DeleteImage"){

	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Apakah anda yakin ingin menghapus dokumen ini?")
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Delete'))
	
} else if (Skenario == "YesDeleteImage"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Delete_Document'))

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Delete'))
	
	WebUI.delay(2)
	
	GEN5.ProcessingCommand()
	
	println (GlobalVariable.DataParamL4)
	
	if (GlobalVariable.DataParamL4.size() == 1){
		
		println(GlobalVariable.DataParamS2)
		
		boolean processing = WebUI.waitForElementNotPresent(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): GlobalVariable.DataParamS2]), 2,FailureHandling.OPTIONAL)
		
		if (processing){
			
			KeywordUtil.markPassed("Document is deleted")
			
		} else {
		
			KeywordUtil.markFailedAndStop("Failed..document not deleted")
		
		}
		
	} else {
	
		boolean processing = WebUI.waitForElementNotPresent(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): GlobalVariable.DataParamS2]), 2, FailureHandling.OPTIONAL)
		
		if (processing){			
			KeywordUtil.markPassed("Document is deleted")			
		} else {		
			KeywordUtil.markFailedAndStop("Failed..document not deleted")
		}
	
		for (i = 0; i < GlobalVariable.DataParamL4.size(); i++){
		
			ArrayList image = (GlobalVariable.DataParamL4[i]).toString().split("\\.")
			
			boolean muncul = WebUI.waitForElementPresent(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): image[0]]), 2)
			
			if (muncul){
				KeywordUtil.markPassed("Document dispayed")
			} else {
				KeywordUtil.markFailedAndStop("Failed..document not dispayed")
			}
			
		}
	
	}

} else if (Skenario == "NoDeleteImage"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
	
	WebUI.delay(2)

	for (i = 0; i < GlobalVariable.DataParamL3.size(); i++){
	
		ArrayList img = (GlobalVariable.DataParamL3[i]).toString().split("\\.")
		
		WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_Nama_Document-namadoc', [('namadoc'): img[0]]))
	}

} else if (Skenario == "SubmitImage"){
	
	WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Apakah anda yakin ingin mengirimkan dokumen claim ini?")

	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
	
	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Notifikasi'))	

} else if (Skenario == "YesPopUpSubmitTanpaVerify"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Notifikasi'))
	
} else if (Skenario == "YesPopUpSubmit"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Submit'))

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_Yes_PopUp_Notifikasi'))	
	
	WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/BOX_PopUp_Notifikasi'), 2, FailureHandling.OPTIONAL)
	
	boolean processing = WebUI.waitForElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), 2, FailureHandling.OPTIONAL)
	
	if (processing) {
		
		WebUI.verifyElementText(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/LBL_PopUp_Notifikasi'), "Dokumen berhasil di-Submit.")
		
	}

	WebUI.verifyElementVisible(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_OK_PopUp_Notifikasi'), FailureHandling.OPTIONAL)
	
} else if (Skenario == "NoPopUpSubmit"){

	WebUI.click(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/BTN_No_PopUp_Notifikasi'))
		
} else if (Skenario == "ListDocument"){

	String ScreenListDokumen = WebUI.getAttribute(findTestObject('Web/GEN5 GA/Upload Document Rawat Jalan/PopUp Upload/TXT_List_Dokumen'), "value")
	
	println (ScreenListDokumen)
	
	ArrayList ScreenData = new ArrayList()
	
	ArrayList xx = ScreenListDokumen.split("- ")
	
	for (ee = 0; ee < xx.size(); ee++){
		
		ScreenData.add(xx[ee].trim())
		
	}

	println (GlobalVariable.DataParamL1)
	
	ArrayList SourceData = new ArrayList()	
		
	println (GlobalVariable.GA_QueryGetListDocIncompleted.replace("GVCNO",GlobalVariable.DataParamL1[0]))
	
	ArrayList DataP = REA.getAllDataDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetListDocIncompleted.replace("GVCNO",GlobalVariable.DataParamL1[0]))
	
	println (DataP)
	
	SourceData.add("List dokumen yang kurang :")
	
	for (bb = 0; bb < DataP.size(); bb++){
		
		//ArrayList DataDoc = new ArrayList()
		
		SourceData.add((DataP[bb])[0] + " : " + (DataP[bb])[1])
		
	}
	
	println (SourceData)
	
	println (ScreenData)
		
	for (ii = 0; ii < SourceData.size(); ii++) {
		
		if (SourceData[ii] == ScreenData[ii]) {
			
				KeywordUtil.markPassed("Value " + ScreenData[ii] +" from screen same with source.")
				
		} else {
							
				KeywordUtil.markFailedAndStop("Value from screen = " + ScreenData[ii] + " has different Value from source = " + SourceData[ii])
				
		}
		
	}
	
	
}







