import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.GEN5
import com.keyword.UI


WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Homepage/BTN_Create_EGL'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Button_Plus_diGrid'), GlobalVariable.maxDelay)

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Button_Plus_diGrid'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Process'), GlobalVariable.maxDelay)

String[] GetMember = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetAutoCompleteData, "MemberNo")

int random = new Random().nextInt(GetMember.length)

String Member = GetMember[random].toString()

WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TXT_Member'), Member)

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/LST_Member'))

WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TXT_Patient_PhoneNo'), '0210012890')

WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TXT_Provider'), ProviderID)

WebUI.delay(1)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/LST_Provider'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Add_Diagnosis'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Submit_Diagnosa'), GlobalVariable.maxDelay)

//int rowDiagnosis = (Math.random() * 10000) as int

String[] GetDiagnosis = UI.getOneColumnDatabase("172.16.94.70", "SEA", GlobalVariable.GA_QueryGetDiagnosis, "Diagnosis")

int randomD = new Random().nextInt(GetDiagnosis.length)

String Diagnosis = GetDiagnosis[randomD].toString()

WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TXT_ID_Diagnosa'), Diagnosis)

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/LST_ID_Diagnosis'))

GEN5.ProcessingCommand()

WebUI.setText(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TXT_Remark_Diagnosa'), 'testing sprint 9')

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Submit_Diagnosa'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Search_Doctor'))

String[] GetDoctor = GEN5.getAllColumnValue(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TBL_Doctor'), "Doctor Name")

int randomDO = new Random().nextInt(GetDoctor.length)

String Doctor = GetDoctor[randomDO].toString()

GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TBL_Doctor'), "Doctor Name", Doctor)

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Choose_Doctor'))

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Submit_Doctor'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Search_Appropriate_RB_Class'))

GEN5.ProcessingCommand()

String[] GetAppRB = GEN5.getAllColumnValue(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TBL_Appropriate_RB_Class'), "Room Type")

int randomARB = new Random().nextInt(GetAppRB.length)

String AppRB = GetAppRB[randomARB].toString()

GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TBL_Appropriate_RB_Class'), "Room Type", AppRB)

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Choose_Appropriate_RB_Class'))

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Search_Treatment_RB_Class'))

GEN5.ProcessingCommand()

String[] GetTreatRB = GEN5.getAllColumnValue(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TBL_Treatment_RB_Class'), "Room Type")

int randomTRB = new Random().nextInt(GetTreatRB.length)

String TreatRB = GetTreatRB[randomTRB].toString()

GEN5.ClickExpectedRow(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/TBL_Treatment_RB_Class'), "Room Type", TreatRB)

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Choose_Treatment_RB_Class'))

GEN5.ProcessingCommand()

WebUI.delay(1)

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Process'))

WebUI.waitForElementClickable(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Close_PopUP_GL'), GlobalVariable.maxDelay)

GEN5.ProcessingCommand()

WebUI.click(findTestObject('Object Repository/Web/GEN5 GA/Create E-GL/Add MemberNo/BTN_Close_PopUP_GL'))




































