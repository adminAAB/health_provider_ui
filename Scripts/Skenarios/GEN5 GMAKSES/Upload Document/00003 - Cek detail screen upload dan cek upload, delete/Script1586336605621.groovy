import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Login/Login'), [('password') : GlobalVariable.password, ('userid') : 'a.RumahSakit@gmail.com'
        , ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Click menu eligibility'), [('menu') : 'uploaddokumen', ('skenario') : 'REACT'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Upload'
        , ('param') : 'pilihdata'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Cek detail screen upload dokumen'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Upload'
        , ('param') : 'CekDataScreenUpload'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen Sce 3 cek detail screen upload'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Upload image'), [('skenario') : 'upload'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Verifikasi data image ke DB'), [('skenario'):'upload'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen Sce 3 upload image tanpa submit'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Upload image'), [('skenario') : 'uploadsubmit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Verifikasi data image ke DB'), [('skenario'):'uploadsubmit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen Sce 3 upload image dg submit'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Upload'
	, ('param') : 'deleteupload'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Upload image'), [('skenario') : 'uploaddelete'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Verifikasi data image ke DB'), [('skenario'):'delete'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen Sce 3 upload image delete submit'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)

