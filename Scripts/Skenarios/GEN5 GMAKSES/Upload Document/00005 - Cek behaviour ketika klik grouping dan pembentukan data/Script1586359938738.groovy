import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Login/Login'), [('password') : GlobalVariable.password, ('userid') : 'a.RumahSakit@gmail.com'
        , ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Click menu eligibility'), [('menu') : 'uploaddokumen', ('skenario') : 'REACT'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('param') : '1data'
        , ('Skenario') : 'Grouping'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Input data grouping'), [('remarks') : 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi fermentum dolor ligula, id consequat lectus rhoncus sagittis. Integer pellentesque, ex non pellentesque blandit, nibh felis porttitor elit, nec malesuada dolor mauris ut odio. Proin at est quam. Donec eleifend elit vulputate, pharetra odio ut, eleifend elit. Nunc a sapien at nibh semper pharetra. Duis aliquet varius mi sed commodo. Aliquam aliquam, sapien nec placerat congue, sem tortor sodales nibh, vel interdum ipsum dui ut metus. Donec tortor orci, auctor et enim eget, consectetur tristique ligula.'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Verifikasi grouping ke DB'), [('param') : '1data'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 5 grouping 1 data dengan remark 100 karakter'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('param') : 'jmlhdatacustom'
        , ('Skenario') : 'grouping', ('jumlahdata') : 2], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Input data grouping'), [('remarks') : 'grouping data claim'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Verifikasi grouping ke DB'), [('param') : '1page'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 5 grouping 2 data'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('param') : 'jmlhpagecustom'
        , ('Skenario') : 'grouping', ('jumlahpage') : 2], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Input data grouping'), [('remarks') : 'grouping data claim'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Verifikasi grouping ke DB'), [('param') : '1page'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 5 grouping beberapa page'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)

