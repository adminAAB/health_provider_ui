import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Login/Login'), [('password') : GlobalVariable.password, ('userid') : 'shp.RumahSakitklinik@gmail.com'
        , ('login') : 'QR'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Click menu eligibility'), [('menu') : 'uploaddokumen', ('skenario') : 'REACT'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/Health Provider/Upload Dokumen/Cek detail screen daftar klaim'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'LoadScreen'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 cek detail screen utama dan kesesuaian data upload dokumen'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'nama'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by nama'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'jp'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by jenis perawatan'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'tanggalFrom'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by tanggal from perawatan'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'tanggalTo'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by tanggal to perawatan'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'tanggalFromTo'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by tanggal from-to perawatan'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'tagihanMin'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by tagihan min'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'tagihanMax'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by tagihan max'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'tagihanMinMax'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by tagihan min-max'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'allAdaData'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by all field ada data'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'Searching'
        , ('Param') : 'allNoData'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario searching by all field tidak ada data'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Get data dan verifikasi data di screen'), [('Skenario') : 'VerifikasiTreatmentType'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 1 skenario verifikasi data jenis perawatan'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)

