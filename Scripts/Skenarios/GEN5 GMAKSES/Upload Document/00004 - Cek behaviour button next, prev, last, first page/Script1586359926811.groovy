import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Login/Login'), [('password') : GlobalVariable.password, ('userid') : 'a.RumahSakit@gmail.com'
        , ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Click menu eligibility'), [('menu') : 'uploaddokumen', ('skenario') : 'REACT'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Behaviour button paging'), [('cek') : 'last'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 4 cek behaviour button last'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Behaviour button paging'), [('cek') : 'prev'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 4 cek behaviour button prev'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Behaviour button paging'), [('cek') : 'first'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 4 cek behaviour button first'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Behaviour button paging'), [('cek') : 'next'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Upload Dokumen sce 4 cek behaviour button next'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)



