import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Login/Login'), [('password') : GlobalVariable.password, ('userid') : 'a.RumahSakit@gmail.com'
        , ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Click menu eligibility'), [('menu') : 'daftar transaksi', ('skenario') : 'REACT'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Input data nomor peserta'), [('skenario') : 'inputdata'
        , ('TreatmentType') : 'IM'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Input all data discharge'), [('skenario') : 'diagnosa'
        , ('cek') : 'allbenefit'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Verifikasi data di database GMAKSES'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Upload Document/Data Preparation'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Verifikasi data di database SEA'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Discharge 2 cek detail summary dan data ketika discharge IM'], 
    FailureHandling.STOP_ON_FAILURE)

//WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Input data nomor peserta'), [('skenario') : 'inputdata'
//        , ('TreatmentType') : 'IM'], FailureHandling.STOP_ON_FAILURE)
//
//WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Input all data discharge'), [('skenario') : 'diagnosa'
//        , ('cek') : 'allbenefit'], FailureHandling.STOP_ON_FAILURE)
//
//WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Verifikasi data di database GMAKSES'), 
//    [:], FailureHandling.STOP_ON_FAILURE)
//
//WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Discharge/Verifikasi data di database SEA'), 
//    [:], FailureHandling.STOP_ON_FAILURE)
//
//WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Discharge 2 cek detail summary dan data ketika discharge GL'], 
//    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)

