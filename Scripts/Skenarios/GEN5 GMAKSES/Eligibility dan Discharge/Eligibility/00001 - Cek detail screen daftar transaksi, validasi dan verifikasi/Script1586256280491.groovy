import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Login/Login'), [('password') : GlobalVariable.password, ('userid') : 'a.RumahSakit@gmail.com'
        , ('login') : 'QR'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Click menu eligibility'), [('menu') : 'daftar transaksi', ('skenario') : 'REACT'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek detail screen tab eligibility'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek urutan menu'), [('cek') : 'textAttribute'
        , ('value') : 'style-card-view', ('tagname') : 'class', ('NamaKolom') : 'description', ('NamaDB') : 'SEA', ('ipDB') : '172.16.94.70'
        , ('query') : 'SELECT [Priority], code, description FROM GardaMedikaAkses.Mst_General AS MG WHERE type = \'TREATMENTTYPE\' ORDER BY MG.[Priority] ASC'
        , ('xpath') : findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LST_Object_Icon_Jenis_Perawatan')], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek urutan menu'), [('cek') : 'tagAttribute'
        , ('NamaKolom') : 'code', ('NamaDB') : 'SEA', ('ipDB') : '172.16.94.70', ('query') : 'SELECT [Priority], code, description FROM GardaMedikaAkses.Mst_General AS MG WHERE type = \'TREATMENTTYPE\' ORDER BY MG.[Priority] ASC'
        , ('xpath') : findTestObject('Object Repository/Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/LST_Object_Icon_Jenis_Perawatan')], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Eligibility sce 1 cek skenario detail screen'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek validasi mandatory'), [('skenario') : 'nomorpesertablank', ('jenisperawatan') : 'OP'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Eligibility sce 1 cek skenario validasi mandatory nomor peserta kosong'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek validasi mandatory'), [('skenario') : 'jpblank',("TreatmentType"):"OP"], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Eligibility sce 1 cek skenario validasi mandatory jenis perawatan kosong'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek validasi mandatory'), [('skenario') : 'allblank'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Eligibility sce 1 cek skenario validasi mandatory all field kosong'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek QR Code'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Eligibility sce 1 cek skenario verifikasi data QR'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/Eligibility dan Discharge/Eligibility/Cek Uppercase'), [('inputNoPeserta') : 'ab1200c'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Screenshots'), [('noskenario') : 'Eligibility sce 1 cek skenario uppercase nomer peserta'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GMAKSES/General/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)

