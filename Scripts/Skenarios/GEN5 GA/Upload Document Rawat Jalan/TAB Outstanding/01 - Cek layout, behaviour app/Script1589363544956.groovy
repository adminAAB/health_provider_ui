import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Login/Login'), [('userid') : 'a.rumahsakit@gmail.com', ('password') : 'P@ssw0rd'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Klik menu Upload Document'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'CekScreenUpload'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 1 cek detail screen upload'
        , ('pathfolder') : 'Sprint 10/Screenshots automation UI/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ClearDataParam'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 1 cek behaviour klik button clear'
        , ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'SearchNotFound'],
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 1 cek notifikasi ketika data tidak ditemukan ketika searching'
        , ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikOKNotif'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'MaxFieldNamaPeserta'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 1 cek ketika input lebih dari max field nama peserta'
        , ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikBack'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 1 cek behaviour klik button back'
        , ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)



