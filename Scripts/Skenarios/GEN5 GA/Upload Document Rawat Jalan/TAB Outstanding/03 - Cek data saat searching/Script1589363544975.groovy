import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Login/Login'), [('userid') : 'a.rumahsakit@gmail.com', ('password') : 'P@ssw0rd'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Klik menu Upload Document'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'TGFrom'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by tgl from'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'TGTo'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by tgl to'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'TGFromTo'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by tgl from to'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'NamaPeserta'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by nama peserta'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'AllDataIsi'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by all field'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'GLNoSubTengah'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by GLNo substring tengah'
	, ('pathfolder') : 'Sprint 10/Screenshots automation UI/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'GLNoSubAwal'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by GLNo substring awal'
	, ('pathfolder') : 'Sprint 10/Screenshots automation UI/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'GLNoSubAkhir'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by GLNo substring akhir'
	, ('pathfolder') : 'Sprint 10/Screenshots automation UI/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'MemberNoSubAwal'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by MemberNo substring awal'
	, ('pathfolder') : 'Sprint 10/Screenshots automation UI/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi data di screen'), [('Skenario') : 'searching', ('Param') : 'MemberNoFull'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 3 cek kesesuaian data pencarian by MemberNo full'
	, ('pathfolder') : 'Sprint 10/Screenshots automation UI/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)


