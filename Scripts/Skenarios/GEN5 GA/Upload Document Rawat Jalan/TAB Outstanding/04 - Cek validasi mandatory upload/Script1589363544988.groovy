import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Login/Login'), [('userid') : 'a.rumahsakit@gmail.com', ('password') : 'P@ssw0rd'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Klik menu Upload Document'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikUpload', ('Param') : 'NotUpload'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'CekScreenPopUpUpload'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 4 cek detail screen pop up upload'
		, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'AllBlank'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 4 cek validasi all field blank'
		, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikOKNotif'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'MaxFieldNomorInvoice'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 4 cek max karakter pada field nomor invoice'
		, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'ImageBlank'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 4 cek validasi mandatory field image'
		, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikOKNotif'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'NomorInvoiceBlank'],
	FailureHandling.STOP_ON_FAILURE)
	
WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 4 cek validasi mandatory field nomor invoice'
		, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)

