import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Login/Login'), [('userid') : 'a.rumahsakit@gmail.com', ('password') : 'P@ssw0rd'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Klik menu Upload Document'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikUpload', ('Param') : 'NotUpload'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Proses Upload dan Delete Image'), [('Skenario') : 'Upload_ValidMaxSize'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'UploadImage'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 5 cek behaviour aplikasi ketika upload valid max size 14MB'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikOKNotif'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikX'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikUpload', ('Param') : 'NotUpload'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Proses Upload dan Delete Image'), [('Param') : 'ppt', ('Skenario') : 'Upload_InvalidDataType'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'Upload_InvalidDataType'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 5 cek validasi upload invalid tipe data (ppt)'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikOKNotif'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Proses Upload dan Delete Image'), [('Param') : 'bmp', ('Skenario') : 'Upload_InvalidDataType'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'Upload_InvalidDataType'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 5 cek validasi upload invalid tipe data (bmp)'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikOKNotif'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Proses Upload dan Delete Image'), [('Param') : 'gif', ('Skenario') : 'Upload_InvalidDataType'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'Upload_InvalidDataType'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 5 cek validasi upload invalid tipe data (gif)'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'KlikOKNotif'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Proses Upload dan Delete Image'), [('Skenario') : 'Upload_InvalidMaxSize'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/Upload Document Rawat Jalan/TAB Outstanding/Verifikasi detail screen'), [('Skenario') : 'ValidasiPopUpUpload' , ('Param') : 'Upload_InvalidMaxSize'],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Screenshots'), [('noskenario') : 'Upload Document sce 5 cek validasi upload invalid max size'
	, ('pathfolder') : 'Sprint 9/Screenshots automation/'], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Web/GEN5 GA/General1/Close Browser'), [:], FailureHandling.STOP_ON_FAILURE)