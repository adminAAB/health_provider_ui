package code

import com.keyword.REA

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date

import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable

public class General {

	@Keyword
	public static def GetColumnNameTable (TestObject tableXpath){

		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"


		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'


		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))


		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))


		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		int index = 0


		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add('')
				//                index += 1
				continue
			} else {
				collsName.add(Colls[0].getText())
			}
		}
		return collsName
	}

	@Keyword
	def convertMonthYear (String MonthYear){

		ArrayList toMonthYear = MonthYear.split(" ")

		String toMonth = toMonthYear[0]

		int toYear = toMonthYear[1].toInteger()

		String month = ""

		if (toMonth == "January"){

			month = "01"

			toMonthYear.set(0, month)
		} else if (toMonth == "February"){

			month = "02"

			toMonthYear.set(0, month)
		} else if (toMonth == "March"){

			month = "03"

			toMonthYear.set(0, month)
		} else if (toMonth == "April"){

			month = "04"

			toMonthYear.set(0, month)
		} else if (toMonth == "May"){

			month = "05"

			toMonthYear.set(0, month)
		} else if (toMonth == "June"){

			month = "06"

			toMonthYear.set(0, month)
		} else if (toMonth == "July"){

			month = "07"

			toMonthYear.set(0, month)
		} else if (toMonth == "August"){

			month = "08"

			toMonthYear.set(0, month)
		} else if (toMonth == "September"){

			month = "09"

			toMonthYear.set(0, month)
		} else if (toMonth == "October"){

			month = "10"

			toMonthYear.set(0, month)
		} else if (toMonth == "November"){

			month = "11"

			toMonthYear.set(0, month)
		} else if (toMonth == "December"){

			month = "12"

			toMonthYear.set(0, month)
		}

		return toMonthYear
	}

	@Keyword
	def chooseDate (String paramdate){

		ArrayList Date = paramdate.split("/")

		int toMonth = Date[1].toInteger()

		int toYear = Date[2].toInteger()

		String toDay = Date[0]

		String defaultmonthyear = WebUI.getText(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/LBL_Month_Year'))

		ArrayList monthyear = convertMonthYear(defaultmonthyear)

		int currentMonth = monthyear[0].toInteger()

		int currentYear = monthyear[1].toInteger()

		if (currentYear < toYear) {

			while (currentYear != toYear) {

				WebUI.click(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/BTN_Next_Datepicker'))

				WebUI.delay(1)

				currentYear = WebUI.getText(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/LBL_Month_Year'))
			}
		} else if (currentYear > toYear) {

			while (currentYear != toYear) {

				WebUI.click(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/BTN_Prev_Datepicker'))

				WebUI.delay(1)

				currentYear = WebUI.getText(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/LBL_Month_Year'))
			}
		}

		if (currentMonth < toMonth) {

			while (currentMonth != toMonth) {

				WebUI.click(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/BTN_Next_Datepicker'))

				WebUI.delay(1)

				currentMonth = WebUI.getText(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/LBL_Month_Year'))
			}
		} else if (currentMonth > toMonth) {

			while (currentMonth != toMonth) {

				WebUI.click(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/BTN_Prev_Datepicker'))

				WebUI.delay(1)

				currentMonth = WebUI.getText(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/LBL_Month_Year'))
			}
		}

		WebUI.delay(1)

		WebUI.click(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Daftar Klaim/LBL_Day-day',[('day'):toDay]))
	}
}
