package code

import com.keyword.REA

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.ArrayList
import java.util.Date

import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable

public class DaftarKlaim {

	@Keyword
	def static GetJenisPerawatan () {

		String Query = "SELECT Description FROM GardaMedikaAkses.Mst_General AS MG WHERE Type='TreatmentType' ORDER BY Priority ASC"

		ArrayList JP = REA.getOneColumnDatabase("172.16.94.70", "SEA", Query, "Description")

		return JP
	}


	@Keyword
	def static DBDataClaim (String cek, String Param, ArrayList Input) {

		String addQuery = ""

		if (Param == "nama") {

			addQuery = " WHERE Nama_Pasien LIKE '"+ Input[0] +"%' "
		} else if (Param == "jp") {

			addQuery = " WHERE Treatment_Type ='"+ Input[0] +"'"
		} else if (Param == "tanggalFrom"){

			addQuery = " WHERE CAST(Tanggal_Admission_To AS Date) >= '"+ Input[0] +"'"
		} else if (Param == "tanggalTo"){

			addQuery = " WHERE CAST(Tanggal_Admission_To AS Date) <= '"+ Input[0] +"'"
		} else if (Param == "tanggalFromTo"){

			addQuery = " WHERE CAST(Tanggal_Admission_To AS Date) >= '"+ Input[0] +"' AND CAST(Tanggal_Admission_To AS Date) <= '"+ Input[1] +"' "
		} else if (Param == "tagihanMin") {

			addQuery = " WHERE a.Total_Tagihan > = "+ Input[0] +""
		} else if (Param == "tagihanMax") {

			addQuery = " WHERE a.Total_Tagihan < = "+ Input[0] +""
		} else if (Param == "tagihanMinMax") {

			addQuery = " WHERE a.Total_Tagihan > = "+ Input[0] +"  AND a.Total_Tagihan <= "+ Input[1] +""
		} else if (Param == "all") {

			addQuery = " WHERE Nama_Pasien LIKE '"+ Input[0] +"%'  AND Treatment_Type = '"+ Input[1] +"' AND a.Total_Tagihan > = "+ Input[2] +"  AND a.Total_Tagihan <= "+ Input[3] +" AND CAST(GMADK.Tanggal_Admission_To AS Date) >= '"+ Input[4] +"' AND CAST(GMADK.Tanggal_Admission_To AS Date) <='"+ Input[5] +"' "
		}

		String Query = ""

		if (cek == "1page"){

			Query = "SELECT Top 10 Cek, ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) NoUrut, Nama_Pasien,Tanggal_Admission_From,Tanggal_Admission_To,Treatment_Type,Diagnosa,REPLACE(REPLACE(FORMAT(Total_Tagihan, 'C0','zh-cn'), '¥','Rp '), ',', '.') Total_Tagihan,REPLACE(REPLACE(FORMAT(Tagihan_Dicover, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Tagihan_Dicover,REPLACE(REPLACE(FORMAT(Excess_Tagihan, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Excess_Tagihan,Upload_Status FROM (SELECT '' Cek, GMADK.ID,Nama_Pasien, CONVERT(VARCHAR, Tanggal_Admission_From, 103) Tanggal_Admission_From , CONVERT(VARCHAR, Tanggal_Admission_To, 103) Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan, Upload_Status FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId = '"+ GlobalVariable.providerID +"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_From, Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan, Upload_Status) aa"
		} else if (cek =='1data'){

			if (Param == "upload"){

				Query = "SELECT Top 20 Cek, ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) NoUrut, aa.ID, Nama_Pasien,Upload_Status,Tanggal_Admission_From,Tanggal_Admission_To,Treatment_Type,Diagnosa,REPLACE(REPLACE(FORMAT(Total_Tagihan, 'C0','zh-cn'), '¥','Rp '), ',', '.') Total_Tagihan,REPLACE(REPLACE(FORMAT(Tagihan_Dicover, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Tagihan_Dicover,REPLACE(REPLACE(FORMAT(Excess_Tagihan, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Excess_Tagihan FROM (SELECT '' Cek, GMADK.ID,Nama_Pasien, CONVERT(VARCHAR, Tanggal_Admission_From, 103) Tanggal_Admission_From , CONVERT(VARCHAR, Tanggal_Admission_To, 103) Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Upload_Status,Excess_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId = '"+ GlobalVariable.providerID+"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_From,Upload_Status, Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan ) aa GROUP BY aa.ID, Nama_Pasien,Tanggal_Admission_From,Tanggal_Admission_To,Treatment_Type,Upload_Status,Diagnosa,Total_Tagihan,Tagihan_Dicover,Excess_Tagihan, Cek"
			} else {

				Query = "SELECT Top 1 Nama_Pasien,Tanggal_Admission_From,Tanggal_Admission_To,Treatment_Type,Diagnosa,REPLACE(REPLACE(FORMAT(Total_Tagihan, 'C0','zh-cn'), '¥','Rp '), ',', '.') Total_Tagihan,REPLACE(REPLACE(FORMAT(Tagihan_Dicover, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Tagihan_Dicover,REPLACE(REPLACE(FORMAT(Excess_Tagihan, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Excess_Tagihan ,Upload_Status FROM (SELECT '' Cek, GMADK.ID,Nama_Pasien, CONVERT(VARCHAR, Tanggal_Admission_From, 103) Tanggal_Admission_From , CONVERT(VARCHAR, Tanggal_Admission_To, 103) Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan, Upload_Status FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId =  '"+ GlobalVariable.providerID +"' AND ID = '"+ GlobalVariable.dataParam +"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_From, Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan, Upload_Status ) aa"
			}
		} else if (cek =='all'){

			Query = "SELECT Cek, ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) NoUrut, Nama_Pasien,Tanggal_Admission_From,Tanggal_Admission_To,Treatment_Type,Diagnosa,REPLACE(REPLACE(FORMAT(Total_Tagihan, 'C0','zh-cn'), '¥','Rp '), ',', '.') Total_Tagihan,REPLACE(REPLACE(FORMAT(Tagihan_Dicover, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Tagihan_Dicover,REPLACE(REPLACE(FORMAT(Excess_Tagihan, 'C0', 'zh-cn'), '¥','Rp '), ',', '.') Excess_Tagihan FROM (SELECT '' Cek, GMADK.ID,Nama_Pasien, CONVERT(VARCHAR, Tanggal_Admission_From, 103) Tanggal_Admission_From , CONVERT(VARCHAR, Tanggal_Admission_To, 103) Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId =  '"+ GlobalVariable.providerID +"' GROUP BY ID) a ON a.ID = GMADK.ID "+ addQuery +" GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_From, Tanggal_Admission_To,Treatment_Type,Diagnosa,a.Total_Tagihan,Tagihan_Dicover,Excess_Tagihan ) aa"
		} else if (cek =='allgrouping'){

			if (Param == "alldata"){

				Query = "SELECT ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) NoUrut, ID,Nama_Pasien FROM (SELECT GMADK.ID,Nama_Pasien,Tanggal_Admission_To FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId = '"+ GlobalVariable.providerID +"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_To) aa "
			} else if (Param == "1page"){

				Query = "SELECT TOP 10 ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) NoUrut, ID,Nama_Pasien FROM (SELECT GMADK.ID,Nama_Pasien,Tanggal_Admission_To FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId = '"+ GlobalVariable.providerID +"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_To) aa "
			} else if (Param == "1data"){

				Query = "SELECT TOP 1 ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) NoUrut, ID,Nama_Pasien FROM (SELECT GMADK.ID,Nama_Pasien,Tanggal_Admission_To FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId = '"+ GlobalVariable.providerID +"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_To) aa "
			} else if (Param == "countdata"){

				Query = "SELECT TOP "+ GlobalVariable.dataParam2.toInteger() + " ROW_NUMBER() OVER (ORDER BY Tanggal_Admission_To ASC) NoUrut, ID,Nama_Pasien FROM (SELECT GMADK.ID,Nama_Pasien,Tanggal_Admission_To FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK INNER JOIN (SELECT ID, SUM(Total_Tagihan) AS Total_Tagihan FROM dbo.GardaMedikaAkses_DaftarKlaim AS GMADK WHERE ProviderId = '"+ GlobalVariable.providerID +"' GROUP BY ID) a ON a.ID = GMADK.ID GROUP BY GMADK.ID,Nama_Pasien, Tanggal_Admission_To) aa "
			}
		}

		println ("Query : " + Query)

		ArrayList DBClaim = new ArrayList()

		if (cek =='1data' && Param != "upload"|| Param == "1data") {

			DBClaim = REA.getOneRowDatabase("172.16.94.48", "LiTT", Query)
		} else {

			DBClaim = REA.getAllDataDatabase("172.16.94.48", "LiTT", Query)
		}

		if (cek == "1data" && Param != "upload"){

			String TanggalPerawatanFrom = GetAllData.convertdateDB(DBClaim[1].toString())

			String TanggalPerawatanTo = GetAllData.convertdateDB(DBClaim[2].toString())

			DBClaim.set(1, TanggalPerawatanFrom + " - " + TanggalPerawatanTo)

			DBClaim.remove(DBClaim[2])
		} else if (cek != "allgrouping" && Param != "upload"){

			int i

			for (i = 0; i < DBClaim.size(); i++){

				String TanggalPerawatanFrom = GetAllData.convertdateDB((DBClaim[i])[3].toString())

				String TanggalPerawatanTo = GetAllData.convertdateDB((DBClaim[i])[4].toString())

				DBClaim[i].set(3, TanggalPerawatanFrom + " - " + TanggalPerawatanTo)

				DBClaim[i].remove((DBClaim[i])[4])
			}
		}

		return DBClaim
	}

	@Keyword
	def static GetDataClaim (String cek){

		ArrayList DataClaim = new ArrayList()

		if (cek == "allpage"){

			String Angka = WebUI.getText(findTestObject('Web/Health Provider/Upload Dokumen/Daftar Klaim/BTN_Angka_Page'))

			ArrayList angka0 = Angka.split(" of ")

			int JumlahPage = angka0[1].toInteger()

			println ("JumlahPage : " + JumlahPage)

			if (JumlahPage > 1){

				DataClaim = REA.getAllDataTableMultiPage(findTestObject('Web/Health Provider/Upload Dokumen/Daftar Klaim/TBL_Claim'), findTestObject('Web/Health Provider/Upload Dokumen/Daftar Klaim/BTN_Next_Page'))
			} else {

				DataClaim = REA.getAllDataTable(findTestObject('Web/Health Provider/Upload Dokumen/Daftar Klaim/TBL_Claim'))
			}
		} else if (cek == "1page"){

			DataClaim = REA.getAllDataTable(findTestObject('Web/Health Provider/Upload Dokumen/Daftar Klaim/TBL_Claim'))
		} else if (cek == "1data"){

			DataClaim = REA.getAllDataTable(findTestObject('Object Repository/Web/Health Provider/Upload Dokumen/Upload Dokumen/TBL_Claim_Popup'))
		}

		return DataClaim
	}

	@Keyword
	def verifikasiDataClaim (String cek){

		ArrayList DBDataClaim = new ArrayList()

		ArrayList ScreenDataClaim = new ArrayList()

		if (cek == "1page"){

			DBDataClaim = DaftarKlaim.DBDataClaim("1page","kosong", ["kosong"])

			println ("DBDataClaim: "+DBDataClaim)

			ScreenDataClaim = DaftarKlaim.GetDataClaim("1page")

			println ("ScreenDataClaim: "+ScreenDataClaim)

			int i

			for (i = 0; i < DBDataClaim.size(); i++){

				int j

				for (j = 0; j < DBDataClaim[0].size(); j++){

					if ((ScreenDataClaim[i])[j] == (DBDataClaim[i])[j]){

						KeywordUtil.markPassed("Value " + (ScreenDataClaim[i])[j] +" from Grid Table same with Database.")
					} else {

						KeywordUtil.markFailedAndStop("Value from Grid Table = " + (ScreenDataClaim[i])[j] + " has different Value from database = " + (DBDataClaim[i])[j])
					}
				}
			}
		} else if (cek == "1data"){

			DBDataClaim = DaftarKlaim.DBDataClaim("1data","kosong", ["kosong"])

			println ("DBDataClaim : " +DBDataClaim)

			ArrayList a = DaftarKlaim.GetDataClaim("1data")

			ScreenDataClaim = a[0]

			println ("ScreenDataClaim : " +ScreenDataClaim)

			WebUI.verifyMatch(ScreenDataClaim.size().toString(), DBDataClaim.size().toString(), false)

			int i

			for (i = 0; i < DBDataClaim.size(); i++){

				if (ScreenDataClaim[i] == DBDataClaim[i]) {

					KeywordUtil.markPassed("Value " + ScreenDataClaim[i] +" from Grid Table same with Database.")
				} else {

					KeywordUtil.markFailedAndStop("Value from Grid Table = " + ScreenDataClaim[i] + " has different Value from database = " + DBDataClaim[i])
				}
			}
		}
	}

	@Keyword
	def VerifikasiJenisPerawatan(){

		String Query = "SELECT Description FROM GardaMedikaAkses.Mst_General AS MG WHERE Type='TreatmentType' ORDER BY Priority asc"

		ArrayList sourceJP = REA.getOneColumnDatabase("172.16.94.70", "SEA", Query, "Description")

		ArrayList screenJP = REA.getArrayButton(findTestObject('Web/Health Provider/Upload Dokumen/Daftar Klaim/CMB_Jenis_Perawatan'), "tagname", "option")

		screenJP.remove(screenJP[0])

		println ("screenJP: "+ screenJP)

		println ("sourceJP: "+ sourceJP)

		WebUI.verifyMatch(screenJP.size().toString(), sourceJP.size().toString(), false)

		int i

		for (i = 0; i < sourceJP.size; i++){

			WebUI.verifyMatch(screenJP[i], sourceJP[i], false)
		}
	}

	@Keyword
	def GetColumnNameTable (TestObject tableXpath){

		WebDriver Driver = DriverFactory.getWebDriver()

		TestObject tObj = tableXpath
		String XpathTable = "${tObj.findPropertyValue('xpath')}"


		String XpathTableRowBody = XpathTable + '/tbody'
		String XpathTableHead = XpathTable + '/thead//tr'
		String XpathTableBody = XpathTable + '/tbody//tr'


		WebElement tableHead = Driver.findElement(By.xpath(XpathTableHead))
		WebElement tableBody = Driver.findElement(By.xpath(XpathTableBody))
		WebElement tableRowBody = Driver.findElement(By.xpath(XpathTableRowBody))


		List<WebElement> rows =  tableHead.findElements(By.tagName("th"))
		List<WebElement> baris =  tableBody.findElements(By.tagName("td"))
		List<WebElement> rowBody = tableRowBody.findElements(By.tagName("tr"))


		List<String> collsName = new ArrayList()
		List<String> column = new ArrayList()
		int index = 0


		int i
		for (i = 0 ; i < rows.size() ; i++){
			List<WebElement> Colls = rows[i].findElements(By.tagName("span"))
			if (Colls.size() == 0){
				collsName.add('')
				//                index += 1
				continue
			} else {
				collsName.add(Colls[0].getText())
			}
		}
		return collsName
	}
}




