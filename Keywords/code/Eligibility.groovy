package code

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Eligibility {

	@Keyword
	def Valid (){

		String QueryValidMember = "SELECT TOP 1 a.MemberNo , RTRIM(TreatmentType) AS TreatmentType ,  a.Name , a.MNo, PT.ProductType, ClientID FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.memberno = a.memberno		OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )        AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND HCE.MemberNo IS NULL AND ((FGLI.FamilyPlanF = 0 AND IndividualRemainingLimit > 0) OR (FGLI.FamilyPlanF = 1 AND FamilyRemainingLimit > 0)) GROUP BY a.MNo , a.MemberNo , RTRIM(TreatmentType) , a.Name, PT.ProductType, ClientID"

		ArrayList ValidMember = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryValidMember)

		GlobalVariable.dataDinamis = ValidMember

		println (GlobalVariable.dataDinamis)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)
	}

	@Keyword
	def validLimitMuncul (){

		String QueryvalidLimit = "SELECT TOP 1 a.MemberNo , RTRIM(TreatmentType) AS TreatmentType ,  a.Name , a.MNo, PT.ProductType, ClientID FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Mst_General AS MG ON ClientID = code AND  MG.Type ='ClientLimit' LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.memberno = a.memberno		OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )        AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND HCE.MemberNo IS NULL AND ((FGLI.FamilyPlanF = 0 AND IndividualRemainingLimit > 0) OR (FGLI.FamilyPlanF = 1 AND FamilyRemainingLimit > 0)) GROUP BY a.MNo , a.MemberNo , RTRIM(TreatmentType) , a.Name, PT.ProductType, ClientID"

		ArrayList ValidLimit = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryvalidLimit)

		GlobalVariable.dataDinamis = ValidLimit

		println (GlobalVariable.dataDinamis)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)
	}

	@Keyword
	def ValidKeteranganMuncul (){

		String QueryValidKeterangan = "SELECT TOP 1 a.MemberNo , RTRIM(MBT.TreatmentType) AS TreatmentType ,  a.Name , a.MNo, PT.ProductType, a.PayorID FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Mapping_Treatment_TC AS MTT ON a.PayorID = MTT.ClientID AND MTT.Status = '1' LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.memberno = a.memberno		OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )        AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND HCE.MemberNo IS NULL AND ((FGLI.FamilyPlanF = 0 AND IndividualRemainingLimit > 0) OR (FGLI.FamilyPlanF = 1 AND FamilyRemainingLimit > 0)) GROUP BY a.MNo , a.MemberNo , RTRIM(MBT.TreatmentType) , a.Name, PT.ProductType, a.PayorID"

		ArrayList ValidKeterangan = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryValidKeterangan)

		GlobalVariable.dataDinamis = ValidKeterangan

		println (GlobalVariable.dataDinamis)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)
	}

	@Keyword
	def Valid0 (){

		String QueryValidMember = "SELECT TOP 1 a.MemberNo , RTRIM(TreatmentType) AS TreatmentType ,  a.Name , a.MNo, PT.ProductType, ClientID FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.memberno = a.memberno	OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )        AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND HCE.MemberNo IS NULL AND ((FGLI.FamilyPlanF = 0 AND IndividualRemainingLimit <= 0) OR (FGLI.FamilyPlanF = 1 AND FamilyRemainingLimit <= 0)) GROUP BY a.MNo , a.MemberNo , RTRIM(TreatmentType) , a.Name, PT.ProductType, ClientID"

		//SELECT TOP 1 a.MemberNo , RTRIM(TreatmentType) AS TreatmentType ,  a.Name , a.MNo, PT.ProductType FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.memberno = a.memberno		OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )        AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND HCE.MemberNo IS NULL AND ((FGLI.FamilyPlanF = 0 AND IndividualRemainingLimit <= 0) OR (FGLI.FamilyPlanF = 1 AND FamilyRemainingLimit <= 0)) GROUP BY a.MNo , a.MemberNo , RTRIM(TreatmentType) , a.Name, PT.ProductType"

		ArrayList ValidMember0 = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryValidMember)

		GlobalVariable.dataDinamis = ValidMember0

		println (GlobalVariable.dataDinamis)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)
	}

	@Keyword
	def InvalidTreatmentType (){

		String QueryValid = "SELECT TOP 1 a.MemberNo , RTRIM(TreatmentType) AS TreatmentType , a.Name, a.MNo,  a.NAME, ClientID FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.memberno = a.memberno INNER JOIN GardaMedikaAkses.Mst_General AS MG ON ClientID = code WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND GETDATE() BETWEEN a.PDate AND a.PPDate AND HCE.MemberNo IS NULL GROUP BY a.MNo, a.MemberNo , RTRIM(TreatmentType) , a.Name, ClientID"

		//SELECT TOP 1 a.MemberNo , RTRIM(TreatmentType) AS TreatmentType , a.Name, a.MNo, ClientID FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON HCE.memberno = a.memberno INNER JOIN GardaMedikaAkses.Mst_General AS MG ON ClientID = code WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND GETDATE() BETWEEN a.PDate AND a.PPDate AND HCE.MemberNo IS NULL GROUP BY a.MNo, a.MemberNo , RTRIM(TreatmentType) , a.Name, ClientID"

		ArrayList Valid = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryValid)

		GlobalVariable.dataDinamis = Valid

		String QueryMember = "SELECT a.MemberNo , RTRIM(TreatmentType) AS TreatmentType FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID  WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND GETDATE() BETWEEN a.PDate AND a.PPDate AND MemberNo ='" + GlobalVariable.dataDinamis[0] +"' GROUP BY a.MemberNo , RTRIM(TreatmentType)"

		ArrayList JP = REA.getOneColumnDatabase("172.16.94.70", "SEA", QueryMember, "TreatmentType")

		String penampungJP = ''

		int i

		for (i = 0 ; i < JP.size() ; i++) {

			penampungJP += "'" + JP[i] + "',"

			if (i == JP.size()-1) {

				penampungJP += "'" + JP[i] + "'"
			} else {

				penampungJP += "'" + JP[i] + "',"
			}
		}

		String QueryTreatment = "SELECT Top 1 Code FROM GardaMedikaAkses.Mst_General AS MG WHERE Type ='TreatmentType' and code NOT IN ("+ penampungJP +")"

		String TreatmentType = REA.getValueDatabase("172.16.94.70", "SEA", QueryTreatment, "Code")

		GlobalVariable.dataDinamis.set(1, TreatmentType)

		println (GlobalVariable.dataDinamis)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)
	}


	@Keyword
	def Unregistered (String Flag){

		String QueryValidMember = "SELECT TOP 1 PM.MemberNo, RTRIM(TreatmentType) AS TreatmentType, PDate, PPDate, Name FROM dbo.Policy_Member PM INNER JOIN dbo.Policy_Member_Plan PMP ON PM.MNo = PMP.MNo AND PMP.ExcludeF = 0 INNER JOIN dbo.Policy_Benefit_Schedule PBS ON PMP.PNO = PBS.PNO AND PMP.PPlan = PBS.PPlan AND PMP.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID WHERE CAST(GETDATE() AS DATE) BETWEEN PM.PDate AND PM.PPDate AND PM.MemberNo NOT IN (SELECT MemberNo FROM GardaMedikaAkses.HST_Claim_Eligibility)"

		ArrayList dataMember = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryValidMember)

		if (Flag == "XXX"){

			String data = dataMember[0] + "XXX"

			dataMember.set(0, data)

			GlobalVariable.dataDinamis = dataMember
		} else {

			String data = "'" + dataMember[0]

			dataMember.set(0, data)

			GlobalVariable.dataDinamis = dataMember
		}

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(4)

		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleUnregistered]), 1)
	}


	@Keyword
	def StatusE () {

		String QueryMemberStatusE = "SELECT Top 1 MemberNo,TreatmentType,CE.EntryDt FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE CE.ResultStatusClaim = 'E' AND CE.EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) AND MemberNo NOT IN (SELECT MemberNo FROM GardaMedikaAkses.Hst_Claim_Eligibility WHERE ID IS NULL)"

		ArrayList dataMemberStatusE = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryMemberStatusE)

		GlobalVariable.dataDinamis = dataMemberStatusE

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0] )

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)

		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleAlreadyRegistered]), 1)
	}

	@Keyword
	def StatusENonH (){

		String QueryStatusENonH = "SELECT TOP 1 a.MemberNo , RTRIM(CE.TreatmentType) AS TreatmentType ,  a.Name , a.MNo, PT.ProductType, ClientID FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Claim_Eligibility AS CE ON CE.MemberNo = a.MemberNo OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND  ((FGLI.FamilyPlanF = 0 AND IndividualRemainingLimit <= 0) OR (FGLI.FamilyPlanF = 1 AND FamilyRemainingLimit <= 0)) and CE.ResultStatusClaim = 'E' AND CE.EntryDt < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) AND CE.MemberNo IN (SELECT MemberNo FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim='E' GROUP BY memberno HAVING COUNT (memberno) <= 1 ) GROUP BY a.MNo , a.MemberNo , RTRIM(CE.TreatmentType) , a.Name, PT.ProductType, ClientID"

		//SELECT Top 1 MemberNo, TreatmentType, MemberName FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE CE.ResultStatusClaim = 'E' AND CE.EntryDt < DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) AND MemberNo IN (SELECT MemberNo FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim='E' GROUP BY memberno HAVING COUNT (memberno) <= 1 )"

		ArrayList dataMemberStatusENonH = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryStatusENonH)

		GlobalVariable.dataDinamis = dataMemberStatusENonH

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)
	}

	@Keyword
	def StatusD () {

		//		String Query = "SELECT TOP 1 ID FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE ResultStatusClaim = 'E' AND CE.EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) AND MemberNo IN (SELECT memberno  FROM GardaMedikaAkses.Hst_Claim_Eligibility GROUP BY memberno HAVING COUNT (memberno) = 1 )"
		//
		//		String dataID = REA.getValueDatabase("172.16.94.70", "SEA", Query, "ID")
		//
		//		println (dataID)
		//
		//		String Query1 = "SELECT TOP 1 MemberNo FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE ResultStatusClaim = 'E' AND CE.EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) AND MemberNo IN (SELECT memberno  FROM GardaMedikaAkses.Hst_Claim_Eligibility GROUP BY memberno HAVING COUNT (memberno) = 1 ) AND ID = '"+ dataID +"'"
		//
		//		String datanopeserta = REA.getValueDatabase("172.16.94.70", "SEA", Query1, "MemberNo")
		//
		//		println (datanopeserta)
		//
		//		println (Query1)
		//
		//		String update = "UPDATE GardaMedikaAkses.Claim_Eligibility SET ResultStatusClaim = 'D' WHERE MemberNo = '"+ datanopeserta +"' AND ID = '" + dataID +"'"
		//
		//		String insert = "INSERT INTO GardaMedikaAkses.Hst_Claim_Eligibility (ID , MemberNo, ResultStatusClaim , Status , EntryUsr , EntryDt , UpdateUsr , UpdateDt) VALUES  ( '" + dataID +"' , '"+ datanopeserta +"' , 'D', '1' , 'SYS' ,GETDATE(), NULL ,NULL ) "
		//
		//		println (insert)
		//
		//		println (update)
		//
		//		REA.updateValueDatabase("172.16.94.70", "SEA", update)
		//
		//		REA.updateValueDatabase("172.16.94.70", "SEA", insert)

		String QueryMemberStatusD = "SELECT TOP 1 a.MemberNo , RTRIM(MBT.TreatmentType) AS TreatmentType ,  a.Name , a.MNo, PT.ProductType, a.PayorID FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Mapping_Treatment_TC AS MTT ON a.PayorID = MTT.ClientID AND MTT.Status = '1' OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )  AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND ((FGLI.FamilyPlanF = 0 AND IndividualRemainingLimit > 0) OR (FGLI.FamilyPlanF = 1 AND FamilyRemainingLimit > 0))  AND a.MemberNo = '"+ datanopeserta +"' "

		ArrayList dataMemberStatusD = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryMemberStatusD)

		GlobalVariable.dataDinamis = dataMemberStatusD

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)
	}

	@Keyword
	def Invalid (String Flag) {

		if (Flag == "sudah daftar"){

			String QueryPolisExpired = "SELECT TOP 1 LTRIM(RTRIM(PM.MemberNo)) MemberNo , RTRIM(TreatmentType) AS TreatmentType , PDate , PPDate FROM dbo.Policy_Member PM INNER JOIN dbo.Policy_Member_Plan PMP ON PM.MNo = PMP.MNo AND PMP.ExcludeF = 0 INNER JOIN dbo.Policy_Benefit_Schedule PBS ON PMP.PNO = PBS.PNO AND PMP.PPlan = PBS.PPlan AND PMP.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID LEFT JOIN GardaMedikaAkses.Hst_Claim_Eligibility AS HCE ON LTRIM(RTRIM(PM.MemberNo)) = LTRIM(RTRIM(HCE.MemberNo))  WHERE LTRIM(RTRIM(PM.MemberNo)) NOT IN (SELECT  PM.MemberNo FROM dbo.Policy_Member PM WHERE GETDATE() BETWEEN PM.PDate AND PM.PPDate ) AND HCE.ResultStatusClaim IS NOT NULL"

			ArrayList dataPolisExpired = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryPolisExpired)

			GlobalVariable.dataDinamis = dataPolisExpired
		} else {

			String QueryPolisExpired1 = "SELECT TOP 1 LTRIM(RTRIM(PM.MemberNo)) MemberNo , RTRIM(TreatmentType) AS TreatmentType , PDate , PPDate FROM dbo.Policy_Member PM INNER JOIN dbo.Policy_Member_Plan PMP ON PM.MNo = PMP.MNo AND PMP.ExcludeF = 0 INNER JOIN dbo.Policy_Benefit_Schedule PBS ON PMP.PNO = PBS.PNO AND PMP.PPlan = PBS.PPlan AND PMP.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID  WHERE LTRIM(RTRIM(PM.MemberNo)) NOT IN (SELECT LTRIM(RTRIM(PM.MemberNo)) FROM dbo.Policy_Member PM WHERE GETDATE() BETWEEN PM.PDate AND PM.PPDate ) AND LTRIM(RTRIM(PM.MemberNo)) NOT IN (SELECT LTRIM(RTRIM(MemberNo)) FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE2)"

			ArrayList dataPolisExpired1 = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryPolisExpired1)

			GlobalVariable.dataDinamis = dataPolisExpired1
		}

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TXT_NomorPeserta'),
				GlobalVariable.dataDinamis[0])

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Jenis_Perawatan-jenisperawatan',
				[('jenisperawatan') : GlobalVariable.dataDinamis[1]]))

		WebUI.click(findTestObject('Web/Health Provider/Daftar Transaksi/Tab Eligibility/BTN_Daftar'))

		WebUI.delay(2)

		WebUI.verifyElementPresent(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/LBL_PopUp_Member_Unregistered-notifunregister', [('notifunregister'): GlobalVariable.notifNotEligibleUnregistered]), 1)
	}

	@Keyword
	def verifyClaimEligible () {

		String QueryClaimID = "SELECT * FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE ID = (SELECT MAX (ID) FROM GardaMedikaAkses.Claim_Eligibility)"

		GlobalVariable.dataParam = REA.getValueDatabase("172.16.94.70", "SEA", QueryClaimID, "ID")

		println(GlobalVariable.dataDinamis)

		ArrayList sourceClaim = [GlobalVariable.dataDinamis[0], GlobalVariable.dataDinamis[1], GlobalVariable.dataDinamis[2], GlobalVariable.providerID, GlobalVariable.statusEligible, "1"]

		println (sourceClaim)

		ArrayList sourceHstClaim = [GlobalVariable.dataParam, GlobalVariable.dataDinamis[0], GlobalVariable.statusEligible, "1"]

		println (sourceClaim)

		String QueryClaim = "SELECT MemberNo, TreatmentType, MemberName, ProviderId, ResultStatusClaim, Status FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE ID = (SELECT MAX (ID) FROM GardaMedikaAkses.Claim_Eligibility)"

		//SELECT MemberNo, MNO, MemberName, ProviderId, TreatmentType, ResultStatusClaim, Status

		String QueryHstClaim = "SELECT ID, MemberNo, ResultStatusClaim, Status FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE WHERE ID = '" + GlobalVariable.dataParam + "' AND ResultStatusClaim = 'E'"

		REA.compareRowDBtoArray("172.16.94.70", "SEA", QueryClaim, sourceClaim)

		REA.compareRowDBtoArray("172.16.94.70", "SEA", QueryHstClaim, sourceHstClaim)
	}

	@Keyword
	def verifyClaimNotELigible (String Flag) {

		if (Flag == "bukan petik") {

			ArrayList sourceData = ['null', GlobalVariable.dataDinamis[0], GlobalVariable.statusNotEligible, "1"]

			String Query = "SELECT ID, MemberNo, ResultStatusClaim, Status FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE WHERE LTRIM(RTRIM(MemberNo)) = '"+ GlobalVariable.dataDinamis[0] +"' AND EntryDt = (SELECT MAX (EntryDt) FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE WHERE LTRIM(RTRIM(MemberNo)) = '"+ GlobalVariable.dataDinamis[0] +"')"

			REA.compareRowDBtoArray("172.16.94.70", "SEA", Query, sourceData)
		} else {

			ArrayList sourceData = ['null', GlobalVariable.dataDinamis[0], GlobalVariable.statusNotEligible, "1"]

			String Query = "SELECT ID, MemberNo, ResultStatusClaim, Status FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE WHERE LTRIM(RTRIM(MemberNo)) = ''"+ GlobalVariable.dataDinamis[0] +"' AND EntryDt = (SELECT MAX (EntryDt) FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE WHERE LTRIM(RTRIM(MemberNo)) = ''"+ GlobalVariable.dataDinamis[0] +"')"

			REA.compareRowDBtoArray("172.16.94.70", "SEA", Query, sourceData)
		}
	}

	@Keyword
	def verifyClaimNotELigibleLimit0 () {

		println(GlobalVariable.dataDinamis)

		String QueryData = "SELECT TOP 1 a.MemberNo , a.MNo, a.Name , RTRIM(TreatmentType) AS TreatmentType , PT.ProductType FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )        AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND MemberNo = '" + GlobalVariable.dataDinamis[0] +"' AND TreatmentType = '" + GlobalVariable.dataDinamis[1] +"' GROUP BY a.MNo , a.MemberNo , RTRIM(TreatmentType) , a.Name, PT.ProductType"

		ArrayList data = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryData)

		println (QueryData)
		String QueryData1 = "select SeqNo from SysSeqno where ObjName='ClaimH'"

		String data1 = REA.getValueDatabase("172.16.94.70", "SEA", QueryData1, "SeqNo")
		println (data1)
		ArrayList sourceClaim = [data1, data[0], data[1], data[2], GlobalVariable.providerID, data[4], GlobalVariable.statusNotEligible, "1"]
		println (sourceClaim)
		ArrayList sourceHstClaim = [data[0], GlobalVariable.statusNotEligible, "1"]

		println (sourceHstClaim)

		ArrayList sourceDtlClaim = null

		String QueryClaim = "SELECT CNO, MemberNo, MNO, MemberName, ProviderId, TreatmentType, ResultStatusClaim, Status FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE ID = (SELECT MAX (ID) FROM GardaMedikaAkses.Claim_Eligibility)"

		String QueryID = "SELECT ID FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE ID = (SELECT MAX (ID) FROM GardaMedikaAkses.Claim_Eligibility)"

		String ID = REA.getValueDatabase("172.16.94.70", "SEA", QueryID, "ID")

		String QueryHstClaim = "SELECT MemberNo, ResultStatusClaim, Status FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE WHERE ID = '" + ID + "' AND ResultStatusClaim = 'D'"

		String QueryDtlClaim ="SELECT ChargeFieldName,BenefitID,NominalInput,NominalApproved,NominalRejected FROM GardaMedikaAkses.Dtl_Claim_Transaction AS DCT WHERE ID = "+ ID +""

		println (REA.getValueDatabase("172.16.94.70", "SEA", QueryDtlClaim, "ChargeFieldName"))

		REA.compareRowDBtoArray("172.16.94.70", "SEA", QueryClaim, sourceClaim)

		REA.compareRowDBtoArray("172.16.94.70", "SEA", QueryHstClaim, sourceHstClaim)

		REA.compareRowDBtoArray("172.16.94.70", "SEA", QueryDtlClaim, sourceDtlClaim)
	}

	@Keyword
	def summaryHeaderEligibility () {

		WebUI.verifyElementText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/LBL_Header_Eligibility'), "GARDA MEDIKA AKSES")

		ArrayList sourceSummaryH = ['Nama', 'Member No', 'NPK', 'Tanggal Lahir', 'Client', 'Effective Date', 'Tanggal Transaksi']

		String QuerySummaryH = "SELECT TOP 1 ': ' + a.NAME AS Nama , ': ' + a.MemberNo AS Member_No , ': ' + EmpID AS NPK , ': ' + convert(varchar, BirthDate, 23) AS Tanggal_Lahir , ': ' + P2.Name AS Client , ': ' + CONCAT (convert(varchar, a.PDate, 23),' - ', convert(varchar, a.PPDate, 23)) AS PeriodePolis , ': ' + convert(varchar, GETDATE(), 23) AS Tanggal_Transaksi FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Mst_General AS MG ON TreatmentType = Code INNER JOIN dbo.Profile AS P2 ON ClientID = P2.ID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND a.MemberNo = '"+ GlobalVariable.dataDinamis[0] +"'"

		ArrayList sourceSummaryH1 = REA.getOneRowDatabase("172.16.94.70", "SEA", QuerySummaryH)

		WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Header_Eligibility'), 120)

		ArrayList screenSummaryH = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Header_Eligibility'), 1, true)

		ArrayList screenSummaryH1 = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Header_Eligibility'), 2, true)

		String aa = screenSummaryH1[5].replace(': ','')

		ArrayList a = aa.split(" - ")

		String b = ": " + REA.convertDate(a[0], false)

		String c = REA.convertDate(a[1], false)

		String d = b + " - "+ c

		screenSummaryH1.set(5, d)

		String data = screenSummaryH1[3].replace(': ','')

		String data1 = screenSummaryH1[6].replace(': ','')

		String PeriodePolis = ': ' + REA.convertDate(data, false)

		String TglTransaksi = ': ' + REA.convertDate(data1, false)

		screenSummaryH1.set(3, PeriodePolis)

		screenSummaryH1.set(6, TglTransaksi)

		println ("screen : "+ screenSummaryH + "source : "+ sourceSummaryH)

		println ("screen : "+ screenSummaryH1 + "source : "+ sourceSummaryH1)

		String aba = sourceSummaryH.size()

		String bb = screenSummaryH.size()

		String cc = sourceSummaryH1.size()

		String dd = screenSummaryH1.size()

		WebUI.verifyMatch(bb, aba, false)

		WebUI.verifyMatch(dd, cc, false)

		int i

		for (i = 0; i < sourceSummaryH.size(); i++){

			WebUI.verifyMatch(screenSummaryH[i], sourceSummaryH[i], false, FailureHandling.CONTINUE_ON_FAILURE)

			WebUI.verifyMatch(screenSummaryH1[i], sourceSummaryH1[i], false, FailureHandling.CONTINUE_ON_FAILURE)

		}
	}

	@Keyword
	def summaryDetailEligibility (String Flag, String cek) {

		if (Flag == "Biaya"){

			String QuerySummaryD = "SELECT Top 1 MG.Description AS JenisPerawatan FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Mst_General AS MG ON TreatmentType = Code INNER JOIN dbo.Profile AS P2 ON ClientID = P2.ID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND MemberNo = '" + GlobalVariable.dataDinamis [0] +"' and TreatmentType ='" + GlobalVariable.dataDinamis [1] +"'"

			ArrayList sourceSummaryD = REA.getOneRowDatabase("172.16.94.70", "SEA", QuerySummaryD)

			ArrayList sourceSummaryD1 = new ArrayList()

			println (GlobalVariable.dataDinamis)

			String QueryFlagLimit = "SELECT COUNT (*) countL FROM GardaMedikaAkses.Mst_General AS MG WHERE Type = 'ClientLimit' AND Code = '" + GlobalVariable.dataDinamis[5] + "'"

			String countLimit = REA.getValueDatabase("172.16.94.70", "SEA", QueryFlagLimit, "countL")

			ArrayList screenSummaryD = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Detail_Eligibility'), 1 , false)

			ArrayList screenSummaryD1 = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Detail_Eligibility'), 2 , false)

			if (countLimit != '0'){

				ArrayList StringKolom1 = ["Limit Saat ini", "Coverage:"]

				int a

				for (a= 0 ; a < StringKolom1.size(); a++) {

					sourceSummaryD.add(StringKolom1[a])
				}

				String QueryGetLimit = "SELECT * FROM dbo.fn_GetLimitInfo ('" + GlobalVariable.dataDinamis[3] + "', '"+ GlobalVariable.dataDinamis[4] + "')"

				ArrayList GetLimit = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryGetLimit)

				if (GetLimit[0] == "1"){

					String limitFamily = GetLimit[6]

					//println ("limitFamily: " + limitFamily)

					sourceSummaryD1.add(limitFamily)

				} else {

					String limitIndividu = GetLimit[3]

					//println ("limitIndividu: " + limitIndividu)

					sourceSummaryD1.add(limitIndividu)

				}

				String amount = screenSummaryD1[0].replace('Rp','').replace('.','').replace(' ','') + ".00"

				screenSummaryD1.set(0, amount)

			} else {

				ArrayList StringTambahan = ["Coverage:"]

				int b

				for (b= 0 ; b < StringTambahan.size(); b++) {

					sourceSummaryD.add(StringTambahan[b])
				}

			}


			String QuerySourceBiaya = "SELECT ChargeFieldName FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND MemberNo = '"+ GlobalVariable.dataDinamis [0] +"' and TreatmentType ='"+ GlobalVariable.dataDinamis [1] +"' GROUP BY ChargeFieldName, [Priority] ORDER BY [Priority] asc"

			ArrayList SourceBiaya = REA.getOneColumnDatabase("172.16.94.70", "SEA", QuerySourceBiaya, "ChargeFieldName")

			String QuerySourceTambahan = "SELECT Remarks FROM GardaMedikaAkses.Mapping_Treatment_TC AS MTT WHERE ClientID = '" + GlobalVariable.dataDinamis[5] + "' AND RemarksType = 'Remarks1' ORDER BY Priority ASC"

			ArrayList SourceTambahan = REA.getOneColumnDatabase("172.16.94.70", "SEA", QuerySourceTambahan, "Remarks")

			String QuerySourceTambahan1 = "SELECT Remarks FROM GardaMedikaAkses.Mapping_Treatment_TC AS MTT WHERE ClientID = '" + GlobalVariable.dataDinamis[5] + "' AND RemarksType = 'Remarks2' ORDER BY Priority ASC"

			ArrayList SourceTambahan1 = REA.getOneColumnDatabase("172.16.94.70", "SEA", QuerySourceTambahan1, "Remarks")

			int d

			for (d= 0 ; d < SourceBiaya.size(); d++) {

				sourceSummaryD.add(SourceBiaya[d])

				sourceSummaryD1.add("Sesuai Tagihan")
			}

			ArrayList StringTambahan3 = ["Keterangan Tambahan:"]

			if (SourceTambahan.size() > 0){

				int k

				for (k = 0; k < StringTambahan3.size(); k++) {

					sourceSummaryD.add(StringTambahan3[k])

				}

			}

			int e
			for (e = 0 ; e < SourceTambahan.size(); e++) {

				sourceSummaryD.add(SourceTambahan[e])

				sourceSummaryD1.add(SourceTambahan1[e])
			}


			println ("sourceSummaryD: " + sourceSummaryD)

			println ("screenSummaryD: " + screenSummaryD)

			println ("sourceSummaryD1: " + sourceSummaryD1)

			println ("screenSummaryD1: " + screenSummaryD1)

			WebUI.verifyMatch(screenSummaryD.size().toString(), sourceSummaryD.size().toString(), false)

			WebUI.verifyMatch(screenSummaryD1.size().toString(), sourceSummaryD1.size().toString(), false)



			int f

			for (f = 0; f < sourceSummaryD.size(); f++){

				WebUI.verifyMatch(screenSummaryD[f], sourceSummaryD[f], false, FailureHandling.CONTINUE_ON_FAILURE)

			}

			int g

			for (g = 0; g < sourceSummaryD1.size(); g++){

				WebUI.verifyMatch(screenSummaryD1[g], sourceSummaryD1[g], false, FailureHandling.CONTINUE_ON_FAILURE)

			}

		} else if (Flag == "tanpaBiaya") {

			println (GlobalVariable.dataDinamis)

			String QuerySummaryD = "SELECT Description FROM GardaMedikaAkses.Mst_General where Type='TreatmentType' AND Code = '" + GlobalVariable.dataDinamis [1] +"'"

			ArrayList sourceSummaryD = REA.getOneRowDatabase("172.16.94.70", "SEA", QuerySummaryD)

			ArrayList sourceSummaryD1 = new ArrayList()

			ArrayList screenSummaryD = new ArrayList()

			ArrayList screenSummaryD1 = new ArrayList()

			if (cek =="limit0"){

				String QueryFlagLimit = "SELECT COUNT (*) countL FROM GardaMedikaAkses.Mst_General AS MG WHERE Type = 'ClientLimit' AND Code = '" + GlobalVariable.dataDinamis[5] + "'"

				String countLimit = REA.getValueDatabase("172.16.94.70", "SEA", QueryFlagLimit, "countL")

				println (countLimit)


				if (countLimit != "0"){

					ArrayList StringKolom1 = ["Limit Saat ini"]

					int a

					for (a= 0 ; a < StringKolom1.size(); a++) {

						sourceSummaryD.add(StringKolom1[a])
					}

					String QueryGetLimit = "SELECT * FROM dbo.fn_GetLimitInfo ('" + GlobalVariable.dataDinamis[3] + "', '"+ GlobalVariable.dataDinamis[1] + "')"

					ArrayList GetLimit = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryGetLimit)
					println (QueryGetLimit)

					println (GetLimit)

					if (GetLimit != "0") {

						println(sourceSummaryD1)
						if (GetLimit[0] == "1"){

							String limitFamily = GetLimit[6]

							println ("limitFamily: " + limitFamily)

							sourceSummaryD1.add(limitFamily)

						} else {

							String limitIndividu = GetLimit[3]

							println ("limitIndividu: " + limitIndividu)

							sourceSummaryD1.add(limitIndividu)

						}

					}

					screenSummaryD1 = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Detail_Eligibility'), 2, false)

				}

				screenSummaryD = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Detail_Eligibility'), 1, false)

				String QueryWordingL = "SELECT Description FROM GardaMedikaAkses.Mst_General AS MG WHERE Type='EligibilityL'"

				String wordingL = REA.getValueDatabase("172.16.94.70", "SEA", QueryWordingL, "Description")

				sourceSummaryD.add(wordingL)

				println ("screenSummaryD1: " + screenSummaryD1)

				println ("sourceSummaryD1: " + sourceSummaryD1)

				println ("screenSummaryD: " + screenSummaryD)

				println ("sourceSummaryD: " + sourceSummaryD)

				WebUI.verifyMatch(sourceSummaryD.size().toString(), screenSummaryD.size().toString(), false)

				WebUI.verifyMatch(sourceSummaryD1.size().toString(), screenSummaryD1.size().toString(), false)

				int f

				for (f = 0; f < sourceSummaryD.size(); f++){

					WebUI.verifyMatch(screenSummaryD[f], sourceSummaryD[f], false, FailureHandling.CONTINUE_ON_FAILURE)

				}

				if (countLimit != "0"){
					int g

					for (g = 0; g < sourceSummaryD1.size(); g++){

						WebUI.verifyMatch(screenSummaryD1[g], sourceSummaryD1[g], false, FailureHandling.CONTINUE_ON_FAILURE)

					}
				}

			} 	else if (cek =="benefit") {

				screenSummaryD = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Eligibility/TBL_Detail_Eligibility'), 1, false)

				println (screenSummaryD)

				String QueryWordingB = "SELECT Description FROM GardaMedikaAkses.Mst_General AS MG WHERE Type='EligibilityB'"

				println (GlobalVariable.dataDinamis)

				String wordingB = REA.getValueDatabase("172.16.94.70", "SEA", QueryWordingB, "Description")

				sourceSummaryD.add(wordingB)

				println ("screenSummaryD: " + screenSummaryD)

				println ("sourceSummaryD: " + sourceSummaryD)

				WebUI.verifyMatch(sourceSummaryD.size().toString(), screenSummaryD.size().toString(), false)

				int f

				for (f = 0; f < sourceSummaryD.size(); f++){

					WebUI.verifyMatch(screenSummaryD[f], sourceSummaryD[f], false, FailureHandling.CONTINUE_ON_FAILURE)

				}


			}


		}

	}
}
