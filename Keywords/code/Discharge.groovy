package code

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.keyword.REA
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class Discharge {

	@Keyword
	def CekNomorPesertaValidProviderValid (String input){

		if (input == "nomor peserta"){

			String QueryGetMemberNo = "SELECT TOP 1 SUBSTRING(MemberNo, 2, 3) AS InputMemberno, MemberNo,ProviderId FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim = 'E' AND EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) GROUP BY ProviderId,MemberNo"

			ArrayList GetMemberNo = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryGetMemberNo)

			GlobalVariable.dataDinamis = GetMemberNo
		} else {

			String QueryGetMemberNo = "SELECT TOP 1 SUBSTRING(MemberName, 1, 4) AS InputMemberName, MemberNo,ProviderId FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim = 'E' AND EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) GROUP BY ProviderId,MemberNo,MemberName"

			ArrayList GetMemberNo = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryGetMemberNo)

			GlobalVariable.dataDinamis = GetMemberNo
		}

		println (GlobalVariable.dataDinamis)

		String QueryGetEmail = "SELECT ParameterValue, UP.UserID,RoleID,* FROM General.UserParameters AS UP INNER JOIN General.ExternalUsers AS EU ON EU.UserID = UP.UserID INNER JOIN General.UserApplicationRoles AS UAR ON UAR.ApplicationID = UP.ApplicationID AND UAR.UserID = EU.UserID WHERE ParameterName = 'PROVIDERID' AND ParameterValue = '"+ GlobalVariable.dataDinamis[2] +"'  AND RoleID IN (77967,77968,77969,77970) AND UAR.ApplicationID = '13' AND EU.RowStatus = 0 AND UP.RowStatus = 0 ORDER BY UP.CreatedDate ASC"

		String Email = REA.getValueDatabase("172.16.94.74", "a2isAuthorizationDB", QueryGetEmail, "UserID")

		WebUI.callTestCase(findTestCase('Web/Health Provider/Login/Flow Login'), [('password') : GlobalVariable.password, ('userid') : Email
			, ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Web/Health Provider/Klik button menu di home menu'), [('menu') : 'daftar transaksi'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Web/Health Provider/Daftar Transaksi/Tab Discharge/Klik tab discharge'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TXT_Nomor_Peserta'), GlobalVariable.dataDinamis[0])

		println (GlobalVariable.dataDinamis[0])

		WebUI.delay(2)

		String Querynomorpeserta = "SELECT CONCAT(MemberNo,' - ', MemberName) peserta, TreatmentType FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim = 'E' AND (MemberNo LIKE '%" + GlobalVariable.dataDinamis[0] + "%' OR MemberName LIKE '%" + GlobalVariable.dataDinamis[0] + "%') AND ProviderId ='"+ GlobalVariable.providerID +"' AND EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE()))"

		ArrayList nomorpeserta = REA.getOneColumnDatabase("172.16.94.70", "SEA", Querynomorpeserta, "peserta")

		println (nomorpeserta)

		ArrayList treatmentType = REA.getOneColumnDatabase("172.16.94.70", "SEA", Querynomorpeserta, "TreatmentType")

		WebUI.delay(2)

		def xpath = findTestObject('Web/Health Provider/Daftar Transaksi/Tab Discharge/TBL_Nomor_Peserta')

		ArrayList screenNoPeserta = REA.getArrayButton(xpath, "tagname", "li")

		println (screenNoPeserta)

		WebUI.verifyMatch(screenNoPeserta.size().toString(), nomorpeserta.size().toString(), false)

		println (screenNoPeserta.size().toString() + " - " + nomorpeserta.size().toString())

		int i

		for (i = 0 ; i < nomorpeserta.size() ; i++) {

			WebUI.verifyMatch(screenNoPeserta[i], nomorpeserta[i], false)

			println ("data screen : " + screenNoPeserta[i] + " , data DB : " + nomorpeserta[i])
		}

		//		WebUI.click(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/LST_Nomor_Peserta-nomorpeserta',[('nomorpeserta'): screenNoPeserta[0]]))
		//
		//		WebUI.click(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/BTN_Check'))
		//
		//		String QueryDataBiaya = "SELECT ChargeFieldName FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND MemberNo = '"+ screenNoPeserta[1] +"' and TreatmentType ='"+ treatmentType[1] +"' GROUP BY ChargeFieldName, [Priority] ORDER BY [Priority] asc"
		//
		//		ArrayList GetDataBiaya = REA.getOneColumnDatabase("172.16.94.70", "SEA", QueryDataBiaya, "ChargeFieldName")
		//
		//		WebUI.delay(2)
		//
		//		def xpath1 = findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/LST_Field_Biaya')
		//
		//		ArrayList screenFieldBiaya = REA.getArrayButton(xpath1, "id", "label")
		//
		//		println (screenFieldBiaya)
		//
		//		String DataBiaya = GetDataBiaya.size()
		//
		//		String FieldBiaya = screenFieldBiaya.size()
		//
		//		println ("count screen : " + FieldBiaya + " , count DB : " + DataBiaya)
		//
		//		WebUI.verifyMatch(FieldBiaya, DataBiaya, false)
		//
		//		int j
		//		for (j = 0 ; j < GetDataBiaya.size() ; j++) {
		//
		//			WebUI.verifyMatch(screenFieldBiaya[j], GetDataBiaya[j], false)
		//
		//			println ("data screen : " + screenFieldBiaya[j] + " , data DB : " + GetDataBiaya[j])
		//		}
	}

	@Keyword
	def CekNomorPesertaProviderInvalid (){

		String QueryGetMemberNo = "SELECT TOP 1 MemberNo,ProviderId FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim = 'E' AND EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) GROUP BY ProviderId,MemberNo"

		ArrayList GetMemberNo = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryGetMemberNo)

		println(GetMemberNo)

		String QueryGetEmail = "SELECT ParameterValue, UP.UserID,RoleID,* FROM General.UserParameters AS UP INNER JOIN General.ExternalUsers AS EU ON EU.UserID = UP.UserID INNER JOIN General.UserApplicationRoles AS UAR ON UAR.ApplicationID = UP.ApplicationID AND UAR.UserID = EU.UserID WHERE ParameterName = 'PROVIDERID' AND ParameterValue <> '" + GetMemberNo[1] +"'  AND RoleID IN (77967,77968,77969,77970) AND UAR.ApplicationID = '13' AND EU.RowStatus = 0 AND UP.RowStatus = 0 ORDER BY UP.CreatedDate ASC"

		String GetEmail = REA.getValueDatabase("172.16.94.74", "a2isAuthorizationDB", QueryGetEmail, "UserID")

		println (QueryGetEmail)

		println (GetEmail)

		WebUI.callTestCase(findTestCase('Web/Health Provider/Login/Flow Login'), [('password') : GlobalVariable.password, ('userid') : GetEmail
			, ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Web/Health Provider/Klik button menu di home menu'), [('menu') : 'daftar transaksi'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Web/Health Provider/Daftar Transaksi/Tab Discharge/Klik tab discharge'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TXT_Nomor_Peserta'), GetMemberNo[0])
	}

	@Keyword
	def CekNomorPesertaInvalidProviderValid (){

		String nomorMember

		String QueryGetMemberNo = "SELECT TOP 1 MemberNo,ProviderId FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim = 'E' AND EntryDt > DATEADD(dd, 0, DATEDIFF(dd, 0, GETDATE())) GROUP BY ProviderId,MemberNo"

		ArrayList GetMemberNo = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryGetMemberNo)

		String QueryGetEmail = "SELECT ParameterValue, UP.UserID,RoleID,* FROM General.UserParameters AS UP INNER JOIN General.ExternalUsers AS EU ON EU.UserID = UP.UserID INNER JOIN General.UserApplicationRoles AS UAR ON UAR.ApplicationID = UP.ApplicationID AND UAR.UserID = EU.UserID WHERE ParameterName = 'PROVIDERID' AND ParameterValue = '"+ GetMemberNo[1] +"'  AND RoleID IN (77967,77968,77969,77970) AND UAR.ApplicationID = '13' AND EU.RowStatus = 0 AND UP.RowStatus = 0 ORDER BY UP.CreatedDate ASC"

		String GetEmail = REA.getValueDatabase("172.16.94.74", "a2isAuthorizationDB", QueryGetEmail, "UserID")

		nomorMember = GetMemberNo[0] + "XXX"

		WebUI.callTestCase(findTestCase('Web/Health Provider/Login/Flow Login'), [('password') : GlobalVariable.password, ('userid') : GetEmail
			, ('login') : 'NonQR'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Web/Health Provider/Klik button menu di home menu'), [('menu') : 'daftar transaksi'], FailureHandling.STOP_ON_FAILURE)

		WebUI.callTestCase(findTestCase('Web/Health Provider/Daftar Transaksi/Tab Discharge/Klik tab discharge'), [:], FailureHandling.STOP_ON_FAILURE)

		WebUI.setText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TXT_Nomor_Peserta'), GetMemberNo[0])
	}

	@Keyword
	def getDataDiagnosa () {

		String QueryDiagnosa = "SELECT CONCAT(COALESCE(diagnosis,''),' - ',COALESCE(name,''),' - ',COALESCE(description_1,'')) AS Diagnosa from Diagnosis where isactive = 1 ORDER BY Diagnosis ASC"

		ArrayList sourceDiagnosa = REA.getOneColumnDatabase("172.16.94.70", "SEA", QueryDiagnosa, "Diagnosa")

		ArrayList screenDiagnosa = REA.getArrayButton(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/LST_Diagnosa'), "tagname" ,"li")

		int i

		for (i = 0; i < sourceDiagnosa.size(); i++){

			WebUI.verifyMatch(screenDiagnosa[i], sourceDiagnosa[i], false)
		}
	}

	@Keyword
	def verifyClaimDischarge () {

		println("1. dataDinamis : " + GlobalVariable.dataDinamis)

		String QueryData = "SELECT TOP 1 a.MemberNo , a.MNo, a.Name , RTRIM(TreatmentType) AS TreatmentType , PT.ProductType FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )        AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND MemberNo = '" + GlobalVariable.dataDinamis[1] +"' AND TreatmentType = '" + GlobalVariable.dataDinamis[3] +"' GROUP BY a.MNo , a.MemberNo , RTRIM(TreatmentType) , a.Name, PT.ProductType"

		ArrayList data = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryData)

		String QueryData1 = "select SeqNo from SysSeqno where ObjName='ClaimH'"

		String data1 = REA.getValueDatabase("172.16.94.70", "SEA", QueryData1, "SeqNo")

		int xx

		String diagnosaTambahan = ''

		for (xx = 0; xx < GlobalVariable.dataParam1.size(); xx ++){

			if (xx == GlobalVariable.dataParam1.size()-1) {

				diagnosaTambahan += GlobalVariable.dataParam1[xx]
			} else {

				diagnosaTambahan += GlobalVariable.dataParam1[xx] + ","
			}
		}

		ArrayList sourceClaim = [data1, data[0], data[1], data[2], GlobalVariable.providerID, data[4], GlobalVariable.statusDischarge, GlobalVariable.dataParam, diagnosaTambahan, "1"]

		ArrayList sourceHstClaim = [data[0], GlobalVariable.statusDischarge, "1"]

		ArrayList sourceDtlClaim = []

		String QueryClaim = "SELECT CNO, MemberNo, MNO, MemberName, ProviderId, TreatmentType, ResultStatusClaim, PrimaryDiagnosisCode, SecondaryDiagnosisCode, Status FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE MemberNo = '" + data[0] +"' AND UpdateDt = (SELECT MAX(UpdateDt) FROM GardaMedikaAkses.Claim_Eligibility WHERE MemberNo = '" + data[0] +"')"

		String screenclaim = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryClaim)

		println ("3. QueryClaim : " + QueryClaim)

		String QueryID = "SELECT ID FROM GardaMedikaAkses.Claim_Eligibility AS CE WHERE MemberNo = '" + data[0] +"' AND UpdateDt = (SELECT MAX(UpdateDt) FROM GardaMedikaAkses.Claim_Eligibility WHERE MemberNo = '" + data[0] +"')"

		println ("4. QueryID : " + QueryID)

		String ID = REA.getValueDatabase("172.16.94.70", "SEA", QueryID, "ID")

		println ("5. ID : " + ID)

		println (screenclaim)

		println (sourceClaim)

		String QueryHstClaim = "SELECT MemberNo, ResultStatusClaim, Status FROM GardaMedikaAkses.Hst_Claim_Eligibility AS HCE WHERE ID = '" + ID + "' AND ResultStatusClaim = 'D'"

		String QueryDtlClaim ="SELECT ChargeFieldName,BenefitID,NominalInput,NominalApproved,NominalRejected FROM GardaMedikaAkses.Dtl_Claim_Transaction AS DCT WHERE ID = "+ ID +""

		REA.compareRowDBtoArray("172.16.94.70", "SEA", QueryClaim, sourceClaim)

		REA.compareRowDBtoArray("172.16.94.70", "SEA", QueryHstClaim, sourceHstClaim)
	}


	@Keyword
	def summaryHeaderDischarge () {

		WebUI.verifyElementText(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/LBL_Header_Discharge'), "GARDA MEDIKA AKSES")

		ArrayList sourceSummaryH = ['Nama', 'Member No', 'NPK', 'Tanggal Lahir', 'Client', 'Effective Date', 'Tanggal Transaksi']

		String QuerySummaryH = "SELECT TOP 1 ': ' + a.NAME AS Nama , ': ' + a.MemberNo AS Member_No , ': ' + EmpID AS NPK , ': ' + convert(varchar, BirthDate, 23) AS Tanggal_Lahir , ': ' + P2.Name AS Client , ': ' + CONCAT (convert(varchar, a.PDate, 23),' - ', convert(varchar, a.PPDate, 23)) AS PeriodePolis , ': ' + convert(varchar, GETDATE(), 23) AS Tanggal_Transaksi FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Mst_General AS MG ON TreatmentType = Code INNER JOIN dbo.Profile AS P2 ON ClientID = P2.ID INNER JOIN GardaMedikaAkses.Claim_Eligibility AS CE ON CE.MemberNo = a.MemberNo WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND a.MemberNo = '" + GlobalVariable.dataDinamis[1] + "' AND MBT.TreatmentType = '" + GlobalVariable.dataDinamis[3] + "' AND ResultStatusClaim = 'D'"

		ArrayList sourceSummaryH1 = REA.getOneRowDatabase("172.16.94.70", "SEA", QuerySummaryH)

		WebUI.waitForElementPresent(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TBL_Header_Discharge'), 120)

		ArrayList screenSummaryH = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TBL_Header_Discharge'), 1, false)

		ArrayList screenSummaryH1 = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TBL_Header_Discharge'), 2, false)

		String aa = screenSummaryH1[5].replace(': ','')

		ArrayList a = aa.split(" - ")

		String b = ": " + REA.convertDate(a[0], false)

		String c = REA.convertDate(a[1], false)

		String d = b + " - "+ c

		screenSummaryH1.set(5, d)

		String data = screenSummaryH1[3].replace(': ','')

		String data1 = screenSummaryH1[6].replace(': ','')

		String PeriodePolis = ': ' + REA.convertDate(data, false)

		String TglTransaksi = ': ' + REA.convertDate(data1, false)

		screenSummaryH1.set(3, PeriodePolis)

		screenSummaryH1.set(6, TglTransaksi)

		println ("screen : "+ screenSummaryH + "source : "+ sourceSummaryH)

		println ("screen : "+ screenSummaryH1 + "source : "+ sourceSummaryH1)

		int i

		for (i = 0; i < sourceSummaryH.size(); i++){

			WebUI.verifyMatch(screenSummaryH[i], sourceSummaryH[i], false)

			WebUI.verifyMatch(screenSummaryH1[i], sourceSummaryH1[i], false)
		}
	}

	@Keyword
	def summaryDetailDischarge () {

		String QueryID = "SELECT Description,CE.* FROM GardaMedikaAkses.Claim_Eligibility AS CE INNER JOIN GardaMedikaAkses.Mst_General AS MG ON Code = TreatmentType WHERE ResultStatusClaim = 'D' AND MemberNo ='"+ GlobalVariable.dataDinamis[1] +"' AND ID = (SELECT MAX(ID) FROM GardaMedikaAkses.Claim_Eligibility WHERE ResultStatusClaim = 'D' AND MemberNo ='"+ GlobalVariable.dataDinamis[1] +"')"

		ArrayList data = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryID)

		String QuerySummaryD = "SELECT DCT.ChargeFieldName, NominalInput, NominalApproved, NominalRejected, Priority FROM GardaMedikaAkses.Dtl_Claim_Transaction AS DCT  INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MTB ON MTB.ChargeFieldName = DCT.ChargeFieldName WHERE DCT.ID = " + data[1] + " GROUP BY DCT.ChargeFieldName, NominalInput, NominalApproved, NominalRejected, Priority ORDER BY Priority asc"

		println (QuerySummaryD)

		ArrayList sourceBiayaD = REA.getOneColumnDatabase("172.16.94.70", "SEA", QuerySummaryD, "ChargeFieldName")

		ArrayList sourceSummaryD1 = REA.getOneColumnDatabase("172.16.94.70", "SEA", QuerySummaryD, "NominalInput")

		String sourceNominalApproved = REA.getValueDatabase("172.16.94.70", "SEA", QuerySummaryD, "NominalApproved") //getOneColumnDatabase("172.16.94.70", "SEA", QuerySummaryD, "NominalApproved")

		String sourceNominalRejected = REA.getValueDatabase("172.16.94.70", "SEA", QuerySummaryD, "NominalRejected")

		ArrayList sourceSummaryD = [data[0]]

		sourceSummaryD.add("Coverage:")

		int a

		for (a = 0; a < sourceBiayaD.size(); a++){

			sourceSummaryD.add(sourceBiayaD[a])
		}

		println ("GlobalVariable.dataDinamis: " + GlobalVariable.dataDinamis)

		ArrayList StringStatis = ["Total Biaya", "Coverage Biaya", "Biaya dibayar peserta", "Limit Saat ini"]

		int b

		for (b = 0; b < StringStatis.size(); b++){

			sourceSummaryD.add(StringStatis[b])
		}

		int c

		int totBiaya = 0

		for (c = 0; c < sourceSummaryD1.size(); c++) {

			totBiaya += sourceSummaryD1[c].toInteger()

		}

		sourceSummaryD1.add(totBiaya)

		sourceSummaryD1.add(sourceNominalApproved)

		println ("sourceNominalRejected: " + sourceNominalRejected)

		if (sourceNominalRejected.toInteger() > 0) {

			sourceSummaryD1.add(sourceNominalRejected)
		}

		String QueryGetLimit = "SELECT TOP 1 a.MemberNo , a.MNo, PT.ProductType, FamilyLimitApplyF, FamilyRemainingLimit, IndividualRemainingLimit FROM    Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ( NOLOCK ) ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0        INNER JOIN PPlan p ( NOLOCK ) ON c.PPlan = p.PPlan		INNER JOIN dbo.Product AS P2 ON P2.ProductID = p.ProductID INNER JOIN dbo.ProductType AS PT ON PT.ProductType = P2.ProductType        INNER JOIN dbo.Policy_Benefit_Schedule PBS ( NOLOCK ) ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount        INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ( NOLOCK ) ON MBT.BenefitID = PBS.BenefitID OUTER APPLY dbo.fn_GetLimitInfo(a.MNo, PT.ProductType) AS FGLI WHERE   a.Status = 'R' AND b.PStatus IN ( 'A', 'C' )  AND GETDATE() BETWEEN a.PDate AND a.PPDate  AND MemberNo = '" + GlobalVariable.dataDinamis[1] + "'  AND TreatmentType = '" + GlobalVariable.dataDinamis[3] + "' GROUP BY a.MemberNo , a.MNo, PT.ProductType, FamilyLimitApplyF, FamilyRemainingLimit, IndividualRemainingLimit"

		ArrayList GetLimit = REA.getOneRowDatabase("172.16.94.70", "SEA", QueryGetLimit)

		if (GetLimit[3] == "1"){

			String limitFamily = GetLimit[4]

			println ("limitFamily: " + limitFamily)

			sourceSummaryD1.add(limitFamily)

		} else {

			String limitIndividu = GetLimit[5]

			println ("limitIndividu: " + limitIndividu)

			sourceSummaryD1.add(limitIndividu)

		}

		ArrayList screenSummaryD = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TBL_Detail_Discharge'), 1, false)

		ArrayList screenSummaryD1 = REA.getColumnDataTableUI(findTestObject('Object Repository/Web/Health Provider/Daftar Transaksi/Tab Discharge/TBL_Detail_Discharge'), 2, false)

		int x

		for (x = 0; x < screenSummaryD1.size(); x++){

			if (screenSummaryD1[x].contains("Rp")){

				String amount = screenSummaryD1[x].replace('Rp','').replace('.','').replace(' ', '')

				screenSummaryD1.set(x, amount)

			} else {

				String amount = screenSummaryD1[x].replace('.','').replace(' ', '')

				screenSummaryD1.set(x, amount)

			}

			if (x == screenSummaryD1.size()-1) {

				String amount = screenSummaryD1[x].replace('Rp','').replace('.','').replace(' ', '') + ".00"

				screenSummaryD1.set(x, amount)
			}
		}
		println ("sourceSummaryD1: " + sourceSummaryD1)

		println ("screenSummaryD1: " + screenSummaryD1)

		println ("sourceSummaryD: " + sourceSummaryD)

		println ("screenSummaryD: " + screenSummaryD)

		WebUI.verifyMatch(screenSummaryD1.size().toString(), sourceSummaryD1.size().toString(), false)

		WebUI.verifyMatch(screenSummaryD.size().toString(), sourceSummaryD.size().toString(), false)

		int i

		for (i = 0; i < sourceSummaryD.size(); i++){

			WebUI.verifyMatch(screenSummaryD[i], sourceSummaryD[i], false)
		}

		int j

		for (j = 0; j < sourceSummaryD.size(); j++){

			WebUI.verifyMatch(screenSummaryD1[j], sourceSummaryD1[j], false)
		}
	}
}
