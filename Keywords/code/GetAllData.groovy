package code

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.awt.List

import org.junit.After

import com.keyword.GEN5
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class GetAllData {

	@Keyword
	def static convertdateformat (String Date, String Format){
		
		ArrayList arrayDate = new ArrayList()
		
		arrayDate = Date.split("/")
		
		String bulan = ""
		
		String date = ""
				
		if (arrayDate[1].toString() == '01') {
					
			if (Format == "full") {
					
				bulan = "Januari"
						
			} else if (Format == "singkat"){
					
				bulan = "Jan"
						
			}

			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
					
		} else if (arrayDate[1].toString() == '02') {
		
			if (Format == "full") {
					
				bulan = "Februari"
						
			} else if (Format == "singkat"){
					
				bulan = "Feb"
						
			}			
		
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
			
		} else if (arrayDate[1].toString() == '03') {
		
			if (Format == "full") {
					
				bulan = "Maret"
						
			} else if (Format == "singkat"){
					
				bulan = "Mar"
						
			}			
		
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
			
		} else if (arrayDate[1].toString() == '04') {
		
			if (Format == "full") {
				
				bulan = "April"
					
			} else if (Format == "singkat"){
				
				bulan = "Apr"
					
			}

			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
			
		} else if (arrayDate[1] == '05') {
		
			if (Format == "full" || Format == "singkat") {
				
				bulan = "Mei"
					
			} 
		
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
			
		} else if (arrayDate[1] == '06') {
		
			if (Format == "full") {
				
				bulan = "Juni"
					
			} else if (Format == "singkat"){
				
				bulan = "Jun"
					
			}
					
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
			
		} else if (arrayDate[1] == '07') {
		
			if (Format == "full") {
				
				bulan = "Juli"
					
			} else if (Format == "singkat"){
				
				bulan = "Jul"
					
			}

			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		
		} else if (arrayDate[1] == '08') {
		
			if (Format == "full") {
				
				bulan = "Agustus"
					
			} else if (Format == "singkat"){
				
				bulan = "Agu"
					
			}
					
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
			
		} else if (arrayDate[1] == '09') {
		
			if (Format == "full") {
				
				bulan = "September"
					
			} else if (Format == "singkat"){
				
				bulan = "Sep"
					
			}

			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
					
		} else if (arrayDate[1] == '10') {
		
			if (Format == "full") {
				
				bulan = "Oktober"
					
			} else if (Format == "singkat"){
				
				bulan = "Okt"
					
			}
		
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
					
		} else if (arrayDate[1] == '11') {
		
			if (Format == "full") {
				
				bulan = "November"
					
			} else if (Format == "singkat"){
				
				bulan = "Nov"
					
			}
					
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
			
		} else if (arrayDate[1] == '12') {
		
			if (Format == "full") {
				
				bulan = "Desember"
					
			} else if (Format == "singkat"){
				
				bulan = "Des"
					
			}
					
			arrayDate.set(1, bulan)
		
			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		}
		
		return date
	}
			
	
	
	@Keyword
	def static convertdateDB (String Date){

		ArrayList arrayDate = new ArrayList()

		arrayDate = Date.split("/")

		String bulan = ""

		String date = ""


		if (arrayDate[1].toString() == '01') {

			bulan = "Jan"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1].toString() == '02') {

			bulan = "Feb"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1].toString() == '03') {

			bulan = "Mar"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1].toString() == '04') {

			bulan = "Apr"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '05') {

			bulan = "Mei"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '06') {

			bulan = "Jun"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '07') {

			bulan = "Jul"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '08') {

			bulan = "Agu"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '09') {

			bulan = "Sep"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '10') {

			bulan = "Okt"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '11') {

			bulan = "Nov"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		} else if (arrayDate[1] == '12') {

			bulan = "Des"

			arrayDate.set(1, bulan)

			date = arrayDate[0] + " " + arrayDate[1] + " " + arrayDate[2]
		}

		return date
	}
	
	
	

	@Keyword
	def verifyClaimH (String memberno, String treatmenttype){

		String QueryMNO = "SELECT Top 1 a.MNO, ClientID FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO  AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND MemberNo = '"+ memberno +"' and TreatmentType ='"+ treatmenttype +"' ORDER BY a.PNO DESC"

		ArrayList GetMNO = GEN5.getOneRowDatabase("172.16.94.70", "SEA", QueryMNO)

		String QueryClaimH = "SELECT TOP 1 a.NAME AS TreatmentPlace, AccountNo AS AccountNo, BankName AS BankName, Bank AS Bank, BankBranch AS BankBranch, BankAccount AS BankAccount, ISNULL(b.Nationality,1) AS Nationality, ISNULL(b.AccountType,'C') AS AccountType FROM Profile a LEFT JOIN ProAcc b ON a.ID=b.ID WHERE a.ID='"+ GetMNO[1] +"' AND b.ProAcc = (SELECT MIN(ProAcc) FROM ProAcc WHERE ID='"+ GetMNO[1] +"')"

		println (QueryClaimH)

		ArrayList listDataClaim = GEN5.getOneRowDatabase("172.16.94.70", "SEA", QueryClaimH)

		ArrayList listDataHardcode = [
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'H',
			'I',
			'0',
			'0',
			'',
			'ONLINE',
			'ADMEDIKA',
			'ADMEDIKA',
			'1',
			'',
			'0',
			'0',
			'0',
			'0',
			'0',
			'1',
			'-1',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'0',
			'INA',
			'0',
			'0',
			'0',
			'0',
			'P',
			'',
			'',
			'',
			'IDR',
			'',
			'0',
			'0',
			'0',
			'',
			'P',
			'C',
			'0',
			'0',
			'0',
			'0',
			'1',
			'',
			'',
			'0',
			'',
			'1',
			'',
			'0',
			'0',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			'0',
			'',
			'',
			'',
			'0',
			'0',
			'0',
			'',
			'',
			'',
			'',
			'',
			'0'
		]

		println (listDataClaim)

		int i
		for (i= 0 ; i < listDataHardcode.size(); i++) {

			listDataClaim.add(listDataHardcode[i])
		}

		println (listDataClaim)

		String insertClaimH = "SELECT CNO, ClaimNo, ID, TreatmentPlaceID, ClientNo, MNO, ProductID, PNo, RegID, branch, OCNO, AccountNo, BankAccount, BankName, Bank, BankBranch, ProductType, TreatmentPlace, ExcessPayorID, Nationality, AccountType	Hospital, Doctor, Anamnesis, FDiagnosis, SDiagnosis, DDiagnosis, LDiagnosis, Symptoms, DiseaseH, Physical, Consultation, Treatment, Suggestion, Therapist, Status, ClaimType, Exclude, Reimburse, EDescription, CCategory, FUser, LUser, HoldF, Remarks, Billed, Verified, Accepted, Excess, PreRegID, ProviderF, PrevCNO, SeqNo, ExcFDiagnosisF, ExcSDiagnosisF, ExcDDiagnosisF, ExcLDiagnosisF, PreExistFDiagnosisF, PreExistExcSDiagnosisF, PreExistDDiagnosisF, PreExistLDiagnosisF, CoAsAmount, TotalDiscount, TotalLoading, Country, InsuredAmount, InsuranceAmount, Deductible, IntSeqNo, CorrespondenceType, CorrespondenceID, RefNo, TerminalID, Currency, RefPaymentID, ManualVerified, ManualAccepted, ManualExcess, refBatchId, Payto, ExcessPayor, ASOF, AlreadyReimburseASOF, Unpaid, Discount, rate, CReason, Reason, Administration, ApprovalID, PRate, RUser, Reimbursementpct, StampDuty, EDCMerchantID, EDCTID, EDCBatchNoIN, EDCTrcIDIN, EDCBatchNoOUT, EDCTrcIDOUT, EDCApprovalCode, Vuser, InvoiceExcessNo, EDCTrIDIN, EDCTrIDOUT, edctidin, edctidout, BatchUploadNo, ACCOUNTNO_EXCESS, BANKACCOUNT_EXCESS, BANKNAME_EXCESS, AdjustF, ExGratiaF, FollowUpF, OtherTreatmentPlace, TreatmentRemarks, edctidid, edcidout, FollowUpUser, FollowingUpF FROM dbo.ClaimH AS CH WHERE CNO = (SELECT MAX(CNO) FROM dbo.ClaimH WHERE FUser = 'ADMEDIKA')"

		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", insertClaimH, listDataClaim)
	}

	@Keyword
	def verifyClaimDiagnosis (ArrayList Diagnosis){

		List<String> getDiagnosisID = new ArrayList()

		int i
		for (i = 0; i < Diagnosis.size(); i++) {

			String QueryCekDiagnosis = "SELECT Diagnosis FROM dbo.Diagnosis AS D WHERE Name = '" + Diagnosis[i] + "'"

			String getValue = GEN5.getValueDatabase("172.16.94.70", "SEA", QueryCekDiagnosis, "Diagnosis")

			getDiagnosisID.add(getValue)
		}

		println (getDiagnosisID)

		int y
		for (y = 0; y < getDiagnosisID.size(); y++) {

			String screenDiagnosis = "SELECT DiagnosisID, Remarks, ExcludeF FROM dbo.Claim_Diagnosis AS CD WHERE CNO=" + GlobalVariable.dataParam + " AND DiagnosisID = '" + getDiagnosisID[y] + "'"

			ArrayList newList = new ArrayList()

			newList = [getDiagnosisID[y], "", "0"]

			GEN5.compareRowDBtoArray("172.16.94.70", "SEA", screenDiagnosis, newList)
		}
	}

	@Keyword
	def verifyClaimDetail () {

		ArrayList listDataHardcode = ['' , '1', '0', '0', '0', '' , '' , 'V', '0', '0', '0', '0', '0', '0', '' , '0', '0', '0', '0', '0']

		String QueryCNO = "select SeqNo from SysSeqno where ObjName = 'ClaimH'"

		GlobalVariable.dataParam = GEN5.getValueDatabase("172.16.94.70", "SEA", QueryCNO, "SeqNo")

		String screenDataClaimDetail = "SELECT [CNO], [SeqNo], [BenefitID], [Billed], [Verified], [Accepted], [UnPaid], [Description], [Quantity], [Exclude], [QAccepted], [Excess], [Reason], [Remarks], [Status], [qverified], [TemporaryF], [ManualVerified], [ManualAccepted], [ManualExcess], [ProRateF], [CReason], [ExcessCoy], [ExcessEmp], [Discount], [ApropriateRate], [CoverGM] FROM [ClaimDetail] WHERE CNO = (SELECT MAX(CNO) FROM [ClaimDetail])"

		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", screenDataClaimDetail, listDataHardcode)

		/* belum ditambahin source data [SeqNo], [BenefitID], [Billed], [Verified], [Accepted], [UnPaid] dan proses compare*/
	}

	@Keyword
	def verifyClaimEditHistory (String claimno, String cno) {

		String QuerySourceData = "SELECT ISNULL(( SELECT MAX(ClaimHistoryID) + 1 FROM  ClaimEditHistory WHERE ClaimNo = '"+ claimno +"' ), 1) ,'ADMEDIKA' ,[CNO] ,[ClaimNo] ,[ID] ,[ClaimDate] ,[ClientNo] ,[MNO] ,[ProductID] ,[Start] ,[Finish] ,[Hospital] ,[Doctor] ,[Anamnesis] ,[FDiagnosis] ,[SDiagnosis] ,[DDiagnosis] ,[LDiagnosis] ,[Symptoms] ,[DiseaseH] ,[Physical] ,[Consultation] ,[Treatment] ,[Suggestion] ,[Therapist] ,[Status] ,[ClaimType] ,[Exclude] ,[Reimburse] ,[EDescription] ,[CCategory] ,[PNo] ,[FDate] ,[FUser] ,[LDate] ,[LUser] ,[HoldF] ,[Remarks] ,[Billed] ,[Verified] ,[Accepted] ,[Excess] ,[PreRegID] ,[RegID] ,[ProviderF] ,[SeqNo] ,[ExcFDiagnosisF] ,[ExcSDiagnosisF] ,[ExcDDiagnosisF] ,[ExcLDiagnosisF] ,[PreExistFDiagnosisF] ,[PreExistExcSDiagnosisF] ,[PreExistDDiagnosisF] ,[PreExistLDiagnosisF] ,[CoAsAmount] ,[TotalDiscount] ,[TotalLoading] ,[Country] ,[InsuredAmount] ,[InsuranceAmount] ,[Deductible] ,[branch] ,[IntSeqNo] ,[OCNO] ,[CorrespondenceType] ,[CorrespondenceID] ,[RefNo] ,[TerminalID] ,[Currency] ,[refPaymentDate] ,[RefPaymentID] ,[ManualVerified] ,[ManualAccepted] ,[ManualExcess] ,[refBatchId] ,[Payto] ,[ExcessPayor] ,[ASOF] ,[AlreadyReimburseASOF] ,[AccountNo] ,[BankAccount] ,[BankName] ,[ProductType] ,[EstDiscardDate] ,[Unpaid] ,[Discount] ,[rate] ,[TreatmentPlace] ,[ExcessPayorID] ,[CReason] ,[Reason] ,[Administration] ,[ApprovalDate] ,[ApprovalID] ,[PRate] ,[RDate] ,[RUser] ,[Reimbursementpct] ,[StampDuty] ,[EDCMerchantID] ,[EDCTID] ,[EDCBatchNoIN] ,[EDCTrcIDIN] ,[EDCBatchNoOUT] ,[EDCTrcIDOUT] ,[EDCApprovalCode] ,[Vdate] ,[Vuser] ,[InvoiceExcessDate] ,[InvoiceExcessNo] ,[ReceiptDate] ,[EDCTrIDIN] ,[EDCTrIDOUT] ,[edctidin] ,[edctidout] ,[BatchUploadNo] ,[ACCOUNTNO_EXCESS] ,[BANKACCOUNT_EXCESS] ,[BANKNAME_EXCESS] ,[AdjustF] ,[ExGratiaF] ,[Ftime] ,[LTime] ,[FollowUpF] ,[OtherTreatmentPlace] ,[TreatmentRemarks] ,[edctidid] ,[edcidout] ,[FollowUpDate] ,[FollowUpUser] ,[FollowingUpF] ,[Bank] ,[BankBranch] ,[Nationality] ,[AccountType] FROM dbo.ClaimH AS CH WHERE CNO = '"+ cno +"'"

		ArrayList SourceData = GEN5.getOneRowDatabase("172.16.94.70", "SEA", QuerySourceData)

		String QueryScreenData = "SELECT [ClaimHistoryID] ,[Edit_User] ,[CNO] ,[ClaimNo] ,[ID] ,[ClaimDate] ,[ClientNo] ,[MNO] ,[ProductID] ,[Start] ,[Finish] ,[Hospital] ,[Doctor] ,[Anamnesis] ,[FDiagnosis] ,[SDiagnosis] ,[DDiagnosis] ,[LDiagnosis] ,[Symptoms] ,[DiseaseH] ,[Physical] ,[Consultation] ,[Treatment] ,[Suggestion] ,[Therapist] ,[Status] ,[ClaimType] ,[Exclude] ,[Reimburse] ,[EDescription] ,[CCategory] ,[PNo] ,[FDate] ,[FUser] ,[LDate] ,[LUser] ,[HoldF] ,[Remarks] ,[Billed] ,[Verified] ,[Accepted] ,[Excess] ,[PreRegID] ,[RegID] ,[ProviderF] ,[SeqNo] ,[ExcFDiagnosisF] ,[ExcSDiagnosisF] ,[ExcDDiagnosisF] ,[ExcLDiagnosisF] ,[PreExistFDiagnosisF] ,[PreExistExcSDiagnosisF] ,[PreExistDDiagnosisF] ,[PreExistLDiagnosisF] ,[CoAsAmount] ,[TotalDiscount] ,[TotalLoading] ,[Country] ,[InsuredAmount] ,[InsuranceAmount] ,[Deductible] ,[branch] ,[IntSeqNo] ,[OCNO] ,[CorrespondenceType] ,[CorrespondenceID] ,[RefNo] ,[TerminalID] ,[Currency] ,[refPaymentDate] ,[RefPaymentID] ,[ManualVerified] ,[ManualAccepted] ,[ManualExcess] ,[refBatchId] ,[Payto] ,[ExcessPayor] ,[ASOF] ,[AlreadyReimburseASOF] ,[AccountNo] ,[BankAccount] ,[BankName] ,[ProductType] ,[EstDiscardDate] ,[Unpaid] ,[Discount] ,[rate] ,[TreatmentPlace] ,[ExcessPayorID] ,[CReason] ,[Reason] ,[Administration] ,[ApprovalDate] ,[ApprovalID] ,[PRate] ,[RDate] ,[RUser] ,[Reimbursementpct] ,[StampDuty] ,[EDCMerchantID] ,[EDCTID] ,[EDCBatchNoIN] ,[EDCTrcIDIN] ,[EDCBatchNoOUT] ,[EDCTrcIDOUT] ,[EDCApprovalCode] ,[Vdate] ,[Vuser] ,[InvoiceExcessDate] ,[InvoiceExcessNo] ,[ReceiptDate] ,[EDCTrIDIN] ,[EDCTrIDOUT] ,[edctidin] ,[edctidout] ,[BatchUploadNo] ,[ACCOUNTNO_EXCESS] ,[BANKACCOUNT_EXCESS] ,[BANKNAME_EXCESS] ,[AdjustF] ,[ExGratiaF] ,[Ftime] ,[LTime] ,[FollowUpF] ,[OtherTreatmentPlace] ,[TreatmentRemarks] ,[edctidid] ,[edcidout] ,[FollowUpDate] ,[FollowUpUser] ,[FollowingUpF] ,[Bank] ,[BankBranch] ,[Nationality] ,[AccountType] FROM dbo.ClaimEditHistory AS CEH WHERE CNO = '"+ cno +"'"

		GEN5.compareRowDBtoArray("172.16.94.70", "SEA", QueryScreenData, SourceData)
	}


	@Keyword
	def SummaryEligibleHeader (String memberno, String treatmenttype) {

		String QueryMNO = "SELECT top 1 a.MNO, ClientID FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO  AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND MemberNo = '"+ memberno +"' and TreatmentType ='"+ treatmenttype +"'"

		ArrayList GetMNO = GEN5.getOneRowDatabase("172.16.94.70", "SEA", QueryMNO)

		GlobalVariable.dataDinamis = GetMNO

		String QueryDataSummary = "SELECT Top 1 a.NAME AS Nama, MemberNo AS Member_No, EmpID AS NPK, BirthDate AS Tanggal_Lahir, P2.Name AS Client, a.PDate AS Period_From, a.PPDate AS Period_To, MG.Description FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID INNER JOIN GardaMedikaAkses.Mst_General AS MG ON TreatmentType = Code INNER JOIN dbo.Profile AS P2 ON ClientID = P2.ID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND MemberNo = '"+ memberno +"' and TreatmentType ='"+ treatmenttype +"'"

		ArrayList GetDataSummary = GEN5.getOneRowDatabase("172.16.94.70", "SEA", QueryDataSummary)
	}

	@Keyword
	def SummaryEligibleDetailLimit (String memberno, String treatmenttype) {

		String QueryFlagLimit = "SELECT COUNT (*) countL FROM GardaMedikaAkses.Mst_General AS MG WHERE Type = 'ClientLimit' AND Code = '" + GlobalVariable.dataDinamis[1] + "'"

		String countLimit = GEN5.getValueDatabase("172.16.94.70", "SEA", QueryFlagLimit, "countL")

		String limit

		if (countLimit == '1'){

			String QueryGetLimit = "SELECT * FROM dbo.fn_GetLimitInfo (" + GlobalVariable.dataDinamis[0] + ", "+ treatmenttype + ")"

			ArrayList GetLimit = GEN5.getOneRowDatabase("172.16.94.70", "SEA", QueryGetLimit)

			println (GetLimit)

			if (GetLimit[0] == "1"){

				String limitFamily = GetLimit[6]

				println (limitFamily)

				limit = limitFamily
			} else {

				String limitIndividu = GetLimit[3]

				println (limitIndividu)

				limit = limitIndividu
			}

			return limit
		}
	}

	@Keyword
	def SummaryEligibleDetailBiaya (String memberno, String treatmenttype) {

		String QueryDataBiaya = "SELECT ChargeFieldName FROM Policy_Member a WITH ( NOLOCK ) INNER JOIN policy b WITH ( NOLOCK ) ON a.pno = b.pno INNER JOIN Policy_Member_Plan c ON b.PNO = c.PNO AND a.MNO = c.MNO AND c.ExcludeF = 0 INNER JOIN PPlan p ON c.PPlan = p.PPlan INNER JOIN dbo.Policy_Benefit_Schedule PBS ON c.PNO = PBS.PNO AND c.PPlan = PBS.PPlan AND c.BAmount = PBS.BAmount INNER JOIN GardaMedikaAkses.Mapping_Treatment_Benefit AS MBT ON MBT.BenefitID = PBS.BenefitID WHERE a.Status = 'R' AND b.PStatus IN ( 'A', 'C' ) AND CAST(GETDATE() AS DATE) BETWEEN a.PDate AND a.PPDate AND MemberNo = '"+ memberno +"' and TreatmentType ='"+ treatmenttype +"' GROUP BY ChargeFieldName ORDER BY [Priority] asc"

		ArrayList GetDataBiaya = GEN5.getOneColumnDatabase("172.16.94.70", "SEA", QueryDataBiaya, "ChargeFieldName")

		return GetDataBiaya
	}

	@Keyword
	def SummaryEligibleDetailTambahan () {

		String QueryDataTambahan = "SELECT Remarks FROM GardaMedikaAkses.Mapping_Treatment_TC AS MTT WHERE ClientID = '" + GlobalVariable.dataDinamis[1] + "' ORDER BY Priority ASC"

		ArrayList DataTambahan = GEN5.getOneColumnDatabase("172.16.94.70", "SEA", QueryDataTambahan, "Remarks")

		return DataTambahan
	}

	@Keyword
	def SummaryNotEligibleDetail (String memberno, String treatmenttype, String StatusEligible) {

		String QueryJenisPerawatan = "SELECT Type, Code, Description FROM GardaMedikaAkses.Mst_General AS MG WHERE Type = 'TreatmentType' AND Code = '" + treatmenttype + "'"

		String JenisPerawatan = GEN5.getValueDatabase("172.16.94.70", "SEA", QueryJenisPerawatan, "Description")

		println (JenisPerawatan)

		if (StatusEligible == "N") {

			String QueryDesc = "SELECT Description FROM GardaMedikaAkses.Mst_General where Type ='EligibilityB' AND Code = 'N'"

			String Desc = GEN5.getValueDatabase("172.16.94.70", "SEA", QueryDesc, "Description")
		} else {

			String QueryDesc = "SELECT Description FROM GardaMedikaAkses.Mst_General where Type ='EligibilityL' AND Code = '0'"

			String Desc = GEN5.getValueDatabase("172.16.94.70", "SEA", QueryDesc, "Description")
		}
	}
}
